﻿using UnityEngine;
using System.Collections;

#if GAMEJOLT
public class GameJoltManager : MonoBehaviour 
{
    public bool IsSignedIn
    {
        get { return GameJolt.API.Manager.Instance.CurrentUser != null; }
    }

    private void Start ()
    {


    }


    public void ShowTrophies ()
    {
        GameJolt.UI.Manager.Instance.ShowTrophies();
    }

    public void ShowLeaderboards ()
    {
        GameJolt.UI.Manager.Instance.ShowLeaderboards();
    }
}
#endif