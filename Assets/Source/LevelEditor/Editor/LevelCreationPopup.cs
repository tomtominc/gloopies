﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Linq;

public class LevelCreationPopup : EditorWindow 
{
    private LevelData levelData = null;

    [SerializeField]
    public LevelEditor _editor;

    public static void CreateWindow ( LevelEditor editor )
    {
        // Get existing open window or if none, make a new one:
        LevelCreationPopup window = (LevelCreationPopup)EditorWindow.GetWindow (typeof (LevelCreationPopup));
        window._editor = editor;
        window.Show();

    }

    public void OnGUI ()
    {

        if ( levelData == null )
        {
            levelData = new LevelData ();
        }

        EditorGUIExtensions.BeginContent ( Color.white );

        levelData.name = EditorGUILayout.TextField ( "Name", levelData.name );
        levelData.rows = EditorGUILayout.IntSlider ("Rows", levelData.rows, 1, 9);
        levelData.columns = EditorGUILayout.IntSlider ("Columns",levelData.columns, 1, 7 );

        EditorGUIExtensions.EndContent ();

        if ( EditorGUIExtensions.Button ( "Create", "Button", Color.white ) )
        {
            levelData.collection = _editor.collection.folderPath.Split('/')[1];

            levelData.piece = new string[levelData.rows * levelData.columns];
            for ( int i = 0; i < levelData.piece.Length; i++ ) levelData.piece[i] = "0";

            levelData.item = new string[levelData.rows * levelData.columns];
            for ( int i = 0; i < levelData.item.Length; i++ ) levelData.item[i]   = "0";

            levelData.tile = new string[levelData.rows * levelData.columns];
            for ( int i = 0; i < levelData.tile.Length; i++ ) levelData.tile[i]   = "0";

            string jsonObject = JSONSerializer.Serialize (levelData,true);

            string folderPath = string.Format ("{0}/Resources/Levels/{1}", Application.dataPath, levelData.collection);

            string filePath = string.Format ("{0}/{1}.json", folderPath, levelData.jsonFile );

            FileExtensions.CreateDirectoryIfNotExists ( folderPath );

            FileInfo file = new System.IO.FileInfo(filePath);

            file.Directory.Create();

            System.IO.File.WriteAllText(file.FullName, jsonObject);

            AssetDatabase.SaveAssets ();

            AssetDatabase.Refresh ();

            _editor.collection.Reload ();

            _editor.collection.GenerateData ();

            Close ();
        }
    }
}
