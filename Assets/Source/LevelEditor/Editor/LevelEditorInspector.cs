﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEditorInternal;
using Rotorz.ReorderableList;

[CustomEditor(typeof(LevelEditor))]
public class LevelEditorInspector : Editor
{
    private LevelEditor _editor;

    private const string kPiecesFolder = "Prefab/Pieces";
    private const string kItemsFolder  = "Prefab/Items" ;
    private const string kTilesFolder  = "Prefab/Tiles" ;
    private const string kLevelManager = "LevelAssets/LevelManager";

    private void OnEnable()
    {
        _editor = target as LevelEditor;

        _editor._pieces = kPiecesFolder.LoadAllResources < GameObject > ( true );
        _editor._items  = kItemsFolder.LoadAllResources  < GameObject > ( true );
        _editor._tiles  = kTilesFolder.LoadAllResources  < GameObject > ( true );

        if ( _editor.levelManager == null )
        {
            _editor.levelManager = kLevelManager.LoadFromResources < LevelManager > ( true );
        }
    }

    public override void OnInspectorGUI()
    {
        EditorGUIExtensions.DrawScriptField ( serializedObject );

        EditorGUIExtensions.SetBackgroundColor ( EditorColor.blue );

        EditorGUIExtensions.DrawHeader ( "Import Options" );

        EditorGUIExtensions.BeginContent(Color.white);

        ImportSettings();

        EditorGUIExtensions.EndContent();

        EditorGUILayout.Space ();

        EditorGUIExtensions.RestoreBackgroundColor ();

        EditorGUIExtensions.SetBackgroundColor ( EditorColor.green );

        EditorGUIExtensions.DrawHeader ( "Editor Options" );

        EditorGUIExtensions.BeginContent(Color.white);

        EditorSettings();

        EditorGUIExtensions.EndContent();

        EditorGUIExtensions.RestoreBackgroundColor ();

        EditorGUILayout.Space ();

        EditorGUIExtensions.SetBackgroundColor ( EditorColor.red );

        EditorGUIExtensions.DrawHeader ( "Prefab Storage" );

        EditorGUIExtensions.BeginContent(Color.white);

        PrefabStorageMenu();

        EditorGUIExtensions.EndContent();

        EditorGUIExtensions.RestoreBackgroundColor ();

        EditorUtility.SetDirty ( target );
    }

    private void PrefabStorageMenu ()
    {
        _editor.shadowPrefab = (GameObject) EditorGUILayout.ObjectField ( "Layer Prefab", _editor.shadowPrefab, typeof ( GameObject ), false );
        _editor.tapPrefab = (GameObject) EditorGUILayout.ObjectField ( "Tap Prefab", _editor.tapPrefab, typeof ( GameObject ), false );
    }

    private void EditorSettings()
    {
        _editor.editMode = (LevelEditor.EditMode)EditorGUILayout.EnumPopup("Edit Mode", _editor.editMode);

        if ( !Application.isPlaying )
        {
            EditorGUILayout.HelpBox ("Controls only active in play mode!",MessageType.Warning );
            return;
        }

        switch ( _editor.editMode )
        {
            case LevelEditor.EditMode.Solution: EditModeSolution (); break;
            case LevelEditor.EditMode.Build: EditModeGUI (); break;
            case LevelEditor.EditMode.Play: break;
        }
    }

    private void EditModeSolution ()
    {
        EditorGUILayout.Space ();

        _editor.currentEditObjective = EditorGUIExtensions.DrawSelectableHeader ( "Solution Header GUI", "1 Star Objective", "2 Star Objective", "3 Star Objective" );

        EditorGUIExtensions.BeginContent (Color.white);

        if ( _editor.objectiveData.Count <= _editor.currentEditObjective )
        {
            _editor.objectiveData.Add ( new ObjectiveData () );
        }



        EditorGUILayout.LabelField ( string.Format ( "Current Taps Objective {0} ", _editor.currentEditObjective ), _editor.currentObjectiveData.solution.Length.ToString () );

        if ( EditorGUIExtensions.Button ("Clear Solution", "Button", EditorColor.red ) )
        {
            _editor.ClearCurrentSolution ();
        }

        EditorGUIExtensions.EndContent ();

    }

    private void EditModeGUI()
    {
        _editor.pieceKeys = _editor.GetPieceNames ();
        _editor.itemKeys  = _editor.GetItemNames ();
        _editor.tileKeys  = _editor.GetTileNames ();

        if ( _editor.currentLayer == LevelEditor.Layer.Piece )
            DrawBuilderGUI ( "Piece Settings", _editor.pieceKeys );
        else if ( _editor.currentLayer == LevelEditor.Layer.Tile )
            DrawBuilderGUI ( "Tile Settings", _editor.tileKeys  );
        else if ( _editor.currentLayer == LevelEditor.Layer.Item )
            DrawBuilderGUI ( "Item Settings", _editor.itemKeys );
    }

    private void TapCycleGUI()
    {
        
    }

    public void DrawBuilderGUI ( string title, string[] options )
    {
        EditorGUILayout.BeginHorizontal ();

        int current = _editor.GetCurrentLayerObject ();

        current = EditorGUILayout.Popup ( "Currently Selected", current, options );

        _editor.SetCurrentLayerObject ( current );

        EditorGUILayout.EndHorizontal ();
    }

    private void ImportSettings()
    {
        EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

        if (EditorGUIExtensions.Button("Reload All", EditorStyles.toolbarButton, Color.white))
        {
            _editor.ReloadAll ();
        }

        if (EditorGUIExtensions.Button("Reload Pieces",EditorStyles.toolbarButton, Color.white))
        {
            _editor.ReloadPieces ();
        }

        if (EditorGUIExtensions.Button("Save", EditorStyles.toolbarButton, Color.white))
        {
            _editor.SaveAll ();
        }

        if (EditorGUIExtensions.Button("Create New", EditorStyles.toolbarButton, Color.white))
        {
            LevelCreationPopup.CreateWindow( _editor );
        }

        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space ();

        _editor.collection = (LevelCollection)EditorGUILayout.ObjectField ( "Collection", _editor.collection, typeof(LevelCollection), false );

        if ( _editor.collection == null )
        {
            EditorGUILayout.HelpBox("Place Collection to get started", MessageType.Info);

            return;
        }

        EditorGUILayout.Space ();

        if ( _editor.collection != null )
        {
            LevelData importData = null;

            bool reload = false;

            for (int i = 0; i < _editor.collection.LevelValues.Count; i++ )
            {
                LevelValue level = _editor.collection.LevelValues[i];

                bool isSpecial = level.data.isSpecialTutorial || level.data.tutorial.Length > 0;

                Color color = isSpecial ? EditorColor.red : _editor.levelData == null ? Color.white : level.data.name == _editor.levelData.name ? EditorColor.green : Color.white;
                EditorGUIExtensions.BeginContent ( color );

                EditorGUILayout.BeginHorizontal ();

                level.data.id = EditorGUILayout.IntField ( level.data.id , GUILayout.Width (24f) );

                level.data.name = EditorGUILayout.TextField (  level.data.name  );

                level.state.levelName = level.data.name;

                var list = level.data.objectives.ToList ();
                list.RemoveAll ( x => x.solution.Length <= 0 );
                level.data.objectives = list.ToArray ();

                for ( int j = 0; j < 3; j++ )
                {
                    bool hasSolution = level.data.objectives.Length > j;
                    EditorGUILayout.Toggle ( hasSolution );
                }

                level.data.difficulty = (int)((Difficulty)EditorGUILayout.EnumPopup ( (Difficulty) level.data.difficulty ));

                if ( EditorGUIExtensions.Button ("Open Json File","Button", EditorColor.blue ) )
                {
                    string fileName = string.Format( "{0}/{1}/{2}/{3}/{4}.json", Application.dataPath, "Resources", "Levels", level.data.collection, level.data.jsonFile);

                    InternalEditorUtility.OpenFileAtLineExternal (fileName, 1);
                }
                if ( EditorGUIExtensions.Button ("Reimport Json","Button", EditorColor.orange ) )
                {
                    string fileName = string.Format( "{0}/{1}/{2}","Levels", level.data.collection, level.data.jsonFile);

                    TextAsset asset = fileName.LoadFromResources < TextAsset > (true);

                    level.data = JSONSerializer.Deserialize < LevelData > ( asset.text );

                }
                if ( EditorGUIExtensions.Button("Import", "Button", EditorColor.green) )
                {
                    if (!isSpecial)
                    {
                        importData = level.data;
                    }
                    else 
                    {
                        EditorUtility.DisplayDialog ("Can't Import Tutorial Levels", "Functionality to import tutorial levels has not been implemented.", "Ok");
                    }
                }

                if ( EditorGUIExtensions.Button ("Delete", "Button", EditorColor.red ) )
                {
                    _editor.collection.DeleteLevel ( level );

                    reload = true;
                }

                EditorGUILayout.EndHorizontal ();

                EditorGUIExtensions.EndContent ();
            }

            if ( reload )
            {
                _editor.SaveAll ();
            }

            if ( importData != null )
            {
                _editor.LoadLevel ( importData );
            }
        }

    }
}
