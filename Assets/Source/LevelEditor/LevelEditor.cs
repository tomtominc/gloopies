﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using com.ootii.Messages;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class LevelEditor : Singleton < LevelEditor >
{
    public enum EditMode
    {
        Build,
        Solution,
        Play,
    }

    public EditMode editMode;

    public enum Layer
    {
        Piece,
        Item,
        Tile
    }

    public LevelManager levelManager;
    public Layer currentLayer;

    public LevelCollection collection;

    public LevelData levelData;

    public bool isTapMode = true;
    public int nTaps = 0;
    public GameObject currentLayerObject;

    [SerializeField]
    public List < GameObject > _pieces;
    [SerializeField]
    public List < GameObject > _items;
    [SerializeField]
    public List < GameObject > _tiles;

    [SerializeField]
    public string[] pieceKeys;
    [SerializeField]
    public string[] itemKeys;
    [SerializeField]
    public string[] tileKeys;

    public GameObject shadowPrefab;
    public GameObject tapPrefab;

    public Dictionary < Layer, string[] > keyMapping;

    public List < GameObject > shadows = new List<GameObject> ();
    public List < Dictionary < int, GameObject > > tapTileObjectives;

    private int _currentEditObjective = 0;

    public BoardLevel CurrentBoardLevel
    {
        get { return GameManager.Instance.CurrentBoard.CurrentBoardLevel; }
    }

    public Board board
    {
        get { return GameManager.Instance.CurrentBoard.Board; }
    }

    [NonSerialized]
    private bool cellsEnabled = true;

    [NonSerialized]
    private bool displayTapTiles = true;

    public int currentEditObjective
    {
        get
        {
            return _currentEditObjective;
        }

        set 
        {
            if (tapTiles != null )
            {
                foreach (var pair in tapTiles )
                {
                    
                    pair.Value.SetActive ( false );
                }
            }

            _currentEditObjective = value;

            if (tapTiles == null ) return;

            if ( tapTiles.Count <= 0 && currentObjectiveData.solution.Length > 0 )
            {
                GenerateTapTiles (null);
            }

            foreach (var pair in tapTiles )
            {
                pair.Value.SetActive ( displayTapTiles );
            }
        }
    }
    public List < ObjectiveData > objectiveData;

    public void SetToWhiteSprite ()
    {
        
    }

    public ObjectiveData currentObjectiveData 
    {
        get 
        { 
            while ( objectiveData.Count <= currentEditObjective )
            {
                objectiveData.Add ( new ObjectiveData () );
            }

            return objectiveData [ currentEditObjective ]; 
        }
    }

    public Dictionary < int, GameObject > tapTiles
    {
        get 
        {
            if ( tapTileObjectives == null ) return null;

            foreach (var i in tapTileObjectives [ currentEditObjective ].Where( x => x.Value == null ).ToList() ) 
            {
                tapTileObjectives [ currentEditObjective ].Remove(i.Key);
            }

            return tapTileObjectives [ currentEditObjective ]; 
        }

        set 
        {
            tapTileObjectives [ currentEditObjective ] = value;
        }
    }

    public string currentKey
    {
        get 

        { 
            return keyMapping [ currentLayer ] [ GetCurrentLayerObject () ]; 
        }
    }

    public void Start()
    {
        _currentEditObjective = 0;

        keyMapping 
        = new Dictionary < Layer , string[] > ()
        {
            { Layer.Piece, pieceKeys },
            { Layer.Item , itemKeys  },
            { Layer.Tile , tileKeys  }
        };

        MessageDispatcher.AddListener ("BoardComplete", GenerateTapTiles  );
        MessageDispatcher.AddListener ("UndoLastMove" , RemoveLastTapTile );
    }

    public string[] GetPieceNames ()
    {
        List < string > names = new List < string > ();

        for (int i = 0; i < _pieces.Count; i++ )
        {
            Piece piece = _pieces[i].GetComponent < Piece > ();

            if (piece.modifiers.Length > 0 )
            {
                for ( int j = 0;  j < piece.modifiers.Length; j++ )
                {
                    names.Add ( string.Format ("{0}{1}", piece.jsonKey, piece.modifiers[j] ) );
                }
            }
            else
            {
                names.Add ( piece.jsonKey );
            }
        }

        return names.ToArray ();
    }


    public string[] GetItemNames ()
    {
        List < string > names = new List < string > ();

        for (int i = 0; i < _items.Count; i++ )
        {
            Item item = _items[i].GetComponent < Item > ();

            if (item.modifiers.Length > 0 )
            {
                for ( int j = 0;  j < item.modifiers.Length; j++ )
                {
                    names.Add ( string.Format ("{0}{1}", item.jsonKey, item.modifiers[j] ) );
                }
            }
            else
            {
                names.Add ( item.jsonKey );
            }
        }

        return names.ToArray ();
    }

    public string[] GetTileNames ()
    {
        List < string > names = new List < string > ();

        for (int i = 0; i < _tiles.Count; i++ )
        {
            Tile tile = _tiles[i].GetComponent < Tile > ();

            if (tile.modifiers.Length > 0 )
            {
                for ( int j = 0;  j < tile.modifiers.Length; j++ )
                {
                    names.Add ( string.Format ("{0}{1}", tile.jsonKey, tile.modifiers[j] ) );
                }
            }
            else
                names.Add ( tile.jsonKey );
        }

        return names.ToArray ();
    }

    #if UNITY_EDITOR
    public void SaveAll ()
    {
        if ( editMode == EditMode.Solution )
        {
            ReloadPieces ();
            Save ();
            ReloadAll ();
        }
        else if ( editMode == EditMode.Build )
        {
            Save ();
            ReloadAll ();
        }
    }
    #endif

    #if UNITY_EDITOR
    private void Save ()
    {
        for ( int i = 0; i < board._map.Length; i++ )
        {
            var data = board._map[i];

            try
            {
                levelData.piece [i] = data.pieceKey;
            }
            catch (System.IndexOutOfRangeException e)
            {
                Debug.LogFormat ("Piece Length: {0} Tile Length: {1} Index: {2}", levelData.piece.Length, levelData.tile.Length, i);
                throw new System.ArgumentOutOfRangeException("index is out of range for piece array", e);
            }

            try
            {
                levelData.tile  [i] = data.tileKey;
            }
            catch (System.IndexOutOfRangeException e)
            {
                Debug.LogFormat ("Piece Length: {0} Tile Length: {1} Index: {2}", levelData.piece.Length, levelData.tile.Length, i);
                throw new System.ArgumentOutOfRangeException("index is out of range for tile array", e);
            }
  

        }

        objectiveData.RemoveAll ( x => x.solution.Length <= 0 );

        levelData.objectives = objectiveData.ToArray ();

        string json = JSONSerializer.Serialize < LevelData > ( levelData, true );

        string path = string.Format( "{0}/{1}/{2}/{3}/{4}.json", Application.dataPath, "Resources", "Levels", levelData.collection, levelData.jsonFile );

        File.WriteAllText( path, json  );

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        collection.Reload ();
        collection.GenerateData();
    }
    #endif

    public void ReloadAll ()
    {
        LoadLevel ( levelData );
    }

    public void LoadLevel(string level)
    {
        LoadLevel ( JSONSerializer.Deserialize < LevelData > (level) );
    }

    public void LoadLevel ( LevelData importData )
    {
        objectiveData = new List<ObjectiveData> ();

        levelData = importData;

        foreach ( var data in levelData.objectives )
        {
            objectiveData.Add 
            (
                new ObjectiveData (data)
            );
        }

        tapTileObjectives = new List<Dictionary<int, GameObject>> ();

        tapTileObjectives.Add ( new Dictionary<int, GameObject> () );
        tapTileObjectives.Add ( new Dictionary<int, GameObject> () );
        tapTileObjectives.Add ( new Dictionary<int, GameObject> () );

        GameManager.Instance.LoadLevel(levelData);
    }

    public void ReloadPieces ()
    {
        foreach ( var cell in CurrentBoardLevel.Map)
        {
            cell.Data.pieceKey = levelData.GetPieceCode ( cell.Data.row, cell.Data.column );

            cell.Refresh ();
        }
    }

    public void GenerateTapTiles ( IMessage message )
    {
        if (CurrentBoardLevel == null ) return;

        foreach ( var pair in tapTiles )
        {
            Destroy ( pair.Value );
        }

        tapTiles = new Dictionary<int, GameObject> ();

        for (int i = 0; i < currentObjectiveData.solution.Length; i++ )
        {
            Dialogue dialogue = currentObjectiveData.solution[i];

            if ( dialogue == null ) continue;

            Cell cell = CurrentBoardLevel.GetCell ( dialogue.row, dialogue.column );

            PlaceTapTile ( cell, i, false );
        }
    }

    public void ClearCurrentSolution ()
    {
        if ( currentObjectiveData.solution.Length <= 0 ) return;

        currentObjectiveData.solution = new Dialogue[0];

        GenerateTapTiles ( null );
    }

    public void RemoveLastTapTile ( IMessage message )
    {
        if ( editMode != EditMode.Solution ) return;
        if ( currentObjectiveData.solution.Length <= 0 ) return;

        var solution = currentObjectiveData.solution.ToList ();

        solution.Remove ( solution.Last () );

        currentObjectiveData.solution = solution.ToArray ();

        GenerateTapTiles ( null );
    }

    public void SetLevelImport(TextAsset asset)
    {
        //LevelImport = asset;
    }

    public void PlaceShadow ( Cell cell )
    {

        Sprite s = CellFactory.Instance.GetSprite ( currentKey );

        GameObject go = Instantiate < GameObject > ( shadowPrefab );

        go.transform.SetParent ( GameManager.Instance.CurrentBoard.transform, false );

        go.transform.localPosition = cell.transform.localPosition;

        go.transform.FindChild ("Icon").GetComponent < Image > ().sprite = s;

        shadows.Add ( go );
    }

    public void PlaceTapTile ( Cell cell, int n, bool addToSolution )
    {
        if ( board == null || cell == null ) return;

        int index = board.GetIndex ( cell.Data.row, cell.Data.column );

        GameObject go = null;

        if ( tapTiles.ContainsKey ( index ) )
        {
            go = tapTiles [ index ];
        }
        else
        {
            go = Instantiate < GameObject > ( tapPrefab );

            go.transform.SetParent ( cell.transform, false );

            go.transform.localPosition = Vector3.zero;

            tapTiles.Add ( index, go );
        }

        if ( go == null )
        {
            Debug.LogWarning ( "The Gameobject go is null when trying to place tap tile.");
            return;
        }

        foreach ( var pair in tapTiles )
        {
            pair.Value.transform.FindChild ("Order").GetComponent < TapTile > ().SetNormal ();
        }

        TapTile tile = go.transform.FindChild ("Order").GetComponent < TapTile > ();

        tile.PlaceTapOrderLabel ( n );

        tile.SetLast ();

        if ( addToSolution == false ) return;

        Dialogue tap = new Dialogue ( cell.Data.row, cell.Data.column, "" );

        var solution = currentObjectiveData.solution.ToList ();

        solution.Add ( tap );

        currentObjectiveData.solution = solution.ToArray ();
    }

    public void RemoveShadows ()
    {
        foreach ( var go in shadows )
        {
            Destroy ( go );
        }

        shadows = new List<GameObject> ();
    }

    public void IncrementCurrentLayer ( Cell cell )
    {
        string[] keys = keyMapping [ currentLayer ];

        int current = GetCurrentLayerObject();
        current = current.GetWrapped(0,keys.Length - 1,1);
        SetCurrentLayerObject(current);

        if ( cell == null ) return;

        RemoveShadows ();
        PlaceShadow ( cell );
    }

    public void PlaceLayerObject(Cell cell)
    {
        switch (currentLayer)
        {
            case Layer.Piece:
                cell.Data.pieceKey = currentKey;
                break;

            case Layer.Item:
                cell.Data.itemKey = currentKey;
                break;

            case Layer.Tile:
                cell.Data.tileKey = currentKey;
                break;
        }

        cell.Refresh ();
    }

    public void RemoveAllLayerObjects (Cell cell)
    {
        cell.Data.pieceKey = "0";
        cell.Data.itemKey = "0";
        cell.Data.tileKey = "0";

        cell.Refresh ();
    }
    public void Update()
    {
        
        QueryHotKeys ();
        
        switch ( editMode )
        {
            case EditMode.Build: BuildMode (); break;
            case EditMode.Solution: SolutionMode (); break;
            case EditMode.Play: PlayMode (); break;
        }
    }

    public void QueryHotKeys ()
    {
        EditMode mode = editMode;

        if (Input.GetKeyDown(KeyCode.Q))
            editMode = EditMode.Build;
        if (Input.GetKeyDown(KeyCode.W))
            editMode = EditMode.Solution;
        if (Input.GetKeyDown(KeyCode.E))
            editMode = EditMode.Play;


        if ( mode != editMode )
        {
            OnChangedMode ();
        }
    }

    public void OnChangedMode ()
    {
        RemoveShadows ();

        displayTapTiles = true;

        cellsEnabled = true;

//        foreach ( Cell cell in boardView._map )
//        {
//            //FIX
//            //cell.Enabled = cellsEnabled;
//        }
    }

    public void PlayMode ()
    {
        if (displayTapTiles) 
        {
            displayTapTiles = false;

            if ( currentEditObjective > -1 )
            {
                currentEditObjective = 0;
            }
        }
    }

    public void SolutionMode ()
    {
        Cell touchCell = GetCurrentCell ();

        if ( touchCell == null ) return;

        displayTapTiles = true;

        if ( Input.GetKey ( KeyCode.LeftCommand ) )
        {
            displayTapTiles = false;
        }


        if ( Input.GetMouseButtonDown (0))
        {
            PlaceTapTile ( touchCell, currentObjectiveData.solution.Length, true );
        }
    }

    public void BuildMode ()
    {
        if ( cellsEnabled )
        {
            cellsEnabled = false;

//            foreach ( var c in boardView._map )
//            {
//                //FIX
//                //c.Enabled = cellsEnabled;
//            }
        }
        
        if (Input.GetKeyDown(KeyCode.Alpha1))
            currentLayer = LevelEditor.Layer.Piece;
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            currentLayer = LevelEditor.Layer.Item;
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            currentLayer = LevelEditor.Layer.Tile; 

        var cell = GetCurrentCell ();

        if ( cell == null ) 
        {
            RemoveShadows ();

            return;
        }

        if ( Input.GetKey (KeyCode.LeftCommand) )
        {

            RemoveShadows ();
            if ( Input.GetMouseButtonDown (0))
            {
                RemoveAllLayerObjects ( cell );
            }

        }
        else if ( Input.GetMouseButtonDown (0))
        {
            PlaceLayerObject ( cell );
        }
        else if (Input.GetMouseButtonDown(1))
        {
            IncrementCurrentLayer ( cell );
        }
        else if (cell != null)
        {
            RemoveShadows ();
            PlaceShadow ( cell );
        }

    }

    public Cell GetCurrentCell ()
    {
        var raycastResults = GetRaycast ();

        Cell cell = null;

        foreach ( var result in raycastResults )
        {
            if ( result.gameObject.tag == "Cell" )
            {
                cell = result.gameObject.GetComponent < Cell > ();
                break;
            }
        }

        return cell;
    }

    public List < RaycastResult > GetRaycast ()
    {
        PointerEventData pointer = new PointerEventData(EventSystem.current);
        pointer.position = Input.mousePosition;

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointer, raycastResults);

        return raycastResults;
    }

    public string GetKey()
    {
        return string.Format("CurrentLayerAt{0}", currentLayer);
    }

    public int GetCurrentLayerObject()
    {
        return PlayerPrefs.GetInt(GetKey(), 0);
    }

    public void SetCurrentLayerObject(int value)
    {
        PlayerPrefs.SetInt(GetKey(), value);
    }
}
