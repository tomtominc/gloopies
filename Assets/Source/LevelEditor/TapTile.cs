﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TapTile : MonoBehaviour 
{
    public GameObject textPrefab;
    public Sprite normal;
    public Sprite last;

    private Image image
    {
        get { return transform.parent.GetComponent < Image > (); }
    }

    public void PlaceTapOrderLabel ( int n )
    {
        GameObject go = Instantiate < GameObject > (textPrefab);

        go.transform.SetParent ( transform, false );

        Text order = go.GetComponent < Text > ();

        order.text = n.Pretty ();
    }

    public void SetNormal ()
    {
        image.sprite = normal;
    }

    public void SetLast ()
    {
        image.sprite = last;
    }
}
