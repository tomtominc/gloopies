﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using UI.Pagination;

public class CollectionButton : PaginationButton 
{    
    public GameObject newText;
    public Sprite backupSprite;
    public GameObject backFlow;

    private Image backdrop;
    private Image icon;



    private float iconYOffset = 10f;
    private float iconMoveDuration = 0.2f;

    private bool _currentValue;

    public bool CurrentValue 
    {
        get 
        { 
            return _currentValue; 
        }

        set 
        {
             Initialize ();

            _currentValue = value;

            Vector3 endValue = value ?  new Vector3 (0f,iconYOffset) : Vector3.zero;
            icon.rectTransform.DOAnchorPos ( endValue, iconMoveDuration ).Play();
      
            backdrop.DOFade ( value ? 1f : 0f, iconMoveDuration ).Play();
        }
    }

    private void Initialize ()
    {
        backdrop = transform.FindChild ("Backdrop").GetComponent < Image > ();
        icon = transform.FindChild ("Icon").GetComponent < Image > ();
    }

    private void TryActivateNotification ( CollectionGridView view , bool currentMenu )
    {
        if (GameManager.Instance.CurrentMenu == GameManager.Menu.SelectMenu )
            SoundManager.Instance.PlaySoundFX ( "SetPositiveValue" );

        if ( currentMenu )
        {
            ActivateNotification ( false ); 
            return;

        }

        if ( view._currentCollection.cost <= User.coinAmount && view._currentCollection.locked && view._currentCollection.cost > 0 )
        {
            ActivateNotification ( true );   
        }
        else if ( view._currentCollection.gateCost <= User.coinAmount && view._currentCollection.gateLocked ) 
        {
            ActivateNotification ( true );   
        }
        else
        {
            ActivateNotification ( false ); 
        }
    }

    public void ActivateNotification ( bool control )
    {
        backFlow.SetActive ( control );
        newText.SetActive ( control );
    }

    public override void SetInfo ( PageInfo info )
    {
        if ( _currentValue != info.isCurrent )
        {
            CurrentValue = info.isCurrent;
        }

        if ( info.page != null )
        {
            Initialize ();


            var view = info.page.transform.GetComponentInChildren < CollectionGridView > ();

            if ( view == null )
            {
                icon.sprite = backupSprite;
                return;
            }


            if ( view.buttonImage )
            {
                icon.sprite = view.buttonImage;
            }
            else 
            {
                icon.sprite = backupSprite;

            }

            TryActivateNotification ( view , info.isCurrent );
        }
    }
}
