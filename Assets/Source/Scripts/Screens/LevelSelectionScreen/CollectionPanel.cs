﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UI.Pagination;
using System.Collections.Generic;

public class CollectionPanel : MonoBehaviour 
{
    public List < CollectionButton > list = new List<CollectionButton> ();

    private PagedRect pages;
    private CollectionButton[] buttons;

    public void PageChanged ( Page currentPage, Page previousPage )
    {
        pages   = GetComponentInParent < PagedRect > ();
        buttons = GetComponentsInChildren < CollectionButton > ( false );

        for ( int i = 0; i < buttons.Length; i++)
        {
            var button = buttons[i];

            if ( i == pages.CurrentPage && !button.CurrentValue)
            {
                button.CurrentValue = true;
            }
            else if ( button.CurrentValue  && i != pages.CurrentPage )
            {
                button.CurrentValue = false;
            }
        }
    }
}

