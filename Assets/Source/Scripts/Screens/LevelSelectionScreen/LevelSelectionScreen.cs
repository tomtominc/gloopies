﻿using UnityEngine;
using System.Collections;
using PotionSoup.Engine2D;
using PotionSoup.MenuManagement;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using PotionSoup.Persistent;

public class LevelSelectionScreen : MonoBehaviour
{
    public GameObject buttonPrefab;
    public MenuController controller;

    public DynamicButton homeButton;
    public DynamicButton forwardButton;
    public DynamicButton backButton;

    public TextField levelName;

    private int _buttonsPerPage = 20;

    private const string kCurrentCollection = "kCurrentCollection";
    private const string kCurrentPage = "kCurrentPage";

    private int _currentCollection
    {
        get { return PersistentSettings.GetValue (kCurrentCollection, 0 ); }
        set { PersistentSettings.SetValue (kCurrentCollection, value); }
    }
    private int _currentPage
    {
        get { return PersistentSettings.GetValue (kCurrentPage, 0 ); }
        set { PersistentSettings.SetValue (kCurrentPage, value); }
    }

    private GameManager _gameManager
    {
        get { return GameManager.Instance; }
    }

    private LevelManager _levelManager
    {
        get { return _gameManager.LevelManager; }
    }

    private LevelCollection[] Collections
    {
        get { return _levelManager.Collections; }
    }

    private GridLayoutGroup _grid
    {
        get { return GetComponent < GridLayoutGroup > (); }
    }

    private int _totalPages 
    {
        get { return Mathf.CeilToInt ( (_currentState.Count - 1) / _buttonsPerPage ); }
    }

    private int _totalCollections
    {
        get { return Collections.Length; }
    }

    private int _totalLevelsInCollection
    {
        get { return _currentState.Count; }
    }

    private LevelCollection _currentState
    {
        get { return Collections[_currentCollection]; }
    }

    private int _startingLevel
    {
        get { return _currentPage * _buttonsPerPage; }
    }

    private int _createCount
    {
        get { return _startingLevel + (Mathf.Min (_totalLevelsInCollection - _startingLevel, _buttonsPerPage )); }
    }

    public void Initialize ()
    {
        controller.SetInteractability ( true );
        RefreshLayout ();
    }

    public void RefreshLayout ()
    {
        transform.DestroyChildren ();

        RefreshGrid ();

        levelName.Text = _currentState.name;

        for ( int i = _startingLevel; i < _createCount; i++ )
        {
            CreateButton ( _currentCollection, i );
        }

        SetButtonCallbacks ();
    }

    public void RefreshGrid ()
    {
        switch ( _gameManager.deviceType )
        {
            case GameManager.DeviceType.iPhone4:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 48, _grid.cellSize.y );

                break;
            case GameManager.DeviceType.iPhone5:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 48, _grid.cellSize.y );

                break;

            case GameManager.DeviceType.iPad:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 60, _grid.cellSize.y );

                break;

            default:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 60, _grid.cellSize.y );

                break;
                
        }
    }

    private LevelButton CreateButton ( int collection, int levelNumber )
    {
        LevelState state = _levelManager.GetLevelState ( collection, levelNumber );

        if ( state == null )
        {
            Debug.LogFormat ( "Starting Level: {0} Create Count: {1} Total Levels: {2}", _startingLevel, _createCount, _totalLevelsInCollection );
            Debug.LogErrorFormat ( "Trying to get level: {0} from the collection at: {1}.", levelNumber, _currentCollection );

            return null;
        }

        GameObject go = Instantiate < GameObject > ( buttonPrefab );

        go.transform.SetParent ( transform, false );

        LevelButton l = go.GetComponent < LevelButton > ();

        l.Initialize ();

        l.SetData ( _currentCollection, state );

        return l;
    }

    private void SetButtonCallbacks ()
    {
        SetBackButtonCallback ();

        SetForwardButtonCallback ();
    }

    private void SetForwardButtonCallback ()
    {
        if ( _currentCollection >= _totalCollections - 1 && _currentPage >= _totalPages - 1 )
        {
            forwardButton.RemoveAllListeners ();
            forwardButton.gameObject.SetActive ( false );
        }
        else if ( _currentPage < _totalPages )
        {
            forwardButton.gameObject.SetActive ( true );
            forwardButton.RemoveAllListeners ();
            forwardButton.onClick.AddListener ( MoveToNextPage );
        }
        else 
        {
            forwardButton.gameObject.SetActive ( true );
            forwardButton.RemoveAllListeners ();
            forwardButton.onClick.AddListener ( MoveToNextCollection );
        }

    }

    private void SetBackButtonCallback ()
    {
        backButton.gameObject.SetActive ( true );
        homeButton.gameObject.SetActive ( false );

        if ( _currentCollection <= 0 && _currentPage <= 0 )
        {
            backButton.RemoveAllListeners ();
            homeButton.RemoveAllListeners ();
            homeButton.onClick.AddListener ( Close );

            backButton.gameObject.SetActive ( false );
            homeButton.gameObject.SetActive ( true );
        }
        else if ( _currentPage > 0 )
        {
            backButton.RemoveAllListeners ();
            backButton.onClick.AddListener ( MoveToLastPage  );
        }
        else
        {
            backButton.RemoveAllListeners ();
            backButton.onClick.AddListener ( MoveToLastCollection  );
        }


    }

    private void MoveToLastPage ()
    {
        _currentPage -= 1;

        RefreshLayout ();
    }

    private void MoveToLastCollection ()
    {
        _currentCollection -= 1;
        _currentPage = _totalPages;

        RefreshLayout ();
    }

    private void MoveToNextPage ()
    {
        _currentPage += 1;

        RefreshLayout ();
    }

    private void MoveToNextCollection ()
    {
        _currentCollection += 1;
        _currentPage = 0;

        RefreshLayout ();
    }

    private void Close ()
    {
        controller.SetInteractability ( false );
        GameManager.Instance.LoadStartMenu ();
    }
}
