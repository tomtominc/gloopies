﻿using UnityEngine;
using System.Collections;
using PotionSoup.MenuManagement;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;

public class CollectionGridView : MonoBehaviour 
{
    public GameObject buttonPrefab;
    public Sprite buttonImage;
    public Sprite buttonName;

    private int _buttonsPerPage = 25;

    private const string kCurrentCollection = "kCurrentCollection";
    private const string kCurrentPage = "kCurrentPage";

    public LevelCollection _currentCollection;

    private GameManager _gameManager
    {
        get { return GameManager.Instance; }
    }

    private LevelManager _levelManager
    {
        get { return _gameManager.LevelManager; }
    }

    private LevelCollection[] Collections
    {
        get { return _levelManager.Collections; }
    }

    private GridLayoutGroup _grid
    {
        get { return GetComponent < GridLayoutGroup > (); }
    }

    private int _totalPages 
    {
        get { return Mathf.CeilToInt ( (_currentCollection.Count - 1) / _buttonsPerPage ); }
    }

    private int _totalCollections
    {
        get { return Collections.Length; }
    }

    private int _totalLevelsInCollection
    {
        get { return _currentCollection.Count; }
    }


    private int _createCount
    {
        get { return (Mathf.Min (_totalLevelsInCollection, _buttonsPerPage )); }
    }

    public List < CollectionGate > Gates = new List < CollectionGate > ();


    [HideInInspector]
    public List < LevelButton > buttons = new List<LevelButton> ();

    public void RefreshLayout ()
    {
        Gates = transform.parent.GetComponentsInChildren < CollectionGate > (true).ToList ();

        buttons.Clear ();

        RefreshLockState();

        transform.DestroyChildren ();

        RefreshGrid ();

        for ( int i = 0; i < _createCount; i++ )
        {
            CreateButton ( _currentCollection.id , i );
        }

        foreach ( var gate in Gates )
        {
            gate.TryRefresh ();
        }
    }

    public void RefreshGrid ()
    {
        switch ( _gameManager.deviceType )
        {
            case GameManager.DeviceType.iPhone4:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 40f, 55f );

                break;
            case GameManager.DeviceType.iPhone5:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 40f,  74f );

                break;

            case GameManager.DeviceType.iPad:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 40f, 64f );

                break;

            case GameManager.DeviceType.WSVGA:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 40f, 72f );

                break;

            case GameManager.DeviceType.WVGA:

                _grid.constraintCount = 5;
                _grid.cellSize = new Vector2 ( 40f, 70f );

                break;

            default:

            _grid.constraintCount = 5;
            _grid.cellSize = new Vector2 ( 40f, 64f );

                break;

        }
    }

    public void RefreshLockState ()
    {
        if ( _currentCollection.locked && _currentCollection.cost > 0 )
        {
            if ( User.coinAmount >= _currentCollection.cost )
            {
                // display badge icon
            }
        }
        else
        {
            // if the collection is unlocked and the first level isn't unlocked, unlock it.
            LevelState state = _currentCollection.GetLevelState( 0 );

            if ( state.locked )
            {
                state.locked = false;
            }
        }
    }

    public void UnlockLevel ( int level )
    {
        if ( buttons.Count <= level ) 
        {
            Debug.LogWarning ( "Not enough buttons" );
            return;
        }

        buttons [ level ].Unlock ();

        Timer unlock = new Timer ( 2f, () =>
            {
                var state = _currentCollection.GetLevelState ( level );

                state.locked = false;


                RefreshLayout ();

            });

        unlock.Start ( this );

    }

    private LevelButton CreateButton ( int collection, int levelNumber )
    {
        LevelState state = _currentCollection.GetLevelState ( levelNumber );

        if ( state == null )
        {
            Debug.LogFormat ( "Starting Level: {0} Create Count: {1} Total Levels: {2}", 0, _createCount, _totalLevelsInCollection );
            Debug.LogWarningFormat ( "Trying to get level: {0} from the collection at: {1}.", levelNumber, _currentCollection );

            return null;
        }

        GameObject go = Instantiate < GameObject > ( buttonPrefab );

        go.transform.SetParent ( transform, false );

        LevelButton l = go.GetComponent < LevelButton > ();

        l.name = string.Format ( "Level Button {0}", levelNumber );

        l.Initialize ();

        l.SetData ( _currentCollection.id , state );

        buttons.Add ( l );

        return l;
    }
}
