﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using PotionSoup.MenuManagement;
using PotionSoup.Engine2D;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.UI;
using TextFx;
using System;

public class WinScreen : MonoBehaviour
{
    public Image whiteBackground;
    public GameObject background;
    public ObjectiveToggleBox objectiveToggleBox;
    public RectTransform GloopieDisplayTarget;
    public TextFxUGUI clearText;
    public RectTransform objectiveOriginalParent;
    public RectTransform objectiveTempParent;

    public GameObject everyplayShareButton;


    public float whiteFadeDuration = 0.3f;
    public float gloopieMoveDuration = 0.3f;

    public List < Transform > buttons = new List<Transform>();

    private Piece _target;
    private LevelState _state;
    private Cell.SolutionValue _value;
    private RectTransform _toggleBoxRect;

    public void OnGameOver(Dictionary < string, object > table)
    {
        everyplayShareButton.SetActive(false);

        gameObject.SetActive(true);

        _target = table["Gloopie"]    as Piece;
        _state = table["LevelState"]    as LevelState;
        _value = (Cell.SolutionValue)table["SolutionValue"];

        StartCoroutine(DoWinAction());

    }

    public IEnumerator DoWinAction()
    {
        if (_target)
        {
            //_target.transform.SetParent ( GloopieDisplayTarget , true );
            _target.SpriteRenderer.sortingOrder = 102;
            _target.ShadowRenderer.sortingOrder = 101;
        }
        else
            Debug.Log("Target is null");

        _toggleBoxRect = objectiveToggleBox.GetComponent < RectTransform >();

        _toggleBoxRect.SetParent(objectiveTempParent, true);

        whiteBackground.DOFade(1f, whiteFadeDuration).Play();

        yield return new WaitForSeconds(whiteFadeDuration);

        background.SetActive(true);

        whiteBackground.DOFade(0f, whiteFadeDuration).Play();

        clearText.gameObject.SetActive(true);
        clearText.AnimationManager.PlayAnimation();

        _toggleBoxRect.DOAnchorPos(Vector2.zero, gloopieMoveDuration).Play();

        if (_target)
        {
            // FIX
            _target.transform.DOMove(GloopieDisplayTarget.localPosition, gloopieMoveDuration).Play();

        }
        else
        {
            Debug.LogWarning("Target is null, probably a slide tile, addd logic to fix this.");
        }


        yield return new WaitForSeconds(gloopieMoveDuration);

        objectiveToggleBox.ObjectiveComplete(_value);

        DisplayButtons();

        GameManager.Instance.EnableControls();
    }

    public void DisplayButtons()
    {
        for (int i = 0; i < buttons.Count; i++)
        {

            if (_state.levelNumber == 9 && i >= (buttons.Count - 1))
                break;

            Transform button = buttons[i];

            if (User.removedAds && i == 0)
                button.GetComponent < DynamicButton >().DoDisable();

            button.gameObject.SetActive(true);

            button.DOScale(1f, 1f).SetEase(Ease.OutElastic).SetDelay(0.3f * i).Play();
        }

    }

    public void RemoveButtons()
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            Transform button = buttons[i];

            if (User.removedAds && i == 0)
                button.GetComponent < DynamicButton >().DoDisable();

            button.gameObject.SetActive(true);

            button.DOScale(0f, 0.3f).SetEase(Ease.Linear).SetDelay(0.3f * i).Play();
        }
    }

    public void DoRefresh()
    {
        GameManager.Instance.DisableControls();

        clearText.AnimationManager.ContinuePastLoop(ContinueType.Instant);

        _toggleBoxRect.DOAnchorPos(new Vector2(0f, _toggleBoxRect.anchoredPosition.y - 100f), gloopieMoveDuration).OnComplete 
        (
            () =>
            {
                _toggleBoxRect.SetParent(objectiveOriginalParent, false);
                _toggleBoxRect.localPosition = Vector3.zero;
            }
        ).Play();

        RemoveButtons();


    }

    public void ResetCurrentLevel()
    {
        StartCoroutine(DoAction(GameManager.Instance.ResetCurrentLevel));
    }

    public void LoadSelectMenu()
    {
        StartCoroutine(DoAction(GameManager.Instance.LoadSelectMenu));
    }

    public void GoToNextLevel()
    {
        StartCoroutine(DoAction(GameManager.Instance.GoToNextLevel));
    }

    public IEnumerator DoAction(Action action)
    {
        DoRefresh();

        yield return new WaitForSeconds(1.5f);

        action.Invoke();

        if (_target)
        {
            Destroy(_target.gameObject, 2f);
        }
        else
        {
            Debug.LogWarning("Target is null, probably a slide tile, addd logic to fix this.");

        }
    }

}
