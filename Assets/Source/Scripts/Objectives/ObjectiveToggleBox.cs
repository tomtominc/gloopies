﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;

public class ObjectiveToggleBox : MonoBehaviour 
{
    public PrizeToggleBox solutionOne;
    public PrizeToggleBox solutionTwo;
    public PrizeToggleBox solutionThree;

    public GameObject completeIcon;
    public GameObject objectiveText;

    protected LevelState _state;

    private Dictionary < Cell.SolutionValue, PrizeToggleBox > _prizes;

    public void Initialize ( LevelState state )
    {
        _state = state; 

        if ( gameObject == null ) 
        {
            return;
        }

        if ( _state == null )
        {
            Debug.Log ( "State is null!");
        }

        solutionOne  .SetActive   ( false );
        solutionTwo  .SetActive   ( false );
        solutionThree.SetActive   ( false );

        _prizes = new Dictionary<Cell.SolutionValue, PrizeToggleBox> ()
        {
            { Cell.SolutionValue.None, null },
            { Cell.SolutionValue.One, solutionOne },
            { Cell.SolutionValue.Two, solutionTwo },
            { Cell.SolutionValue.Three, solutionThree }
        };


        if ( _state.GetObjectiveCount () <= 0 )
        {
            gameObject.SetActive ( false );

            return;
        }

        gameObject.SetActive ( true );

        if ( _state.AllObjectivesComplete () )
        {

            foreach (var pair in _prizes )
            {
                if (pair.Value == null ) continue;

                pair.Value.SetActive ( false );
            }

            if ( completeIcon == null )
            {
                Debug.Log ( "Complete Icon is null!" );
            }

            else
            {
                completeIcon.SetActive ( true );
            }
  
            if ( objectiveText == null )
            {
                Debug.Log ( "Objective Text is null!" );
            }

            else
            {
                objectiveText.SetActive ( false );
            }



            return;
        }


        if ( objectiveText == null )
        {
            Debug.Log ( "Objective Text is null!" );
        }

        else
        {
            objectiveText.SetActive ( true );
        }



        if ( completeIcon == null )
        {
            Debug.Log ( "Complete Icon is null!" );
        }

        else
        {
            completeIcon.SetActive ( false );
        }

        for ( int i = 0; i < _state.GetObjectiveCount (); i++ )
        {
            Cell.SolutionValue value = (Cell.SolutionValue)(i+1);

            bool objectiveComplete = _state.GetObjectiveIsComplete ( value );

            PrizeToggleBox current = _prizes[ value ];

            if ( current == null ) continue;
            if ( current.toggle == null ) continue;
            if ( current.button == null ) continue;

            current.SetActive ( true );

            current.toggle.isOn = objectiveComplete;
            current.button.SetActive ( !objectiveComplete );
        }
    }

    public void ObjectiveComplete ( Cell.SolutionValue value )
    {

        if ( value == Cell.SolutionValue.None ) return;
        if ( _state.GetObjectiveIsComplete ( value ) ) return;

        StartCoroutine ( DoObjectiveCompleteEffect ( value ) );

    }

    private IEnumerator DoObjectiveCompleteEffect ( Cell.SolutionValue value )
    {
        PrizeToggleBox current = _prizes [value];

        _state.SetObjective ( value, true );

        float duration = 0.3f;
        Ease ease = Ease.InOutQuad;

        current.button.SetActive ( false );
        current.toggle.gameObject.SetActive ( true );

        yield return new WaitForSeconds ( duration );

        current.toggle.isOn = true;

        current.toggle.graphic.rectTransform.SetAsLastSibling ();

        current.toggle.graphic.rectTransform.DOScale (2f, duration ).SetEase (ease ).Play ();

        yield return new WaitForSeconds ( duration );

        current.toggle.graphic.rectTransform.DOScale (1f, duration).SetEase (ease ).Play ();

        // do the coin effect here.

        GameManager.Instance.inventoryManager.DoCoinEffect ( current.transform.position, GameManager.Instance.coinValueMap [ value ] );
    }
}
