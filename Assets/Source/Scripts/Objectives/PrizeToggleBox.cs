﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PrizeToggleBox : MonoBehaviour 
{
    public Toggle toggle;
    public GameObject  button;

    public void SetActive ( bool control )
    {
        gameObject.SetActive ( control );
    }

}
