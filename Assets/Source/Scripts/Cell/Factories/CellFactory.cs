﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CellFactory : Singleton < CellFactory > 
{
    public GameObject cellPrefab;
    public GameObject defaultTile;
    public GameObject defaultItem;
    public GameObject defaultPiece;

    private const string kNullString = "0";

    private const string kPiecesFolder = "Prefab/SpritePrefabs/Pieces";
    private const string kItemsFolder  = "Prefab/SpritePrefabs/Items" ;
    private const string kTilesFolder  = "Prefab/SpritePrefabs/Tiles" ;
    private const string kOtherFolder  = "Prefab/SpritePrefabs/Other" ;
    private const string kAtlasFolder  = "Atlas/GloopiesAtlas";
    private const string kGloopiesAnims= "Atlas/GloopiesAnims";
    private const string kAnimatorsFolder = "Animators";
    
    private Dictionary < string, Sprite >     _atlas;   
    private Dictionary < string, GameObject > _layers;
    private Dictionary < string, GameObject > _prefabs;

    private void Awake ()
    {
        _atlas = kAtlasFolder.LoadAllResources < Sprite > ( true ).ToDictionary ( x => x.name,x => x );
        _atlas.AddRange ( kGloopiesAnims.LoadAllResources < Sprite > ( true ).ToDictionary ( x => x.name, x => x ) );

        _layers = new Dictionary<string, GameObject> ();
        _prefabs = new Dictionary<string, GameObject> ();

        _layers.AddRange ( kPiecesFolder.LoadAllResources  < GameObject > ( true ).ToDictionary ( x => x.GetComponent < Layer > ().jsonKey , x => x ));
        _layers.AddRange ( kItemsFolder .LoadAllResources  < GameObject > ( true ).ToDictionary ( x => x.GetComponent < Layer > ().jsonKey , x => x ));
        _layers.AddRange ( kTilesFolder .LoadAllResources  < GameObject > ( true ).ToDictionary ( x => x.GetComponent < Layer > ().jsonKey , x => x ));

        _prefabs.AddRange( kOtherFolder.LoadAllResources   < GameObject > ( true ).ToDictionary ( x => x.name , x => x  )  );
    }

    public Cell CreateCell (int row, int column )
    {
        Cell cell = Instantiate < GameObject > ( cellPrefab ).GetComponent < Cell > ();
        return cell;
    }

    public Sprite GetSprite ( string key )
    {
        Sprite value;

        if ( _atlas.TryGetValue ( key , out value ) )
        {
            return value;
        }

        Debug.LogWarningFormat ("Trying to get sprite with key {0} is invalid.", key );

        return value;
    }

    public T Create < T >  ( string key ) where T : Layer
    {
        if (string.IsNullOrEmpty (key) || key == kNullString ) return GetDefault < T > ();

        var decode = Decode ( key );

        if ( _layers.ContainsKey( decode ) == false ) return GetDefault < T > ();

        T layer = Instantiate < GameObject > ( _layers [ decode ] ).GetComponent < T > ();

        if ( layer == null )
        {
            Debug.LogWarningFormat ( "Requested Key {0} of type {1} has not been implemented.", key, typeof(T) );

            return GetDefault < T > ();
        }

        layer.SetEncodedKey( key );

        return layer;
    }

    public GameObject GetPrefabTemplate ( string name )
    {
        if ( ! _prefabs.ContainsKey ( name ) )
        {
            Debug.LogWarning ( "No Prefab by that name has been loaded." );
            return null;
        }

        return _prefabs [ name ];
    }

    public T GetDefault < T > () where T : Layer 
    {
//        if ( typeof(T) == typeof (Tile) ) return defaultTile.GetComponent < T > ();
//        if ( typeof(T) == typeof (Item) ) return defaultItem.GetComponent < T > ();
//        if ( typeof(T) == typeof (Piece)) return defaultPiece.GetComponent< T > ();

        return null;
    }

    private string Decode (string key )
    {
       return key.Split(':')[0];
    }
}


