﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;
using com.ootii.Messages;
using com.FDT.EasySpritesAnimation;

public enum Neighbour
{
	Right, Left, Upper, Lower
}

public class Cell : MonoBehaviour

{
    public enum Direction
    {
        None, Left, Right, Up, Down
    }

    public enum SolutionValue
    {
        None, One, Two, Three
    }


    public static Vector2 GetVector ( Direction direction )
    {
        if (direction == Direction.Left ) return Vector2.left;
        if (direction == Direction.Right ) return Vector2.right;
        if (direction == Direction.Up ) return Vector2.up;
        if (direction == Direction.Down ) return Vector2.down;

        return Vector2.zero;
    }

    public static Direction GetOppositeSide ( Direction direction )
    {
        if (direction == Direction.Left ) return Direction.Right;
        if (direction == Direction.Right ) return Direction.Left;
        if (direction == Direction.Up ) return Direction.Down;
        if (direction == Direction.Down ) return Direction.Up;

        return Direction.None;
    }

    protected Dictionary < Direction,  CellData > _neighbours = new Dictionary < Direction, CellData > ();
    protected Dictionary < int, Direction > _neighbourDirections = new Dictionary<int, Direction> ();

    private Tile  _tile;
    private Item  _item;
    private Piece _piece;

    private const string kTileClickAccept  = "Combine";
    private const string kTileClickDeny    = "TileClick_Deny";

    public Tile Tile 
    {
        get { return _tile; }
    }

    public Item Item 
    {
        get { return _item; }
    }

    public Piece Piece
    {
        get { return _piece; }
    }

    protected CellData _data;

    public CellData Data 
    {
        get { return _data; }
    }

    public bool Occupied 
    {
        get 
        {
            if ( _piece == null ) return false;

            return _piece.ConsiderOccupied (); 
        }
    }

    public Vector2 Size 
    {
        get 
        {
            Sprite sprite = spriteRenderer.sprite;

            var cellSize = sprite.bounds.size;

            cellSize.x = cellSize.x * transform.localScale.x;
            cellSize.y = cellSize.y * transform.localScale.y;

            return cellSize;
        }
    }

    public Vector3 position
    {
        get { return transform.position; }
    }

    public Vector3 localPosition
    {
        get { return transform.localPosition; }
    }

    public BoardManager CurrentBoard
    {
        get { return GameManager.Instance.CurrentBoard; }
    }

    public SpriteRenderer spriteRenderer
    {
        get { return GetComponent < SpriteRenderer > (); }
    }

    public SpriteAnimation animator
    {
        get { return GetComponent < SpriteAnimation > (); }
    }

    private SolutionValue _value = SolutionValue.None;

    public SolutionValue solutionValue
    {
        get 
        {
            return _value;
        }
        set 
        {
            _value = value;

            SolutionItem solutionItem = (SolutionItem) CellFactory.Instance.Create < Item > ( "Coin" );

            _data.itemKey = "Coin";

            solutionItem.gameObject.SetActive ( false );
            solutionItem.SetValue ( _value );


            SetItem ( solutionItem );
        }
    }

    public void Initialize ( CellData data )
    {
        this._data = data;

        FindNeighbours();
    }

    public void DoInitialzeEvent ( Layer.KeyType type )
    {
        InitializeEvent e = new InitializeEvent ( this, type );
        CurrentBoard.AddEvent( e ); 
    }

    public void Refresh ()
    {
        SetTile ( CellFactory.Instance.Create<Tile> ( _data.tileKey ));
        SetItem ( CellFactory.Instance.Create<Item> ( _data.itemKey ));
        SetPiece( CellFactory.Instance.Create<Piece>( _data.pieceKey));
    }

    public void OnRefreshFromUndo ()
    {
        if ( _piece != null )
        {
            _piece.OnUndo ();
        }
    }


    private void Update ()
    {
        bool outofdate = false;

        foreach ( var n in _neighbours )
        {
            Cell cell = GetCellFromCoordinate ( new Coordinate(n.Value.row,n.Value.column) );
            if ( cell == null )
            {
                outofdate = true;
                continue;
            }

            Debug.DrawLine( transform.position, cell.transform.position, Color.red );
        }

        if (outofdate) _neighbours.Clear();
    }

    public Cell GetCellFromCoordinate ( Coordinate coor )
    {
        return  CurrentBoard.CurrentBoardLevel.GetCell( coor.row, coor.column );
    }

    public Cell GetNeighbour ( Direction direction )
    {
        if ( _neighbours.ContainsKey ( direction ) )
            return GetCellFromCoordinate ( new Coordinate ( _neighbours [ direction ].row, _neighbours [ direction ].column ) );

        return null;
    }

    public Direction GetDirection ( Cell neighbour )
    {
        if ( _neighbourDirections.ContainsKey ( neighbour.Data.GetHashCode () ) )
        {
            return _neighbourDirections [ neighbour.Data.GetHashCode () ];
        }

        return Direction.None;
    }

    public void Clear ()
    {
        _data = null;
        _piece = null;
        _tile = null;
        _item = null;

        transform.DestroyChildren();
    }

	public void FindNeighbours ()
    {
        if (_neighbours.Count > 0 ) return;

        if ( Data.row + 1 <  CurrentBoard.Board.rows )
        {
            SetNeighbour ( Direction.Down, CurrentBoard.Board.GetCell( _data.row + 1, _data.column ) );
        }

        if ( Data.row - 1 >= 0 )
        {
            SetNeighbour ( Direction.Up, CurrentBoard.Board.GetCell( _data.row - 1, _data.column ) );
        }

        if ( Data.column + 1 <  CurrentBoard.Board.columns )
        {
            SetNeighbour ( Direction.Right, CurrentBoard.Board.GetCell( _data.row, _data.column + 1 ) );   
        }

        if ( Data.column - 1 >= 0 )
        {
            SetNeighbour ( Direction.Left, CurrentBoard.Board.GetCell( _data.row, _data.column - 1 ) );
        }

	}

	protected void SetNeighbour ( Direction direction, CellData data )
	{
		if ( data == null ) return;

        if ( _neighbours.ContainsKey ( direction ) )
        {
            return;
        }

        _neighbours.Add ( direction, data);
        _neighbourDirections.Add ( data.GetHashCode (), direction );

	}

    public void RefreshPieceInPlace ()
    {
        // FIX
//        if ( _piece == null ) return;
//
//        Vector2 position = _piece.rect.anchoredPosition;
//
//        Destroy ( _piece.gameObject );
//
//        _piece = CellFactory.Instance.Create<Piece>( _data.pieceKey);
//
//        _piece.transform.SetParent ( CurrentBoard._pieceGroup.transform, false );
//
//        _piece.rect.anchoredPosition = position;
    }

    public void SetTile ( Tile tile )
    {
        if ( _tile != null )
        {
            Destroy( _tile.gameObject );
        }

        _tile = tile;


        if ( _tile )
        {
            SetLayer ( _tile );

        }
    }

    public void SetItem ( Item item )
    {
        if ( _item != null )
        {
            Destroy( _item.gameObject );
        }

        _item = item;

        if ( _item )
        {
            SetLayer ( _item );

            //HACK

            if ( solutionValue != SolutionValue.None )
                ((SolutionItem)_item).SetValue ( solutionValue );
        }
    }

    public void SetPiece ( Piece piece )
    {
        if ( _piece != null )
        {
            Destroy( _piece.gameObject );
        }

        _piece = piece;

        if ( _piece )
        {
            SetLayer ( _piece );
        }
    }

    public void SetLayer ( Layer layer )
    {
        layer.transform.SetParent ( transform, false );
        layer.transform.localPosition = layer.offset;
        layer.transform.SetAsLastSibling ();
    }

    protected void Hint ()
    {
        if ( _piece ) _piece.OnHint();
        if ( _item )  _item.OnHint (); 
        if ( _tile )  _tile.OnHint ();

    }

    protected void Normal ()
    {
        if ( _piece ) _piece.OnNormal();
        if ( _item  ) _item.OnNormal (); 
        if ( _tile  ) _tile.OnNormal ();

    }

    protected bool TryToOccupy ( List < Cell > removals )
    {
        if ( _piece ) 
        {
            if ( ! _piece.TryToOccupy ( removals ) )
            {
                return false;
            }
        }
        if ( _item  )
        {
            if ( ! _item.TryToOccupy  ( removals ) )
            {
                return false;
            }
        }
        if ( _tile  )
        {
            if ( ! _tile.TryToOccupy  ( removals ) )
            {
                return false;
            }
        }

        return true;
    }

    public void EndTurn ()
    {
        if ( _piece ) _piece.EndTurn (this);
        if ( _item  ) _item .EndTurn (this); 
        if ( _tile  ) _tile .EndTurn (this);

    }

    protected Dictionary < Piece.Type, List < CellData > > GetSorted ()
    {
        var collection = new Dictionary<Piece.Type, List<CellData>> ();

        foreach ( var data in _neighbours )
        {
            Piece.Type type = data.Value.PieceType;

            if ( collection.ContainsKey( type ) ) 
            {
                collection [ type ].Add ( data.Value );
            }
            else 
            {
                collection.Add( type, new List < CellData > () { data.Value } ); 
            }
        }

        return collection;
    }

//	public void OnPointerEnter ( PointerEventData data )
//    {
//        var collection = GetSorted();
//
//        int tryCount = 0;
//
//        foreach ( var pair in collection )
//        {
//            if ( pair.Value.Count < 2 ) continue;
//
//            if ( pair.Key == Piece.Type.NONE ) continue;
//
//            tryCount++;
//
//            if ( tryCount > 1 ) Debug.LogWarning( "more than 1 type is similar around the cell, this is an invalid rule!");
//
//            foreach ( var cell in pair.Value )
//            {
//                Cell c = GetCellFromCoordinate( new Coordinate ( cell.row, cell.column ) ); 
//                c.Hint();
//            }
//        }
//
//	}
//
//    public void OnPointerExit (PointerEventData data)
//    {
//        foreach ( var cellData in _neighbours )
//        {
//            Cell cell = GetCellFromCoordinate( new Coordinate ( cellData.Value.row, cellData.Value.column ) ); 
//            cell.Normal();
//        }
//    }

	public void OnPointerClick ()
	{
        if ( Occupied )
        {
            Piece.OnPressed ();
            return;
        }

        var collection = GetSorted();

        List < Cell > removals = new List<Cell> ();
        int tryCount = 0;
        Piece.Type combinetype = Piece.Type.None;

        foreach ( var pair in collection )
        {
            if ( pair.Value.Count < 2 ) continue;

            if ( pair.Key == Piece.Type.None ) continue;

            combinetype = pair.Key;

            tryCount++;

            if ( tryCount > 1 ) Debug.LogWarning( "more than 1 type is similar around the cell, this is an invalid rule!");

            foreach ( var cell in pair.Value )
            {
                removals.Add(GetCellFromCoordinate( new Coordinate (cell.row,cell.column))); 
            }
        }

        if ( tryCount > 1 ) return;

        if (removals.Count > 1 && TryToOccupy ( removals ) == true )
        {
            SoundManager.Instance.PlayRandomSoundFX ( string.Format ("{0}_{1}", combinetype,  kTileClickAccept ) );

            CurrentBoard.RegisterMatch( this, removals );
        }
        else 
        {
            SoundManager.Instance.PlaySoundFX( kTileClickDeny );
        }
	}

    public override string ToString()
    {
        return string.Format("[Cell ( {3} , {4} ), Tile={0}, Item={1}, Piece={2}", Tile, Item, Piece, Data.row, Data.column );
    }


}
