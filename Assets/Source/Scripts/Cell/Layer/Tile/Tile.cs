﻿using UnityEngine;
using System.Collections;
using PotionSoup.MenuManagement;
using System.Collections.Generic;

public class Tile : Layer
{
    public enum Type
    {
        NONE,
        RENDER
    }

    public virtual bool IsMarker 
    {
        get { return false; }
    }

    public virtual Sprite TileSprite
    {
        get { return null; }
    }
    public override void OnGameOver()
    {
        
    }

    public Type type { get; set; }

    protected override void SetKey ( string key )
    {
        this.type = LayerUtils.DecodeTile(EncodedKey);
    }

    public override BoardEvent GetEvent(Cell target)
    {
        return new RefreshEvent ( target );
    }

    public override void OnHint()
    {
        // TODO: hint animation
    }

    public override void OnNormal()
    {
        // TODO: set back to normal 
    }

    public override void SetToFront()
    {
        transform.SetParent( MenuManager.Instance.transform, true );
        transform.SetAsLastSibling();
    }

    public override void OnPressed()
    {
        
    }

    public override bool TryToOccupy(List<Cell> removals)
    {
        return true;
    }

    public override bool CanBeOccupied(Cell cell)
    {
        return true;
    }

    public override BoardEvent GetSlideEvent(Cell target)
    {
        return null;
    }

    public override Cell.Direction GetSlideDirection(Cell.Direction current)
    {
        return current;
    }

    public override void EndTurn(Cell cell)
    {
        
    }
} 
