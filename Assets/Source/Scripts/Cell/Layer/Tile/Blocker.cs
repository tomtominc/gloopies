﻿using UnityEngine;
using System.Collections;

public class Blocker : Tile 
{

    protected override void SetKey(string key)
    {
        
    }

    public override BoardEvent GetEvent(Cell target)
    {
        return null;
    }

    public override bool CanBeOccupied(Cell cell)
    {
        return false;
    }
}
