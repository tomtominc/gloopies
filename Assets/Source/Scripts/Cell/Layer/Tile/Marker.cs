﻿using UnityEngine;
using System.Collections;

public class Marker : Tile 
{
    private const string kTilePerfect = "Tile_Perfect";

    public override bool IsMarker
    {
        get
        {
            return true;
        }
    }

    public override Sprite TileSprite
    {
        get
        {
            return CellFactory.Instance.GetSprite ( kTilePerfect );
        }
    }
}
