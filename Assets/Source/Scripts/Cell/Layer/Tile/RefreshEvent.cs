﻿using UnityEngine;
using System.Collections;

public class RefreshEvent : BoardEvent
{
    public RefreshEvent ( Cell target )
    {
        this.target = target;
    }

    public override float Resolve()
    {
        target.Refresh ();

        return 0f;
    }
}
