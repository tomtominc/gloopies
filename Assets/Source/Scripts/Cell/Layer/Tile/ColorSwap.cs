﻿using UnityEngine;
using System.Collections;
using PotionSoup.Engine2D;
using com.FDT.EasySpritesAnimation;
using System;

public class ColorSwap : Tile 
{
    private Piece.Type typeA;
    private Piece.Type typeB;

    public override string[] modifiers
    {
        get
        {
            return new string[3]
            {
              ":GP:BP", ":GP:RP", ":BP:RP"  
            };
        }
    }


    public override Sprite TileSprite
    {
        get
        {
            return CellFactory.Instance.GetSprite ( EncodedKey );
        }
    }

    protected override void SetKey(string key)
    {
        base.SetKey(key);

        var split = key.Split(':');

        if (split.Length < 3 ) Debug.LogError("Not a valid color swap string! need to put CS:x:y, where x is the first color and y is the second");

        typeA = Piece.TypeByKey [ split[1] ];
        typeB = Piece.TypeByKey [ split[2] ];

        SetAnimation ( EncodedKey );
    }

    public override BoardEvent GetSlideEvent(Cell target)
    {
        return new SliderColorChangeEvent ( target , typeA , typeB );
    }

    public override BoardEvent GetEvent(Cell target)
    {
        return new ColorSwapEvent ( target, typeA , typeB );
    }
}

public class SliderColorChangeEvent : BoardEvent 
{
    Piece.Type typeA;
    Piece.Type typeB;

    public SliderColorChangeEvent ( Cell target, Piece.Type typeA, Piece.Type typeB )
    {
        this.target= target;
        this.typeA = typeA ;
        this.typeB = typeB ;
    }

    public override float Resolve()
    {
        ColorSwapEvent.DoEvent ( target , typeA , typeB ,  
            () => 
            {
                target.RefreshPieceInPlace ();
                target.Piece.RefreshPiece ();

            } );

        return target.Piece.GetCurrentAnimationLength ();
    }
}

public class ColorSwapEvent : BoardEvent
{
    Piece.Type typeA;
    Piece.Type typeB;

    public ColorSwapEvent ( Cell target, Piece.Type typeA , Piece.Type typeB )
    {
        this.target = target;
        this.typeA = typeA ;
        this.typeB = typeB ;
    }

    public override float Resolve()
    {
        ColorSwapEvent.DoEvent ( target , typeA , typeB ,  
            () => 
            {
                target.Refresh ();

                if ( target.Piece )
                    target.Piece.RefreshPiece ();

            } );

        return 0.5f;
    }

    public static void DoEvent ( Cell target , Piece.Type typeA , Piece.Type typeB , Action Finish )
    {
        Board board =  GameManager.Instance.CurrentBoard.Board;

        Piece.Type colorTo =   target.Data.PieceType == typeA ? typeB : typeA ;
        Piece.Type colorFrom = colorTo == typeA ? typeB : typeA;

        target.Data.pieceKey = Piece.KeyByType [ colorTo ];
        board.SetCell( target.Data.row, target.Data.column, target.Data );

        SoundManager.Instance.PlaySoundFX ( "Riser1" );
        target.Tile.SetAnimation ( string.Format ( "Swap_{0}_{1}", typeA , typeB ) );
        target.Piece.OnColorChange ( colorFrom, colorTo );

        Action < Piece >  handler = null;

        handler = ( x ) => 
            {
                SoundManager.Instance.PlaySoundFX ( "GetHit1" );

                Finish.Invoke ();
            };

        target.Piece.OnFinishedTransitionQueue += handler;
    }
}
