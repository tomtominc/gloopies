﻿using UnityEngine;
using System.Collections;

public class Target : Tile 
{
    public override string[] modifiers
    {
        get
        {
            return new string[2]
            {
              ":1", ":2"  
            };
        }
    }
    protected override void SetKey(string key)
    {
        var decode = key.Split(':');

        if (decode.Length < 2 ) Debug.LogError("Not a valid target string! need to put T:x, where x is a matching value.");

        name = string.Format(key);

        sprite = CellFactory.Instance.GetSprite ( name );
    }
}
