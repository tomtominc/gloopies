﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System.IO;
using System.Linq;

public class SliderTile : Tile 
{
    
    public Cell.Direction _direction;

    public Dictionary < string , Cell.Direction > directionMap= new Dictionary<string, Cell.Direction> ()
    {
        {"L", Cell.Direction.Left },
        {"R", Cell.Direction.Right},
        {"D", Cell.Direction.Down },
        {"U", Cell.Direction.Up   }
    };

    public override string[] modifiers
    {
        get
        {
            return new string[4]
            {
              ":L", ":R", ":D", ":U"  
            };
        }
    }

    protected override void SetKey(string key)
    {
        string[] decode = key.Split (':');

        if ( decode.Length < 2 ) Debug.LogError ( "The input key for the slider is not valid, should be in format SL:DIRECTION where DIRECTION insert R,L,U,D.");

        _direction = directionMap [ decode[1] ];
        string sliderAnim = string.Format ( "{0}_Idle", _direction );
        Animator.Play ( sliderAnim );
    }


    public override Cell.Direction GetSlideDirection(Cell.Direction current)
    {
        return _direction;
    }

    public override BoardEvent GetEvent(Cell target)
    {
        return new SliderEvent ( target, _direction );
    }

}

public class SliderEvent : BoardEvent
{
    private Cell.Direction _direction;
    private List < Node > _currentPath;
    private Cell _next;
    private Tweener pathTween;

    private float pointDuration = 0.1f;

    public SliderEvent ( Cell target, Cell.Direction direction )
    {
        this.target = target;

        _direction = direction;
        _next = this.target;

    }

    public override float Resolve()
    {
        _currentPath = new List < Node > ();

        while ( _direction != Cell.Direction.None )
        {
            Cell cell = _next.GetNeighbour ( _direction );

            Node node = new Node ();

            if ( cell == null )
            {
                _direction = Cell.Direction.None;
                break;
            }

            if ( cell.Data.PieceType != Piece.Type.None )
            {
                _direction = Cell.Direction.None;
                break;
            }

            node.cell = cell;

            if ( cell.Tile != null )
            {
                bool valid = cell.Tile.CanBeOccupied ( _next );

                if ( valid == false )
                {
                    _direction = Cell.Direction.None;
                    break;
                }
                else
                {
                    _direction = cell.Tile.GetSlideDirection ( _direction );

                    BoardEvent e_tile = cell.Tile.GetSlideEvent ( target );

                    if ( e_tile != null )
                    {
                        node.boardEvent.Add ( e_tile );
                    }
                }

            }

            node.direction = _direction;

            _next = cell;

            _currentPath.Add ( node );

        }

        Transform transform = target.Piece.transform;

        float duration = pointDuration * ( _currentPath.Count + 1 );

        if ( _currentPath.Count > 0 )
        {
            target.Piece.OnSlide ( _currentPath[0].direction );

            pathTween =  transform.DOPath ( _currentPath.Select ( x => x.cell.position + (Vector3) ( target.Piece.offset * 2f) ).ToArray () , duration , PathType.Linear , PathMode.Ignore, 10, Color.red )
                .OnWaypointChange ( OnWaypointChange )
                .SetEase (Ease.Linear);
            
            pathTween.Play ();
        }

        return duration;
    }

    private void OnWaypointChange ( int p )
    {  
        int point = p - 1;

        if ( point >= _currentPath.Count || point <= -1 ) return;

        Node node = _currentPath [ point ];

        if ( node == null ) return;

        bool lastPoint = point == _currentPath.Count - 1;
        float resolve = 0f;

        if ( node.direction != Cell.Direction.None )
            target.Piece.OnSlide ( node.direction );


        foreach ( var e in node.boardEvent )
        {
            if ( e == null ) continue;
            resolve =  e.Resolve ();
            target.StartCoroutine (  ResolveEvent (resolve, p ) );

        }

        if ( lastPoint )
        {

            target.StartCoroutine ( EndPath ( node, resolve ) );
        }
    }

    public IEnumerator ResolveEvent ( float delay, int currentPoint )
    {
        pathTween.timeScale = 0f;
        yield return new WaitForSeconds ( delay );


        List < Vector3 > points = new List < Vector3 > ();

        for ( int i = currentPoint; i < _currentPath.Count; i++ )
        {
            points.Add ( _currentPath [ i ].cell.position );    
        }

        if ( currentPoint > 0 )
            _currentPath.RemoveRange ( 0, currentPoint );

        if ( points.Count > 0 )
        {
            float duration = pointDuration * ( points.Count + 1 );

            pathTween = target.Piece.transform.DOPath ( points.ToArray () , duration , PathType.Linear , PathMode.Ignore, 10, Color.red )
                .OnWaypointChange ( OnWaypointChange )
                .SetEase (Ease.Linear);

            pathTween.Play ();
        }

        pathTween.timeScale = 1f;
    }

    private IEnumerator EndPath ( Node node, float delay )
    {
        yield return new WaitForSeconds (delay);

        Board board =  CurrentBoard.Board;

        node.cell.Data.pieceKey = target.Data.pieceKey;

        board.SetCell( node.cell.Data.row, node.cell.Data.column, node.cell.Data );

        target.Data.pieceKey = "0";
        board.SetCell( target.Data.row, target.Data.column, target.Data);
        node.cell.Refresh();

        node.cell.Piece.RefreshPiece ();

        target.Refresh();
    }

    private Cell.Direction GetDirection (string key)
    {
        if ( key == "U") return Cell.Direction.Up;
        if ( key == "D") return Cell.Direction.Down;
        if ( key == "R") return Cell.Direction.Right;
        if ( key == "L") return Cell.Direction.Left;

        return Cell.Direction.None;
    }

    public class Node
    {
        public Cell cell;
        public List < BoardEvent > boardEvent = new List < BoardEvent > ();
        public Cell.Direction direction;
    }
}
