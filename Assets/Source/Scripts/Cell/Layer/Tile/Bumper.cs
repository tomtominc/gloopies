﻿using UnityEngine;
using System.Collections;

public class Bumper : Tile 
{
    public enum BumperDirection
    {
        None, TopRight, TopLeft, BottomRight, BottomLeft
    }

    protected BumperDirection _direction;
    public override bool CanBeOccupied(Cell cell)
    {
        return true;
    }

    public override string[] modifiers
    {
        get
        {
            return new string[4]
            {
              ":BL", ":TL", 
              ":BR", ":TR"
            };
        }
    }

    protected override void SetKey(string key)
    {
        string[] decode = key.Split (':');

        if ( decode.Length < 2 ) Debug.LogError ( "The input key for the bumper is not valid, should be in format BM:DIRECTION where DIRECTION insert BR,BL,TR,TL.");

        string side = decode[1];

        _direction = GetDirection ( side );

        string animKey = string.Format ( "{0}_Idle", _direction );

        Animator.Play ( animKey );
    }

    public override Cell.Direction GetSlideDirection(Cell.Direction current)
    {
        if ( _direction == BumperDirection.TopRight )
        {
            if ( current == Cell.Direction.Up) return Cell.Direction.Left;
            if ( current == Cell.Direction.Right ) return Cell.Direction.Down;
        }
        else if ( _direction == BumperDirection.TopLeft )
        {
            if ( current == Cell.Direction.Up) return Cell.Direction.Right;
            if ( current == Cell.Direction.Left ) return Cell.Direction.Down;
        }
        else if ( _direction == BumperDirection.BottomLeft )
        {
            if ( current == Cell.Direction.Down) return Cell.Direction.Right;
            if ( current == Cell.Direction.Left ) return Cell.Direction.Up;
        }
        else if ( _direction == BumperDirection.BottomRight )
        {
            if ( current == Cell.Direction.Down) return Cell.Direction.Left;
            if ( current == Cell.Direction.Right ) return Cell.Direction.Up;
        }

        return Cell.Direction.None;
    }

    public override BoardEvent GetSlideEvent(Cell target)
    {
        return null;
    }

    protected virtual BumperDirection GetDirection (string key)
    {
        if ( key == "TR" ) return BumperDirection.TopRight;
        if ( key == "TL" ) return BumperDirection.TopLeft;
        if ( key == "BR" ) return BumperDirection.BottomRight;
        if ( key == "BL" ) return BumperDirection.BottomLeft;

        return BumperDirection.None;
    }
}
