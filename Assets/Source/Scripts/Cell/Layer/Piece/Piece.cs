﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using PotionSoup.MenuManagement;
using DG.Tweening;
using com.FDT.EasySpritesAnimation;
using System;

public class Piece : Layer
{
    public enum Type
    {
        None, Green, Blue, Red
    }

    public class AnimationParamaters 
    {
        public string animationState;
        public object[] arguments;
    }

    public GameObject emitter;

    private float idleActionInterval = 1.0f;

    public virtual Type Color 
    { 
        get; protected set; 
    }

    public Transform Icon
    {
        get; set;
    }

    public Transform Shadow
    {
        get; set;
    }

    public SpriteRenderer ShadowRenderer
    {
        get; set;
    }

    public Cell Cell
    {
        get { return GetComponentInParent < Cell > (); }
    }

    public Cell.SolutionValue Value
    {
        get 
        {
            if ( Cell == null ) return Cell.SolutionValue.None;

            return Cell.solutionValue;
        }
    }

    public event Action < Piece > OnFinishedTransitionQueue;


    protected const string kInitState            = "{0}_OnInit" ;
    protected const string kSpawnState           = "{0}_OnSpawn" ;

    protected const string kOnStartupState       = "{0}_OnStart_{1}";
    protected const string kOnIdleState          = "{0}_OnIdle_{1}";
    protected const string kOnPressed            = "{0}_OnPressed_{1}";
    protected const string kOnIdleActionState    = "{0}_OnIdleAction_{1}";
    protected const string kColorFromState       = "OnColor_From_{0}";
    protected const string kColorToState         = "OnColor_To_{0}";
    protected const string kCombineState         = "{0}_OnCombine";
    protected const string kSlideState           = "{0}_OnSlide_{1}";
    protected const string kOnWinState           = "{0}_OnWin_{1}";

    public static Dictionary < Type, string > KeyByType = new Dictionary<Type, string> ()
    {
        { Type.None , "0"  },
        { Type.Green, "GP" },
        { Type.Blue,  "BP" },
        { Type.Red ,  "RP" }
    };

    public static Dictionary < string , Type > TypeByKey = new Dictionary<string, Type> ()
    {
        { "0" , Type.None },
        { "GP", Type.Green},
        { "BP", Type.Blue },
        { "RP", Type.Red  }
    };

    protected Action<SpriteAnimation, SpriteAnimationData> cacheTransitionAction;


    private void Awake ()
    {
        Icon = transform.FindChild ( "Icon" );
        Shadow = transform.FindChild ( "Shadow" );

        ShadowRenderer = Shadow.GetComponent < SpriteRenderer > ();

        SpriteRenderer.sortingOrder = 5;
        ShadowRenderer.sortingOrder = 4;

    }

    private void Start ()
    {
        OnStart ();
    }

    protected virtual void OnStart ()
    {
        idleActionInterval = UnityEngine.Random.Range ( 1f, 5f );
    }

    public void Update ()
    {
        OnUpdate ();
    }

    public virtual void OnUpdate ()
    {
        if ( Value != Cell.SolutionValue.None ) return;

        idleActionInterval -= Time.deltaTime;

        if ( idleActionInterval <= 0f  )
        {
            if (IsAnimationEqualTo ( kOnIdleState , Color , Value ) )
            {
                PlayAnimation ( kOnIdleActionState , Color, Value );

                DoAnimationTransition
                (
                    new Queue < AnimationParamaters > 
                    ( 
                        new[] 
                        { 
                            new AnimationParamaters () { animationState = kOnIdleState       , arguments = new object[] { Color , Value } }
                        }
                    )
                );

            }

            idleActionInterval = UnityEngine.Random.Range ( 5f, 10f );
        }

    }

    protected override void SetKey(string key)
    {
        Color = TypeByKey [ key ];
    }

    public override void OnGameOver ()
    {

    }

    public override void OnHint()
    {

    }

    public override void OnNormal()
    {

    }

    public override void OnPressed ()
    {
        if ( IsAnimationEqualTo ( kOnPressed , Color , Value ) || Value != Cell.SolutionValue.None )
        {
            return;
        }

        PlayAnimation ( kOnPressed , Color , Value );

        RefreshPiece ();
    }

    public override bool CanBeOccupied(Cell cell)
    {
        return false;
    }

    public virtual bool ConsiderOccupied ()
    {
        return false;
    }

    public virtual void OnCombine ( Cell cell, Cell[] removals )
    {
        var go = Instantiate < GameObject > ( emitter  );

        go.transform.position = transform.position;

        var bounceEmitter = go.GetComponent < TopDown2DBounceEmitter > ();

        Vector2 force = Vector2.zero;

        foreach ( var removal in removals )
        {
            force += Cell.GetVector ( removal.GetDirection ( cell ) );    
        }

        bounceEmitter.Emit ( force , transform );

        PlayAnimation ( kCombineState , Color );

        RefreshPiece ();
    }

    public virtual void OnColorChange ( Type From, Type To )
    {
        DoAnimationTransition 
        ( 
            new Queue < AnimationParamaters > 
            ( 
                new[] 
                { 
                    new AnimationParamaters () { animationState = kColorFromState , arguments = new object[] { From } },
                    new AnimationParamaters () { animationState = kColorToState   , arguments = new object[] { To   } }
                }
            )
        );
    }

    public override BoardEvent GetSlideEvent(Cell target)
    {
        return null;
    }

    public override Cell.Direction GetSlideDirection(Cell.Direction current)
    {
        return current;
    }

    public virtual void OnSlide ( Cell.Direction direction )
    {
        PlayAnimation ( kSlideState , Color , direction );
    }

    public virtual bool IsAnimationEqualTo ( string animationState , params object[] args  )
    {
        if ( GetCurrentAnimation ().IsNullOrEmpty () ) return false;

        return GetCurrentAnimation ().Equals ( string.Format ( animationState , args ) );
    }

    public virtual string GetCurrentAnimation ()
    {
        return Animator.currentAnimationName;
    }

    public float GetCurrentAnimationLength ()
    {
        SpriteAnimationData data;

        if ( Animator.animationsByName.TryGetValue ( Animator.currentAnimationName, out data ) )
        {
            return data.newFramesTime * data.frameDatas.Count;
        }

        return 0f;

    }

    public override float DoInitalEnter()
    {
        return 0f;
    }

    public override BoardEvent GetEvent(Cell target)
    {
        return null;
    }

    public void PlayAnimation ( string animationState , params object[] args )
    {
        string animation = string.Format ( animationState, args );

        if ( HasBuiltInShadow ( animation ) )
        {
            ShadowRenderer.gameObject.SetActive ( false );
        }

        Animator.Play ( animation  );
    }

    public bool HasBuiltInShadow ( string animationState )
    {
        if ( animationState.Equals ( string.Format ( "{0}_OnWin_None" , Color ) ) )
        {
            return true;
        }

        return false;
    }

    public void DoAnimationTransition ( Queue < AnimationParamaters > animations )
    {
        if ( animations.Count <= 0 )
        {
            return;
        }

        if ( Animator.FinishedInvokeCount > 0 )
        {
            Animator.OnFinishAnimation -= cacheTransitionAction;
        }

        cacheTransitionAction = (x,y) => 
            {
                Animator.OnFinishAnimation -= cacheTransitionAction;

                var anim = animations.Dequeue ();

                PlayAnimation ( anim.animationState , anim.arguments );

                if ( animations.Count > 0 )
                {
                    DoAnimationTransition ( animations );
                }
                else
                {
                    Action < SpriteAnimation , SpriteAnimationData >  handler = null;

                    handler = (v , z ) => 
                        {
                            Animator.OnFinishAnimation -= handler;

                            if ( OnFinishedTransitionQueue != null )
                            {
                                OnFinishedTransitionQueue ( this );
                            }  
                        };
                    
                    Animator.OnFinishAnimation += handler;
                }
            };


        Animator.OnFinishAnimation += cacheTransitionAction;
    }

    public virtual void RefreshPiece ()
    {
        if ( Animator.currentAnimationIdx > -1 )
        {
            string nextState = CurrentBoard.IsOnlyOnePieceLeft () ? kOnWinState : kOnIdleState;

            DoAnimationTransition
            (
                new Queue < AnimationParamaters > 
                ( 
                    new[] 
                    { 
                        new AnimationParamaters () { animationState = kOnStartupState , arguments = new object[] { Color , Value } },
                        new AnimationParamaters () { animationState = nextState       , arguments = new object[] { Color , Value } }
                    }
                )
            );
        }
        else
        {
            PlayAnimation ( kOnStartupState , Color , Value );

            string nextState = CurrentBoard.IsOnlyOnePieceLeft () ? kOnWinState : kOnIdleState;

            DoAnimationTransition  
            (
                new Queue < AnimationParamaters > 
                ( 
                    new[] 
                    { 
                        new AnimationParamaters () { animationState = nextState , arguments = new object[] { Color , Value } }
                    }
                )
            );
        }
    }



    public virtual void OnUndo ()
    {
        RefreshPiece ();
    }

    public override void SetToFront()
    {
        transform.SetParent( MenuManager.Instance.transform, true );
        transform.SetAsLastSibling();
    }

    public override bool TryToOccupy(List<Cell> removals)
    {
        return true;
    }

    public override void EndTurn( Cell cell)
    {

    }

}
