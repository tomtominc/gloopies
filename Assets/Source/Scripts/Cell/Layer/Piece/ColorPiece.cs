﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using com.FDT.EasySpritesAnimation;

public class ColorPiece : Piece 
{
    public override float DoInitalEnter()
    {
        float duration = 0.2f;

        PlayAnimation ( kInitState , Color );

        Icon.DOLocalMove ( new Vector3 (offset.x, 20f, 0f ), 0.5f ).From ().OnComplete 
        (
            () =>
            {
                PlayAnimation ( kSpawnState , Color );

                RefreshPiece ();

                SoundManager.Instance.PlayRandomSoundFX ( "Squish" );

                SoundManager.Instance.PlaySoundFX ( "Bubble_Drop" );

            }
        ).Play ();

        return duration;
    }

    public void OnCombineSound ( float delay )
    {
        string combineGroup = string.Format ( "{0}_Combine", Color );

        SoundManager.Instance.PlayRandomSoundFX (combineGroup, delay );
    }

    public override bool ConsiderOccupied()
    {
        return true;
    }
}
