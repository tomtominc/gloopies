﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

public class ColorSwapPiece : ColorPiece 
{   
    protected Piece.Type _primary;
    protected Piece.Type _secondary;

    public override string[] modifiers
    {
        get
        {
            return new string[6]
            {
                ":GP:BP", ":GP:RP", 
                ":RP:GP", ":RP:BP",
                ":BP:GP", ":BP:RP"
            };
        }
    }

    public const string kOnPulseState = "Pulse_{0}_{1}_{2}";

    public override Type Color
    {
        get
        {
            return _primary;
        }
        protected set
        {
            base.Color = value;
        }
    }

    protected override void SetKey(string key)
    {
        SetColor ( key );
    }

    protected virtual void SetColor ( string key )
    {
        string[] code = key.Split(':');

        if ( code.Length < 3 ) Debug.LogErrorFormat ("Trying to use color piece with invalid key: {0}", key );

        _primary   = Piece.TypeByKey [ code[1] ];
        _secondary = Piece.TypeByKey [ code[2] ];
    }

    public override void RefreshPiece()
    {
        if ( Animator.currentAnimationIdx > -1 )
        {
            DoAnimationTransition
            (
                new Queue < AnimationParamaters > 
                ( 
                    new[] 
                    { 
                        new AnimationParamaters () { animationState = kOnStartupState , arguments = new object[] { Color , Value } },
                        new AnimationParamaters () { animationState = kOnPulseState       , arguments = new object[] { _primary , _secondary , Value } }
                    }
                )
            );
        }
        else
        {
            PlayAnimation ( kOnStartupState , Color , Value );

            DoAnimationTransition  
            (
                new Queue < AnimationParamaters > 
                ( 
                    new[] 
                    { 
                        new AnimationParamaters () { animationState = kOnPulseState , arguments = new object[] { _primary , _secondary , Value } }
                    }
                )
            );
        }
    }

    public override void EndTurn(Cell cell)
    {
        if ( cell.Data.PieceType == Piece.Type.None )
        {
            return;
        }


        cell.Data.pieceKey = string.Format ( "CSP:{0}:{1}" , KeyByType [ _secondary ] , KeyByType [ _primary ] );

        OnColorChange ( _primary , _secondary );

        Action < Piece > handler = null;

        handler = (x) =>
            {
                OnFinishedTransitionQueue -= handler;
                cell.Refresh ();

                if ( cell.Piece )
                    cell.Piece.RefreshPiece ();
            };
        
        OnFinishedTransitionQueue += handler;
    }

    public override bool ConsiderOccupied()
    {
        return true;
    }
}
