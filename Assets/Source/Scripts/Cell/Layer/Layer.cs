﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using com.FDT.EasySpritesAnimation;
using DG.Tweening;

public abstract class Layer : MonoBehaviour
{
    public string jsonKey = "0";
    public Vector2 offset;

    public enum KeyType
    {
        TILE, ITEM, PIECE
    }

    public string EncodedKey { get; set; }

    public virtual string[] modifiers
    {
        get { return new string[0]; }
    }

    public BoardManager CurrentBoard
    {
        get { return GameManager.Instance.CurrentBoard; }
    }

    public void SetEncodedKey ( string key )
    {
        EncodedKey = key;
        SetKey(key); 
    }

    private SpriteRenderer _spriteRenderer;

    public SpriteRenderer SpriteRenderer
    {
        get 
        {
            if ( _spriteRenderer == null )
            {

                _spriteRenderer = transform.GetComponentInChildren < SpriteRenderer > ("Icon", true );
            }

            return _spriteRenderer;
        }
        
    }

    public Sprite sprite
    {
        get { return SpriteRenderer.sprite; }
        set { SpriteRenderer.sprite = value; }
    }

    protected SpriteAnimation _animator;


    public SpriteAnimation Animator
    {
        get 
        { 
            if ( _animator == null )
            {
                _animator = transform.GetComponentInChildren < SpriteAnimation > ("Icon", true); 
            }

            return _animator;
        }
    }

    public virtual void SetAnimation ( string animation )
    {
        Animator.Play ( animation );
    }

    private SpriteRenderer _whiteOverlayRenderer;

    public SpriteRenderer WhiteOverlayRenderer
    {
        get 
        {
            if ( _whiteOverlayRenderer == null )
            {
                var go = CellFactory.Instance.GetPrefabTemplate ( "WhiteRenderer" );

                var whiteObject = Instantiate < GameObject > ( go );

                whiteObject.transform.SetParent ( transform , false );

                whiteObject.transform.localPosition = Vector3.zero;
                whiteObject.transform.localScale = Vector3.one;
                whiteObject.transform.rotation = transform.rotation;

                _whiteOverlayRenderer = whiteObject.GetComponent < SpriteRenderer > ();

                _whiteOverlayRenderer.sprite = sprite;
                _whiteOverlayRenderer.sortingOrder = SpriteRenderer.sortingOrder + 1;
            }

            return _whiteOverlayRenderer;
        }
    }

    public virtual float DoInitalEnter ()
    {
        if ( WhiteOverlayRenderer != null )
        {

            SoundManager.Instance.PlaySoundFX ( "TileIntro" , 0f );
            WhiteOverlayRenderer.color = Color.white;

//            transform.DOScale ( new Vector3 ( 0.2f, 0.2f , 1f ), 0.3f ).From ().OnComplete 
//            (
//                () => { transform.localScale = Vector3.one; }
//            ).Play ();


            WhiteOverlayRenderer.DOFade ( 0f , 0.1f ).SetDelay (0.3f).Play ();

            return 0.5f;
        }

        return 0f;
    }


    protected abstract void SetKey ( string key );
    public abstract void OnHint ();
    public abstract void OnNormal ();
    public abstract void OnPressed ();
    public abstract void OnGameOver ();

    public abstract BoardEvent GetEvent ( Cell target );
    public abstract void SetToFront ();
    public abstract bool TryToOccupy ( List < Cell > removals );
    public abstract bool CanBeOccupied ( Cell cell );
    public abstract BoardEvent GetSlideEvent ( Cell target );
    public abstract Cell.Direction GetSlideDirection ( Cell.Direction current );
    public abstract void EndTurn (Cell cell);


}

public class LayerUtils
{
    public static Tile.Type DecodeTile ( string key )
    {
        var code = key.Split(':')[0];
        Tile.Type type = Tile.Type.NONE;

        if ( code != "0") type = Tile.Type.RENDER;

        return type;
    }
}
