﻿using UnityEngine;
using System.Collections;
using System;
using PotionSoup.MenuManagement;
using System.Collections.Generic;

public class Item : Layer
{
    public enum Type
    {
        NONE,
        CHEST,
        KEY,
        COIN,
        SPAWN_TRIGGER
    }

    public Type type { get; set; }
    public string key { get; set; }

    protected override void SetKey ( string key )
    {
        this.key = key;
        this.type = Type.COIN;
    }

    public override void OnGameOver()
    {
        
    }

    public override float DoInitalEnter()
    {
        return 0f;
    }

    public override void OnHint()
    {
        
    }

    public override void OnNormal()
    {
        
    }

    public override void OnPressed()
    {
        
    }

    public override Cell.Direction GetSlideDirection(Cell.Direction current)
    {
        return current;
    }

    public override bool CanBeOccupied(Cell cell)
    {
        return true;
    }

    public override BoardEvent GetSlideEvent(Cell target)
    {
        return null;
    }

    public override BoardEvent GetEvent(Cell target)
    {
        return new RefreshEvent (target);
    }
    public override void SetToFront()
    {
        transform.SetParent( MenuManager.Instance.transform, true );
        transform.SetAsLastSibling();
    }
    public override bool TryToOccupy(List<Cell> removals)
    {
        return true;
    }

    public override void EndTurn( Cell cell )
    {
        
    }
}
