﻿using UnityEngine;
using System.Collections;

public class PickupEvent : BoardEvent 
{

    public PickupEvent ( Cell target)
    {
        this.target = target;
    }
    public override float Resolve()
    {
        // just call item.animator.Play ("Pickup") or something
        target.Refresh();

        return 0f;
    }
}
