﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;

public class SolutionItem : Item 
{
    [Header ("Initial Enter Options")]
    public Transform root;
    public SpriteRenderer overlay;
    public ParticleSystem flash;

    public float startScale = 0.2f;
    public float scaleDuration = 1.0f;
    public float fadeOutDuration = 0.5f;

    public Ease scaleEase;
    public float amplitude = 3f;
    public float period = 0f;

    [Header ("Idle Options")]
    public float fadeTo = 0.2f;
    public float fadeDuration = 1f;

    [Header ("Sprite Config")]
    public Sprite solutionOneSprite;
    public Sprite solutionTwoSprite;
    public Sprite solutionThreeSprite;

    public Dictionary < Cell.SolutionValue , Sprite > SolutionToSpriteMap;

    private void Start ()
    {
        SolutionToSpriteMap = new Dictionary<Cell.SolutionValue, Sprite> ()
        {
            { Cell.SolutionValue.None , null },
            { Cell.SolutionValue.One , solutionOneSprite },
            { Cell.SolutionValue.Two , solutionTwoSprite },
            { Cell.SolutionValue.Three , solutionThreeSprite },
        };

    }

    public override float DoInitalEnter()
    {
        overlay.DOFade ( 1.0f, fadeDuration ).From ().Play ();

        root.DOScale ( startScale, scaleDuration ).From ().SetEase ( scaleEase , amplitude , period ).OnStart 
        (
            () =>
            {
                gameObject.SetActive (true);
                flash.Play (true);
                SoundManager.Instance.PlaySoundFX ("Shine");
            }
        ).Play ();

        return 0.3f;
    }

    public virtual void SetValue ( Cell.SolutionValue  value )
    {
        if (SolutionToSpriteMap == null )
        {
            SolutionToSpriteMap = new Dictionary<Cell.SolutionValue, Sprite> ()
            {
                { Cell.SolutionValue.None , null },
                { Cell.SolutionValue.One , solutionOneSprite },
                { Cell.SolutionValue.Two , solutionTwoSprite },
                { Cell.SolutionValue.Three , solutionThreeSprite },
            };
        }

        sprite = SolutionToSpriteMap [ value ];
    }
}
