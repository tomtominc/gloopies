﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CellData 
{
    public int row;
    public int column;
    public string pieceKey;
    public string tileKey;
    public string itemKey;

    public Piece.Type PieceType 
    { 
        get 
        { 
            if ( Piece.TypeByKey.ContainsKey ( pieceKey ) )
            {
                return Piece.TypeByKey [ pieceKey ]; 
            }
            else 
            {
                string[] code = pieceKey.Split(':');

                return Piece.TypeByKey [ code[1] ];
            }
        } 
    }
    public Tile.Type TileType 
    {
        get { return LayerUtils.DecodeTile(tileKey); }
    }
    public Item.Type ItemType
    {
        get
        {
            if ( ! itemMap.ContainsKey ( tileKey ) ) return Item.Type.NONE;

            return itemMap [ itemKey ];
        }
    }

    public void SwapPieceType ( string type )
    {
        var decode = pieceKey.Split(':');

        decode[0] = type;

        pieceKey = string.Join(":", decode);
    }

    private Dictionary < string , Item.Type > itemMap = new Dictionary<string, Item.Type> ()
    {
        {"Coin" , Item.Type.COIN }
    };

}
