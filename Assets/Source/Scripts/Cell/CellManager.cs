﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CellManager  
{
    private static Cell[,] Grid;

    public static int columns;
    public static int rows;


    public static void Initialize ( int _rows, int _columns )
    {
        rows = _rows;
        columns = _columns;
        Grid = new Cell[rows, columns];
    }

    public static void AddCell ( int row, int column, Cell cell )
    {
        if (Grid == null )
        {
            Debug.LogError("Grid is empty, did you forget to initilaize it?");
        }

        Grid [row,column] = cell;
    }

    public static Cell GetCell ( int row, int column )
    {
        if (Grid == null )
        {
            Debug.LogError("Grid is empty, did you forget to initilaize it?");
        }

        if ( column >= columns || column < 0 ) return null;

        if ( row >= rows || row < 0 ) return null;

        return Grid[row,column];
    }
}
