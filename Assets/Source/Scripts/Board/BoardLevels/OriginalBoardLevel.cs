﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.FDT.EasySpritesAnimation;
using DG.Tweening;

public class OriginalBoardLevel : BoardLevel 
{
    public int rows = 10;

    public GameObject cliffPrefab;
    public GameObject foreground;

    public TrueParallax parallaxScript;

    public List < Transform > clouds = new List<Transform> ();

    public override void Initialize(BoardManager boardManager)
    {
        base.Initialize(boardManager);

        foreground.SetActive ( true );
        GameManager.Instance.gameControls.GetComponent < GameControls > ().SetOffScreen ();
        layout.transform.localPosition = new Vector3 (0f, -120 + ((_board.rows - 3) * 20 ), 0f );

    }

    public override void Exit()
    {
        foreground.SetActive ( false );
        gameObject.SetActive ( false );

    }

    public override void CreateBoard()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < _board.columns; column++)
            {
                if ( row < _board.rows )
                {
                    Cell cell = CellFactory.Instance.CreateCell(row, column);

                    CellData data = _board.GetCell(row, column);

                    cell.animator.Play (GetCellQuadrant ( row, column, _board.columns ).ToString ());

                    cell.Initialize(data);

                    SetCell(row, column, cell);

                    layout.SetAsParent ( cell.transform );


                }
                else if ( row == _board.rows )
                {
                    var go = Instantiate < GameObject > ( cliffPrefab );

                    var animator = go.GetComponent < SpriteAnimation > ();

                    if ( column == 0 ) animator.Play ( "GrassLining_Left" );
                    else if ( column == _board.columns - 1 ) animator.Play ( "GrassLining_Right" );
                    else if ( column.IsEven () ) animator.Play ( "GrassLining_Alt1" );
                    else animator.Play ( "GrassLining_Alt0" );

                    layout.SetAsParent ( go.transform );

                }
                else
                {
                    var go = Instantiate < GameObject > ( cliffPrefab );

                    var animator = go.GetComponent < SpriteAnimation > ();

                    if ( column == 0 ) animator.Play ( "Cliff_Left" );
                    else if ( column == _board.columns - 1 ) animator.Play ( "Cliff_Right" );
                    else animator.Play ( "Cliff" );

                    layout.SetAsParent ( go.transform );
                }
            }
        }

        QueryInitializerEvents ( Layer.KeyType.TILE );
        QueryInitializerEvents ( Layer.KeyType.ITEM );
        QueryInitializerEvents ( Layer.KeyType.PIECE );
    }

    public override IEnumerator OnInitialize()
    {
        float duration = 2.0f;

        SoundManager.Instance.PlaySoundFX ( "Wind_Intro" );

        layout.transform.DOLocalMoveY ( layout.transform.localPosition.y - 50f, 1.0f ).SetDelay (1.0f).From ().Play ();

        yield return new WaitForSeconds (duration);

        if (! _boardManager.CurrentLevel.IsTutorialLevel () )
        {
            GameManager.Instance.gameControls.GetComponent < GameControls > ().MoveIn ();
        }

        SoundManager.Instance.InternalMusicEnabled = true;

        SoundManager.Instance.PlayMusic ( "GloopTroop", 0f );
        SoundManager.Instance.PlayMusic ( "Wind_Loop",SoundManager.Track.Two, 0f );
    }


}
