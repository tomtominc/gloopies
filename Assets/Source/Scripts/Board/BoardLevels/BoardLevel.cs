﻿using UnityEngine;
using System.Collections;
using UI.Pagination;
using System.Collections.Generic;

public abstract class BoardLevel : MonoBehaviour
{
    public SpriteGridLayout layout;
    public bool refreshGridEveryFrame = false;


    protected Cell[] _map;

    public Cell[] Map
    {
        get { return _map; }
    }

    public enum CellQuadrant
    {
        TOP_LEFT,
        TOP_ALT_0,
        TOP_ALT_1,
        TOP_RIGHT,
        TOP_RIGHT_ALT,

        LEFT_ALT_0,
        LEFT_ALT_1,
        MIDDLE_ALT_0,
        MIDDLE_ALT_1,
        RIGHT_ALT_0,
        RIGHT_ALT_1,

        BOTTOM_LEFT,
        BOTTOM_LEFT_ALT,
        BOTTOM_ALT_0,
        BOTTOM_ALT_1,
        BOTTOM_RIGHT,
        BOTTOM_RIGHT_ALT
    }

    public int Columns
    {
        get { return layout.constraintCount; }
        set { layout.constraintCount = value; }
    }

    public int RealColumns
    {
        get { return _board.columns; }
    }

    protected BoardManager _boardManager;

    protected Board _board
    {
        get { return _boardManager.Board; }
    }

    protected int Size
    {
        get { return _board.rows * _board.columns; }
    }

    public virtual void Initialize(BoardManager boardManager)
    {
        gameObject.SetActive(true);

        _boardManager = boardManager;

        _map = new Cell[ Size ];

        layout.transform.DestroyChildren();

        RefreshGrid();
    }

    public Cell GetCell(int row, int column)
    {
        int index = NumberExtensions.GetIndex(row, column, _board.columns);

        if (index < 0 || index >= _map.Length)
            return null;

        return _map[index];
    }

    public void SetCell(int row, int column, Cell cell)
    {
        int index = NumberExtensions.GetIndex(row, column, _board.columns);

        if (index < 0 || index >= _map.Length)
            return;

        _map[index] = cell;
    }

    public void SetBoard()
    {
        for (int i = 0; i < _map.Length; i++)
        {
            _map[i].Refresh();

            _map[i].OnRefreshFromUndo();

        }
    }

    public GameManager.DeviceType RefreshGrid()
    {

        switch (GameManager.Instance.deviceType)
        {
            case GameManager.DeviceType.iPhone4:

                layout.spacing = new Vector2(0.60f, 0.60f);

                break;
            case GameManager.DeviceType.iPhone5:

                layout.spacing = new Vector2(-0.16f, -0.16f);

                break;

            case GameManager.DeviceType.iPad:

                layout.spacing = new Vector2(1.2f, 1.2f);

                break;

            case GameManager.DeviceType.WSVGA:

                layout.spacing = new Vector2(0f, 0f);

                break;

            case GameManager.DeviceType.WVGA:

                layout.spacing = new Vector2(0.14f, 0.14f);

                break;

            case GameManager.DeviceType.WXGA:

                layout.spacing = new Vector2(0.32f, 0.32f);

                break;

            case GameManager.DeviceType.Unsupported:

                layout.spacing = new Vector2(-0.16f, -0.16f);

                break;

        }

        return GameManager.Instance.deviceType;
    }


    public virtual void CreateBoard()
    {
        for (int row = 0; row < this._board.rows; row++)
        {
            for (int column = 0; column < this._board.columns; column++)
            {
                Cell cell = CellFactory.Instance.CreateCell(row, column);

                CellData data = _board.GetCell(row, column);

                cell.Initialize(data);

                SetCell(row, column, cell);
            }
        }
    }

    public IEnumerator CreateSolutions(ObjectiveData[] objectives)
    {
        if (objectives == null)
            yield break;
        if (objectives.Length <= 0)
            yield break;

        for (int i = 0; i < objectives.Length; i++)
        {
            ObjectiveData data = objectives[i];

            if (data == null)
                continue;

            Dialogue dialogue = data.Last();

            if (dialogue == null)
                continue;

            CellData cellData = _board.GetCell(dialogue.row, dialogue.column);

            string[] decode = cellData.tileKey.Split(':');

            if (decode[0] == "SL")
            {
                cellData = GameManager.Instance.CurrentBoard.GetEndCoordinateForSlider(dialogue.row, dialogue.column, BoardManager.directionMap[decode[1]]);
            }

            if (cellData == null)
                continue;

            Cell cell = GetCell(cellData.row, cellData.column);

            if (cell == null)
                continue;

            cell.solutionValue = (Cell.SolutionValue)(i + 1);

            yield return new WaitForSeconds(0.3f);
        }

    }

    public void PlayLevelMusicOrTutorialMusic(string levelMusic)
    {
        if (_boardManager.CurrentLevel.tutorial != null && _boardManager.CurrentLevel.tutorial.Length > 0)
        {
            
        }
    }

    public virtual void Exit()
    {
        
    }

    public virtual IEnumerator OnInitialize()
    {
        yield return new WaitForSeconds(1f);
    }

    public void QueryInitializerEvents(Layer.KeyType type)
    {
        for (int i = 0; i < _map.Length; i++)
        {
            if (_map[i] == null)
                continue;

            _map[i].DoInitialzeEvent(type);
        }        
    }

    public virtual CellQuadrant GetCellQuadrant(int row, int column, int max)
    {
        if (row == 0)
        {
            if (column == 0)
                return CellQuadrant.TOP_LEFT;
            else if (column == _board.columns - 1)
            {
                if (max.IsEven())
                    return CellQuadrant.TOP_RIGHT_ALT;
                

                return CellQuadrant.TOP_RIGHT;
            }
            else if (column.IsEven())
                return CellQuadrant.TOP_ALT_1;
            else
                return CellQuadrant.TOP_ALT_0;
        }
        else if (row == _board.rows - 1)
        {
            if (column == 0)
            {
                if (max.IsEven())
                    return CellQuadrant.BOTTOM_LEFT_ALT;
                
                return CellQuadrant.BOTTOM_LEFT;
            }
            else if (column == _board.columns - 1)
            {
//                if ( max.IsEven () )
//                    return CellQuadrant.BOTTOM_RIGHT_ALT;


                return CellQuadrant.BOTTOM_RIGHT;
            }
            else if (column.IsEven())
            {
                if (max.IsEven())
                    return CellQuadrant.BOTTOM_ALT_0;
                
                return CellQuadrant.BOTTOM_ALT_1;
            }
            else
            {
                if (max.IsEven())
                    return CellQuadrant.BOTTOM_ALT_1;

                return CellQuadrant.BOTTOM_ALT_0;
            }
        }
        else if (row.IsEven())
        {
            if (column == 0)
                return CellQuadrant.LEFT_ALT_1;
            else if (column == _board.columns - 1)
            {
                if (max.IsEven())
                    return CellQuadrant.RIGHT_ALT_0;
                
                return CellQuadrant.RIGHT_ALT_1;
            }
            else if (column.IsEven())
                return CellQuadrant.MIDDLE_ALT_1;
            else
                return CellQuadrant.MIDDLE_ALT_0;
        }
        else
        {
            if (column == 0)
                return CellQuadrant.LEFT_ALT_0;
            else if (column == _board.columns - 1)
            {
                if (max.IsEven())
                    return CellQuadrant.RIGHT_ALT_1;

                return CellQuadrant.RIGHT_ALT_0;
            }
            else if (column.IsEven())
                return CellQuadrant.MIDDLE_ALT_0;
            else
                return CellQuadrant.MIDDLE_ALT_1;
        }
    }

    public virtual Sprite GetLayoutSprite(int row, int column)
    {
        return null;
    }

    public virtual BoardEvent GetInitialEvent()
    {
        return null;    
    }

    protected virtual void DoStart()
    {
        
    }

    protected virtual void DoUpdate()
    {
        if (Input.GetKey(KeyCode.R) || refreshGridEveryFrame)
        {
            var d = RefreshGrid();

            if (Input.GetKey(KeyCode.S))
            {
                Debug.LogFormat("Device Type: {0} Resolution: {1}", d, (float)((float)Screen.height / (float)Screen.width));
            }
        }
    }

    private void Start()
    {
        DoStart();
    }

    private void Update()
    {
        DoUpdate();
    }

}
