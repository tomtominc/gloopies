﻿using UnityEngine;
using System.Collections;
using com.FDT.EasySpritesAnimation;
using DG.Tweening;

public class SlidingBoardLevel : BoardLevel 
{
    public GameObject wallPrefab;
    public GameObject foreground;

    public override void Initialize(BoardManager boardManager)
    {
        base.Initialize(boardManager);

        foreground.SetActive ( true );
        GameManager.Instance.gameControls.GetComponent < GameControls > ().SetOffScreen ();
        // layout.transform.localPosition = new Vector3 (0f, -120 + ((_board.rows - 3) * 20 ), 0f );
    }

    public override void Exit()
    {
        foreground.SetActive ( false );
        gameObject.SetActive ( false );

    }

    public override void CreateBoard()
    {
        int rows = _board.rows + 1;
        int columns = _board.columns + 1;

        Columns = _board.columns + 2;

        for (int row = -1; row < rows; row++)
        {
            for (int column = -1; column < columns; column++)
            {
                string animKey = GetWallAnimKey ( row, column );

                if (row >= 0 && column >= 0 && row < _board.rows && column < _board.columns )
                {
                    Cell cell = CellFactory.Instance.CreateCell(row, column);

                    CellData data = _board.GetCell(row, column);

                    animKey = string.Format ( "Sliding_{0}", (row + column).IsEven () ? "Even" : "Odd" );

                    cell.animator.Play (animKey);

                    cell.Initialize(data);

                    SetCell(row, column, cell);

                    layout.SetAsParent ( cell.transform );
                }
                else
                {
                    var go = Instantiate < GameObject > ( wallPrefab );

                    var animator = go.GetComponent < SpriteAnimation > ();

                    var render = go.GetComponent < SpriteRenderer > ();

                    render.sortingOrder = 1;

                    animator.Play ( animKey );

                    layout.SetAsParent ( go.transform );
                }
            }
        }

        QueryInitializerEvents ( Layer.KeyType.TILE );
        QueryInitializerEvents ( Layer.KeyType.ITEM );
        QueryInitializerEvents ( Layer.KeyType.PIECE );
    }

    public virtual string GetWallAnimKey ( int row, int column )
    {
        int sum = row + column;
        string animKey = string.Empty;

        if ( row < 0 || column < 0 )
        {
            // top or left

            if ( row < 0 )
            {
                // top

                if ( sum == -2 )
                {
                    animKey = "LUC";
                }
                else if ( sum == -1 )
                {
                    animKey = "LUCR";
                }
                else if ( sum == _board.rows - 2 )
                {
                    animKey = "RUCL";
                }
                else if (sum == _board.rows - 1 )
                {
                    animKey = "RUC";
                }
                else
                {
                    animKey = "UM";
                }


            }
            else
            {
                // left

                if ( sum == -1 )
                {
                    animKey = "LU";
                }
                else if ( sum == _board.columns - 2 )
                {
                    animKey = "LL";
                }
                else if (sum == _board.columns - 1 )
                {
                    animKey = "LLC";
                }
                else
                {
                    animKey = "LM";
                }
            }

        }
        else if ( row >= _board.rows || column >= _board.columns )
        {
            // right or bottom

            if ( column >= _board.columns )
            {
                // right

                int maxColumn = _board.columns;

                if ( sum == maxColumn )
                {
                    animKey = "RU";
                }
                else if ( sum == ( maxColumn + _board.rows ) )
                {
                    animKey = "RLC";
                }
                else if ( sum == (maxColumn + (_board.rows - 1) ) )
                {
                    animKey = "RL";
                }
                else
                {
                    animKey = "RM";
                }


            }
            else
            {
                // bottom
                int columns = _board.columns;
                int extraWalls = columns - 3;
                int sides = (int) ( extraWalls / 2 );

                if ( column == sides )
                {
                    animKey = "LEL";
                }
                else if ( column == sides + 1 )
                {
                    animKey = "LEM";
                }
                else if ( column == sides + 2 )
                {
                    animKey = "LER";
                }
                else 
                {
                    animKey = "LE";
                }
              
            }

        }

        return animKey;
    }

    public override IEnumerator OnInitialize()
    {
        if (! _boardManager.CurrentLevel.IsTutorialLevel () )
        {
            GameManager.Instance.gameControls.GetComponent < GameControls > ().MoveIn ();
        }

        yield break;
    }
}
