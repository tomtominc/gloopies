﻿using UnityEngine;
using System.Collections;
using com.FDT.EasySpritesAnimation;
using DG.Tweening;
using System.Collections.Generic;

public class TileSwapBoardLevel : BoardLevel 
{
    public GameObject foreground;

    public GameObject nonCellPrefab;

    public Vector2 factoryTileAnimRange = new Vector2 ( 14, 23 );

    public List < GameObject >  teslas = new List<GameObject> ();


    public List < GameObject >  tubes = new List<GameObject> ();

    public Vector2 TeslaOffsetFactor = new Vector2 ( 0.9f , 0.9f );

    public override void Initialize(BoardManager boardManager)
    {
        base.Initialize(boardManager);

        foreground.SetActive ( true );
        GameManager.Instance.gameControls.GetComponent < GameControls > ().SetOffScreen ();
    }

    public override void Exit()
    {
        foreground.SetActive ( false );
        gameObject.SetActive ( false );

    }

    public override void CreateBoard()
    {
        
        for (int row = 0; row < _board.rows + 1; row++)
        {
            for (int column = 0; column < _board.columns; column++)
            {
                if ( row < _board.rows )
                {
                    Cell cell = CellFactory.Instance.CreateCell(row, column);

                    CellData data = _board.GetCell(row, column);

                    cell.animator.Play ( Random.Range ( (int)factoryTileAnimRange.x , (int)factoryTileAnimRange.y ) );

                    cell.Initialize(data);

                    SetCell(row, column, cell);

                    layout.SetAsParent ( cell.transform );


                }
                else
                {
                    GameObject nonCell = Instantiate < GameObject > ( nonCellPrefab );

                    layout.SetAsParent ( nonCell.transform );

                    SpriteAnimation animator = nonCell.GetComponent < SpriteAnimation > ();

                    if ( column == 0 ) animator.Play ( "Platform_Left" );
                    else if ( column == _board.columns - 1 ) animator.Play ( "Platform_Right" );
                    else animator.Play ( "Platform_Middle" );
                }
            }
        }


        QueryInitializerEvents ( Layer.KeyType.TILE );
        QueryInitializerEvents ( Layer.KeyType.ITEM );
        QueryInitializerEvents ( Layer.KeyType.PIECE );
    }

    public override IEnumerator OnInitialize()
    {
        Cell topLeft = GetCell ( 0, 0 );
        Cell topRight = GetCell ( 0, _board.columns - 1 );
        Cell bottomLeft = GetCell ( _board.rows - 1 , 0  );
        Cell bottomRight = GetCell ( _board.rows - 1 , _board.columns - 1 );


        teslas [0].transform.position = new Vector3 ( (topLeft.position.x - topLeft.Size.x) * TeslaOffsetFactor.x , (topLeft.position.y + topLeft.Size.y) * TeslaOffsetFactor.y);
        teslas [1].transform.position = new Vector3 ( (topRight.position.x + topRight.Size.x) * TeslaOffsetFactor.x, (topRight.position.y + topRight.Size.y) * TeslaOffsetFactor.y );
        teslas [2].transform.position = new Vector3 ( (bottomLeft.position.x - bottomLeft.Size.x) * TeslaOffsetFactor.x, (bottomLeft.position.y - bottomLeft.Size.y) * TeslaOffsetFactor.y );
        teslas [3].transform.position = new Vector3 ( (bottomRight.position.x + bottomRight.Size.x) * TeslaOffsetFactor.x, (bottomRight.position.y - bottomRight.Size.y)  * TeslaOffsetFactor.y );

        teslas [0].GetComponent < SpriteRenderer > ().sortingOrder = 0;
        teslas [1].GetComponent < SpriteRenderer > ().sortingOrder = 0;

        foreach ( var tesla in teslas )
        {
            tesla.GetComponent < TeslaCoil > ().Init ();
        }


        yield return new WaitForSeconds ( 1f );

        teslas [ 0 ].GetComponent < TeslaCoil > ().DoStartEffect (); 
        teslas [ 1 ].GetComponent < TeslaCoil > ().DoStartEffect (); 

        yield return new WaitForSeconds ( 0.5f );

        teslas [ 2 ].GetComponent < TeslaCoil > ().DoStartEffect (); 
        teslas [ 3 ].GetComponent < TeslaCoil > ().DoStartEffect (); 


        yield return new WaitForSeconds (0.3f );

        if (! _boardManager.CurrentLevel.IsTutorialLevel () )
        {
            GameManager.Instance.gameControls.GetComponent < GameControls > ().MoveIn ();
        }

        SoundManager.Instance.InternalMusicEnabled = true;

        SoundManager.Instance.PlayMusic ( "HexLabs", 0f );
        SoundManager.Instance.PlayMusic ( "Wind_Loop",SoundManager.Track.Two, 0f );
    }
}
