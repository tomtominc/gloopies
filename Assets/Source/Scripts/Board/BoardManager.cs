﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using PotionSoup.Engine2D;
using System.Collections.Generic;
using System.Linq;
using com.ootii.Messages;
using UnityEngine.EventSystems;

public class BoardManager : MonoBehaviour
{
    public OriginalBoardLevel originalBoardLevel;
    public SlidingBoardLevel slidingBoardLevel;
    public TileSwapBoardLevel tileSwapBoardLevel;

    public enum WriterStatus
    {
        None, Tutorial, Hint
    }

    public WriterStatus writerStatus;

    public BoardLevel CurrentBoardLevel
    {
        get { return _currentBoardLevel; }
    }

    public LevelData CurrentLevel 
    {
        get { return _currentLevel; }
    }

    public Dictionary < string , BoardLevel > boardLevels;

    private BoardLevel _currentBoardLevel;

    private CanvasGroup _canvas;

    private LevelData _currentLevel;

    private Queue < BoardEvent > _events;
    private Board _board;

    private bool _gameFinished = false;

    private GameManager _gameManager
    {
        get { return GameManager.Instance; }
    }

    private LevelManager _levelManager
    {
        get { return _gameManager.LevelManager; }
    }

    private DialogueManager _dialogueManager 
    { 
        get { return DialogueManager.Instance; } 
    }

    public bool Interactable 
    {
        set 
        { 
            _canvas.interactable = value;
            SetInteractability( value );
        }
    }



    private Cell _modalCell = null;

    private bool displayHints = false;
    private int hintToDisplay = -1;

    public Board Board 
    {
        get { return _board; } 
    }

    public int Columns 
    {
        get 
        { 
            return _currentBoardLevel.RealColumns;  
        }
        set 
        {
            _currentBoardLevel.Columns = value;
        }
    }

    public void DisableAll ()
    {
        if ( _currentBoardLevel != null )
        {
            _currentBoardLevel.Exit ();

            _currentBoardLevel = null;
        }
    }

    public void Initialize( LevelData levelData )
    {

        boardLevels = new Dictionary < string , BoardLevel > ()
        {
            { "Original", originalBoardLevel },
            { "TileSwap", tileSwapBoardLevel },
            { "Sliding" , slidingBoardLevel  }
        };

        if ( gameObject.IsActive () == false )
        {
            gameObject.SetActive ( true );
        }
        else if ( transform.parent.gameObject.IsActive () == false )
        {
            transform.parent.gameObject.SetActive ( true );
        }

        StartCoroutine ( InitializeBoard ( levelData ) );
    }

    private IEnumerator InitializeBoard ( LevelData levelData )
    {
        GameManager.Instance.DisableControls ();
        SetInteractability ( false );

        yield return new WaitForSeconds( 1.0f );

        yield return new WaitForEndOfFrame ();

        _canvas = GetComponent < CanvasGroup > ();

        _events = new Queue<BoardEvent>();
        _gameFinished = false;

        // clear the board so we can start fresh
        ClearBoard();

        // get the current level
        _currentLevel = levelData; 

        // get the gameobject that hosts the board
        _currentBoardLevel = boardLevels [ _currentLevel.collection ];

        // create the board
        _board = new Board (  _currentLevel );

        // this is where I'll make grid move so it suites the creation
        Columns = _currentLevel.columns;

        // initialize the board view with the current board
        _currentBoardLevel.Initialize ( this );

        // have the board view add initialize events to the board where needed
        _currentBoardLevel.CreateBoard ();

        // initialize the dialogue manager, has access to talk to the player on the board
        _dialogueManager.Inititalize(this);

        yield return new WaitForEndOfFrame ();

        yield return StartCoroutine ( _currentBoardLevel.OnInitialize () );

        yield return new WaitForEndOfFrame ();

        yield return StartCoroutine ( _currentBoardLevel.CreateSolutions (_currentLevel.objectives ) );

        yield return new WaitForEndOfFrame ();

        yield return StartCoroutine ( Resolve() );

        if ( displayHints && hintToDisplay > -1)
        {
            _dialogueManager.Write( _currentLevel.objectives [ hintToDisplay ].solution , false );
            displayHints = false;
            hintToDisplay = -1;

            writerStatus = WriterStatus.Hint;
        }
        else if ( _currentLevel.isSpecialTutorial )
        {
            _dialogueManager.Write ( _currentLevel.tutorial , true );
            writerStatus = WriterStatus.Tutorial;
        }
        else if ( _currentLevel.tutorial.Length > 0 )
        {
            _dialogueManager.Write ( _currentLevel.tutorial  , true );
            writerStatus = WriterStatus.Tutorial;
        }

        GameManager.Instance.EnableControls ();

        MessageDispatcher.SendMessage ("BoardComplete");

        SetInteractability ( true );
    }

    public void DisplayHintGUI (int hint)
    {
        displayHints = true;

        hintToDisplay = hint;
    }

    public void DisplayCurrentTutorial ()
    {
        _dialogueManager.Write ( _currentLevel.tutorial , false );
    }

    public void SetModalCell ( Cell cell )
    {
        _modalCell = cell;

        if ( _modalCell ) InputManager.Instance.interactable = true;

        InputManager.Instance.SetModal ( _modalCell );
    }

    public void SetInteractability ( bool interactable )
    {
        InputManager.Instance.interactable = interactable;
    }

    public void ClearBoard ()
    {
        _board = null;
        _events.Clear();
    }

    public void AddEvent ( BoardEvent e )
    {
        _events.Enqueue(e);
    }

    public void DoMatch ( MatchEvent e )
    {
        _events.Enqueue(e);

        StartCoroutine( Resolve() );
    }

    public void RegisterMatch ( Cell target, List < Cell > removals )
    {
        //FIX
        //target.removalColor =  removals[0].Data.PieceType;
        target.Data.pieceKey = Piece.KeyByType [ removals[0].Data.PieceType ];

        _board.SetCell( target.Data.row, target.Data.column, target.Data );

        foreach ( var cell in removals )
        {
            cell.Data.pieceKey = "0";

            _board.SetCell( cell.Data.row, cell.Data.column, cell.Data );
        }

        MatchEvent e = new MatchEvent ( target, removals.ToArray() );

        DoMatch ( e );
    }

    public void RegisterPlacement ( Cell target )
    {
        AddEvent( new PlaceEvent ( target ) );
    }

    protected IEnumerator Resolve()
    {
        Interactable = false;

        EndTurn ();

        while (_events.Count > 0)
        {
            if (_events.Peek() == null)
            {
                _events.Dequeue ();
                continue;
            }
            
            BoardEvent boardEvent = _events.Dequeue();
            float time = boardEvent.Resolve();
            yield return new WaitForSeconds(time);

        }


        if ( _events.Count < 1 )
            CheckForEndEvent();

        if ( _events.Count > 0 )
            StartCoroutine ( Resolve () );

        _board.Save ();

        if ( _gameFinished == false )
        {
            Interactable = true;
        }
    }

    private void EndTurn ()
    {
        if ( _gameFinished ) return;

        for ( int row = 0; row < _board.rows; row++ )
        {
            for ( int column = 0; column < _board.columns; column++ )
            {
                if ( _currentBoardLevel == null ) break;

                Cell cell = _currentBoardLevel.GetCell ( row, column );

                cell.EndTurn ();
            }
        }

    }

    // used in button DO NOT DELETE
    public void GoToPrevious ()
    {
        _board.SetPrevious ();
        _currentBoardLevel.SetBoard ();

        MessageDispatcher.SendMessage ("UndoLastMove");
    }

    public static Dictionary < string , Cell.Direction > directionMap= new Dictionary<string, Cell.Direction> ()
    {
        {"L", Cell.Direction.Left },
        {"R", Cell.Direction.Right},
        {"D", Cell.Direction.Down },
        {"U", Cell.Direction.Up   }
    };

    public CellData GetNeighbor ( CellData data, Cell.Direction direction )
    {
        if ( data.row + 1 <  Board.rows && direction == Cell.Direction.Up)
        {
            return Board.GetCell( data.row + 1, data.column );
        }

        if ( data.row - 1 >= 0 && direction == Cell.Direction.Down)
        {
            return Board.GetCell( data.row - 1, data.column );
        }

        if ( data.column + 1 <  Board.columns && direction == Cell.Direction.Right)
        {
            return Board.GetCell( data.row, data.column + 1 );   
        }

        if ( data.column - 1 >= 0 && direction == Cell.Direction.Left)
        {
            return Board.GetCell( data.row, data.column - 1 ) ;
        }

        return null;

    }

    public Cell.Direction GetDirectionFromKey ( CellData data , Cell.Direction current )
    {
        string[] decode = data.tileKey.Split (':');

        if ( decode.Length > 1 )
        {
            string side = decode[1];

            Bumper.BumperDirection direction = GetBumperDirection ( side );

            if ( direction == Bumper.BumperDirection.TopRight )
            {
                if ( current == Cell.Direction.Up) return Cell.Direction.Left;
                if ( current == Cell.Direction.Right ) return Cell.Direction.Up;
            }
            else if ( direction == Bumper.BumperDirection.TopLeft )
            {
                if ( current == Cell.Direction.Up) return Cell.Direction.Right;
                if ( current == Cell.Direction.Left ) return Cell.Direction.Up;
            }
            else if ( direction == Bumper.BumperDirection.BottomLeft )
            {
                if ( current == Cell.Direction.Down) return Cell.Direction.Right;
                if ( current == Cell.Direction.Left ) return Cell.Direction.Down;
            }
            else if ( direction == Bumper.BumperDirection.BottomRight )
            {
                if ( current == Cell.Direction.Down) return Cell.Direction.Left;
                if ( current == Cell.Direction.Right ) return Cell.Direction.Down;
            }
        }

        if ( data.tileKey.Equals ( "BL" ) ) return Cell.Direction.None;


        return current;

    }

    protected virtual Bumper.BumperDirection GetBumperDirection (string key)
    {
        if ( key == "TR" ) return Bumper.BumperDirection.TopRight;
        if ( key == "TL" ) return Bumper.BumperDirection.TopLeft;
        if ( key == "BR" ) return Bumper.BumperDirection.BottomRight;
        if ( key == "BL" ) return Bumper.BumperDirection.BottomLeft;

        return Bumper.BumperDirection.None;
    }

    public CellData GetEndCoordinateForSlider ( int row, int column, Cell.Direction direction )
    {
        CellData data = _board.GetCell(row,column);

        CellData _next = data;
        CellData _last = null;

        while ( direction != Cell.Direction.None )
        {
            CellData cell = GetNeighbor ( _next, direction );

            if ( cell == null )
            {
                direction = Cell.Direction.None;
                break;
            }

            if ( cell.PieceType != Piece.Type.None )
            {
                direction = Cell.Direction.None;
                break;
            }

            direction = GetDirectionFromKey ( cell, direction );

            _last = cell;

            _next = cell;

        }

        return _last;
    }
    public Cell GetEndTileForSlider ( int row, int column )
    {
        return GetEndTileForSlider ( _currentBoardLevel.GetCell ( row, column ) );
    }

    public Cell GetEndTileForSlider ( Cell target )
    {
        Cell.Direction _direction = ((SliderTile)target.Tile)._direction;
        Cell _next = target;
        Cell _last = null;

        while ( _direction != Cell.Direction.None )
        {
            Cell cell = _next.GetNeighbour ( _direction );

            if ( cell == null )
            {
                _direction = Cell.Direction.None;
                break;
            }

            if ( cell.Data.PieceType != Piece.Type.None )
            {
                _direction = Cell.Direction.None;
                break;
            }

            _last = cell;

            if ( cell.Tile != null )
            {
                bool valid = cell.Tile.CanBeOccupied ( _next );

                if ( valid == false )
                {
                    _direction = Cell.Direction.None;
                    break;
                }
                else
                {
                    _direction = cell.Tile.GetSlideDirection ( _direction );
                }

            }

            _next = cell;

        }

        return _last;
    }

    public bool IsOnlyOnePieceLeft ()
    {
        int count = 0;

        for (int i = 0; i < CurrentBoardLevel.Map.Length; i++ )
        {
            if ( CurrentBoardLevel.Map [i].Data.PieceType != Piece.Type.None )
            {
                count++;
            }
        }

        bool onlyOneleft = count > 1 ? false : true;

        return onlyOneleft;
    }

    public Piece GetLastPiece ()
    {
        Piece piece = null;

        for (int i = 0; i < CurrentBoardLevel.Map.Length; i++ )
        {
            if ( CurrentBoardLevel.Map [i].Data.PieceType != Piece.Type.None )
            {
                piece = CurrentBoardLevel.Map [i].Piece;
            }
        }

        return piece;
    }

    private void CheckForEndEvent ()
    {
        if ( _gameFinished ) return;

        int count = 0;
        CellData data = null;

        for ( int row = 0; row < _board.rows; row++ )
        {
            for ( int column = 0; column < _board.columns; column++ )
            {

                if ( _board.GetCell(row,column).PieceType != Piece.Type.None )
                {
                    count++;
                    data = _board.GetCell(row,column);

                    if ( data.tileKey.IsNOTNullOrEmpty () )
                    {
                        string[] decode = data.tileKey.Split (':');

                        if ( decode.Length > 0 )
                        {
                            if ( decode[0] == "SL" )
                            {
                                data = GetEndCoordinateForSlider ( data.row, data.column , directionMap [ decode[1] ]  );
                            }
                        }
                    }
                }
            }
        }


        if ( count == 1 )
        {
            GameManager.Instance.DisableControls ();
            _gameFinished = true;
            FinishLevelEvent e_finish = new FinishLevelEvent ( _currentBoardLevel.GetCell ( data.row, data.column ) );
            AddEvent( e_finish );
        }
    }

}
