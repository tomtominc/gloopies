﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Board
{
    public CellData[] _map;

    public int columns;
    public int rows;

    public BoardState state;

    public int Size
    {
        get { return columns * rows; }
    }

    public Board(LevelData levelData)
    {
        this.columns = levelData.columns;
        this.rows = levelData.rows;

        _map = new CellData [ Size ];

        Create(levelData); 
    }

    public Board(int rows, int columns)
    {
        this.columns = columns;
        this.rows = rows;

        _map = new CellData [ Size ];
    }

    public void Create(LevelData levelData)
    {
        for (int row = 0; row < rows; row++)
        {
            for (int column = 0; column < columns; column++)
            {
                CellData data = new CellData()
                {
                        row = row,
                        column = column,
                        pieceKey = levelData.GetPieceCode(row, column),
                        tileKey  = levelData.GetTileCode (row, column),
                        itemKey  = levelData.GetItemCode (row, column)
                };

                SetCell( row, column, data );
            }
        }
    }

    public int GetIndex(int row, int column)
    {
        return (row * columns) + column;
    }

    public void SetCell(int row, int column, CellData data)
    {
        int index = GetIndex(row, column);

        if (index < 0 || index > _map.Length)
            return;

        _map[index] = data;
    }

    public void Save ()
    {
        if ( state == null )
            state = new BoardState ();
        
        state.Update ( _map );
    }

    public void SetPrevious ()
    {
        if ( state == null ) return;

        var data = state.GetPrevious ();

        if ( data == null ) return;

        for ( int i = 0; i < _map.Length; i++ )
        {
 
            _map[i].itemKey = data[i].itemKey;
            _map[i].pieceKey = data[i].pieceKey;
            _map[i].itemKey = data[i].itemKey;

        }


        state.UpdateCurrent ( _map );
    }

    public CellData GetCell(int row, int column)
    {
        int index = GetIndex(row, column);

        if (index < 0 || index > _map.Length)
            return null;

        return _map[index];
    }

    public string PrintBoardState ( List < CellState > states )
    {
        string print = string.Empty;

        for ( int row = 0; row < rows; row++ )
        {
            for ( int column = 0; column < columns; column++ )
            {
                print += states[GetIndex (row, column)].pieceKey + ",  ";
            }

            print += "\n";
        }

        return print;
    }

    public override string ToString()
    {
        string print = string.Empty;

        for ( int row = 0; row < rows; row++ )
        {
            for ( int column = 0; column < columns; column++ )
            {
                print += _map[GetIndex (row, column)].pieceKey + ",  ";
            }

            print += "\n";
        }

        return print;
    }

}
