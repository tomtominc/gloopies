﻿using UnityEngine;
using System.Collections.Generic;

public class BoardState 
{
    private Stack < List < CellState > > _map = new Stack< List < CellState > > ();
    private List < CellState > _current;

    public void Update ( CellData[]  map )
    {
        if ( _current != null )
        {
            _map.Push ( _current );
        }

        UpdateCurrent ( map );
    }

    public List < CellState > GetPrevious ()
    {
        if ( _map.Count <= 0 ) return null;

        var state = _map.Pop ();

        return state;
    }

    public void UpdateCurrent ( CellData[]  map  )
    {
        _current = new List<CellState> ();

        for ( int i = 0; i < map.Length; i++ )
        {
            _current.Add ( new CellState ( map[i].pieceKey, map[i].itemKey, map[i].tileKey) );
        }
    }

    public void Clear ()
    {
        _map.Clear ();
    }

}

public class CellState
{
    public CellState ( string pieceKey, string itemKey, string tileKey )
    {
        this.pieceKey = pieceKey;
        this.itemKey = itemKey;
        this.tileKey = tileKey;
    }

    public string pieceKey;
    public string itemKey;
    public string tileKey;
}
