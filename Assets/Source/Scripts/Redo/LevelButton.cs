﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using PotionSoup.MenuManagement;
using PotionSoup.Engine2D;

public class LevelButton : MonoBehaviour 
{
    public RectTransform button;
    public Text levelNumber;
    public LevelObjectives levelObjectives;
    public RectTransform lockedGraphic;
    public DynamicIcon buttonIcon;

    private LevelManager LevelManager
    {
        get { return GameManager.Instance.LevelManager; }
    }
    public void Initialize ()
    {
    }

    public void SetData ( int collection, LevelState levelState )
    {
		if ( button == null ) return;

        lockedGraphic.gameObject.SetActive (  levelState.locked );
        levelNumber  .gameObject.SetActive ( !levelState.locked );

        int n = LevelManager.GetLevelNumber ( collection, levelState.levelNumber ) + 1;

        levelNumber.text = n.Pretty ();// ( n.Pretty() );

        levelObjectives.Set ( levelState );

        if ( levelState.locked )
        {
            buttonIcon.Disable ();
        }

        if ( ! levelState.locked )
        {
            button.GetComponent < Button > ().onClick.RemoveAllListeners();

            button.GetComponent < Button > ().onClick.AddListener( () => 
                {
					GameManager.Instance.GoToLevel (collection, levelState.levelNumber);
                });
        }
    }

    public void Unlock ()
    {
        if ( lockedGraphic == null ) return;

        Animator anim = lockedGraphic.GetComponent < Animator > ();

        anim.Play ( "Unlock" );
    }

    public virtual float DoEnter ( float delay = 0f )
    {
        return button.DOScale( 1f, 1f ).SetEase(Ease.OutElastic).SetDelay (delay).Duration (true);
    }

    public virtual float DoExit ( float delay = 0f )
    {
        return button.DOScale( 0f, 1f ).SetEase(Ease.InElastic).SetDelay (delay).Duration (true);
    }
}
