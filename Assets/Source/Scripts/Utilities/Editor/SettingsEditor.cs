﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.Analytics;

public class SettingsEditor : MonoBehaviour 
{
    public static void DeleteAllPersistentSettings ()
    {
        
    }

    [MenuItem("Tools/Persistent Settings/Delete Inventory")]
    public static void DeleteInventory ()
    {
        User.totalHints = 0;
    }

    [MenuItem("Tools/Persistent Settings/Delete Analytics Data")]
    public static void DeleteAnalyticsData()
    {
        User.gender = Gender.Unknown;
        User.age = 0;
    }
}
