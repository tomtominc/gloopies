﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class AdRewardVideo : IAdUpdate
{
    public GameObject notReadyIcon;
    public GameObject description;
    public Button button;

    private float frameCount;

    public override void DoUpdate ( AdInfo info )
    {
        if ( info.hasRewardedVideo )
        {
            notReadyIcon.SetActive ( false );
            description.SetActive ( true );
            button.interactable = true;
        }
        else
        {
            notReadyIcon.SetActive ( true );
            description.SetActive ( false );
            button.interactable = false;
        }
    }

    public void ShowRewardedVideo ()
    {
        
        var options = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show("rewardedVideoZone", options);
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                GameManager.Instance.inventoryManager.DoCoinEffect ( Vector3.zero , 100 );
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Failed:
                break;
        }
    }

}
