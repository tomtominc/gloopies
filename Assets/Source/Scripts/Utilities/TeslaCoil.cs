﻿using UnityEngine;
using System.Collections;

public class TeslaCoil : BoardItem 
{
    public GameObject electricOrb;
    public GameObject electricity;

    public override void Init()
    {
        electricOrb.SetActive ( false );
        electricity.SetActive ( false );
    }
    public override void DoStartEffect()
    {
        SoundManager.Instance.PlaySoundFX ( "ElectricShock" );

        electricOrb.SetActive ( true );
        electricity.SetActive ( true );
    }
}
