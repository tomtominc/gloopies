﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextField : MonoBehaviour 
{
    protected Text[] _fields;

    protected string _text;


    public string Text 
    {
        get 
        {
            return _text;
        }

        set 
        {
            if ( _fields == null ) 
                _fields = GetComponentsInChildren < Text > (true);
            
            _text = value;

            foreach ( Text field in _fields )
            {
                string text = _text;

                if ( field.supportRichText == false )
                {
                    text = text.StripHtml ();
                }

                field.text = text;    
            }
        }
    }
}
