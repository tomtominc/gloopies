﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Shadow : MonoBehaviour 
{
    public Transform compareToForScale;

    public float minScale = 0.2f;
    public float maxScale = 1f;
    public float scaler = 10f;

    private void Update ()
    {
        float scale = maxScale / (Mathf.Abs (transform.localPosition.y - compareToForScale.localPosition.y) * scaler);

        transform.localScale = new Vector3 (Mathf.Clamp ( scale, minScale, maxScale ) , Mathf.Clamp ( scale, minScale, maxScale ) , maxScale ); 
    }

}
