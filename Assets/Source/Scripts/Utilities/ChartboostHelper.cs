﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;


public class IAdUpdate : MonoBehaviour
{
    public virtual void DoUpdate(AdInfo info)
    {
        
    }
}

[System.Serializable]
public class AdInfo
{
    public bool hasInterstitial = false;
    public bool hasRewardedVideo = false;
}

public class ChartboostHelper : Singleton < ChartboostHelper >
{
    private float frameCount = 0f;

    public AdInfo info;

    public List < IAdUpdate > Updaters = new List < IAdUpdate >();

    private void Start()
    {
        Debug.Log(Advertisement.isSupported);
        //Advertisement.Initialize("1047321", true);
    }

    private void Update()
    {
        frameCount++;
        if (frameCount > 30)
        {
            
            
            info.hasInterstitial = Advertisement.IsReady("shortVideo");
            info.hasRewardedVideo = Advertisement.IsReady("rewardedVideoZone");

            foreach (var updater in Updaters)
                updater.DoUpdate(info);
            
            frameCount = 0;
        }
    }


}
