﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GameControls : MonoBehaviour 
{
    private RectTransform rect 
    {
        get { return GetComponent < RectTransform > (); }
    }
    public void SetOffScreen ()
    {
        rect.anchoredPosition = new Vector2 ( 0f, -100f );
    }


    public void MoveIn ()
    {
        rect.DOAnchorPosY ( 0f, 0.5f ).Play ();
    }
}
