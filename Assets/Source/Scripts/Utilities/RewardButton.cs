﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RewardButton : MonoBehaviour
{
    public bool isTwitter = false;
    public bool isFacebook = false;

    public bool used = false;

    public GameObject icon;
    public Text text;

    private DynamicButton button { get { return GetComponent < DynamicButton >(); } }

    private void Start()
    {
        CheckForUsed();
    }

    private void OnEnabled()
    {
        CheckForUsed();
    }

    private void CheckForUsed()
    {
        if (isTwitter && User.followedTwitter)
        {
            used = true;
        }
        else if (isFacebook && User.followedFacebook)
        {
            used = true;
        }


        if (used)
        {

            button.DoDisable();
            icon.SetActive(false);
            text.gameObject.SetActive(false);
        }
    }

    public void FollowFacebook()
    {
        if (!used)
        {
            GameManager.Instance.FollowFacebook();
            CheckForUsed();
        }
            
    }

    public void FollowTwitter()
    {
        if (!used)
        {
            GameManager.Instance.FollowTwitter();
            CheckForUsed();
        }
           
    }
}
