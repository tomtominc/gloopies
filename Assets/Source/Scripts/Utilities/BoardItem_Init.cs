﻿using UnityEngine;
using System.Collections;

public class BoardItem_Init : BoardItem 
{
    public override void DoEffect()
    {
        var items = GetComponentsInChildren < BoardItem > (true);

        foreach ( var item in items )
        {
            if ( item == this ) continue;

            if ( item.gameObject.IsActive () == false )
            {
                item.gameObject.SetActive ( true );
            }

            item.DoEffect ();
        }
    }

    public override void DoEndEffect()
    {
        var items = GetComponentsInChildren < BoardItem > (true);

        foreach ( var item in items )
        {
            if ( item == this ) continue;

            if ( item.gameObject.IsActive () == false )
            {
                item.gameObject.SetActive ( true );
            }

            item.DoEndEffect ();
        }
    }

    public override void DoStartEffect()
    {
        var items = GetComponentsInChildren < BoardItem > (true);

        foreach ( var item in items )
        {
            if ( item == this ) continue;

            if ( item.gameObject.IsActive () == false )
            {
                item.gameObject.SetActive ( true );
            }

            item.DoStartEffect ();
        }
    }

    public override void Init()
    {
        var items = GetComponentsInChildren < BoardItem > (true);

        foreach ( var item in items )
        {
            if ( item == this ) continue;

            if ( item.gameObject.IsActive () == false )
            {
                item.gameObject.SetActive ( true );
            }

            item.Init ();
        }
    }
}
