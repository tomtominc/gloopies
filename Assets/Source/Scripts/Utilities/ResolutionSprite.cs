﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResolutionSprite : MonoBehaviour 
{
    public string ipad;
    public string iphone5;
    public string iphone4;

    private Image _image
    {
        get { return GetComponent < Image > (); }
    }

    private void Update ()
    {
        float resolution = (float)((float)Screen.height / (float)Screen.width);

        if ( resolution < 1.4f )
        {
            //ipad
            _image.sprite = CellFactory.Instance.GetSprite ( ipad );
        }
        else if ( resolution < 1.6f )
        {
            //iphone4

            _image.sprite = CellFactory.Instance.GetSprite ( iphone4 );
        }
        else
        { 
            //iphone5

            _image.sprite = CellFactory.Instance.GetSprite ( iphone5 );
        }
    }
}
