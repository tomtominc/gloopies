﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using DG.Tweening;

public class Popup : MonoBehaviour 
{
    public Text titleContent;
    public Text dialogueContent;
    public Text acceptContent;
    public Text declineContent;
    public Button acceptButton;
    public Button declineButton;

    public TweenAnimator animator;

    public void Initialize ( string title, string dialogue, string accept, string decline, Action onAccept, Action onDecline )
    {
        gameObject.SetActive ( true );

        animator.Play ( "Enter" );

        titleContent.text = title;
        dialogueContent.text = dialogue;
        acceptContent.text = accept;
        declineContent.text = decline;

        acceptButton.onClick.RemoveAllListeners ();
        declineButton.onClick.RemoveAllListeners ();


        if (onAccept  != null )  acceptButton.onClick.AddListener ( onAccept.Invoke );
        if (onDecline != null )  declineButton.onClick.AddListener ( onDecline.Invoke );

        acceptButton.onClick.AddListener  ( Exit );
        declineButton.onClick.AddListener ( Exit );

        if ( decline.IsNullOrEmpty () ) declineButton.gameObject.SetActive ( false );
    }

    public void Exit ()
    {
        animator.Play ( "Exit" ).sequence
         .OnComplete 
        (
            () => { Destroy ( gameObject, 1f ); }
        );
    }
}
