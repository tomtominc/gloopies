﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour 
{
    private Vector2 limit;

    public Vector2 extends 
    {
        get 
        {
            var sr = GetComponent < SpriteRenderer > ();

            if ( sr == null ) return new Vector2 ( Screen.currentResolution.width * 0.5f, Screen.currentResolution.height * 0.5f);

            var s = sr.sprite;

            if ( s == null ) return new Vector2 ( Screen.currentResolution.width * 0.5f, Screen.currentResolution.height * 0.5f);

            return s.bounds.size;
        }
    }

    public Vector3 position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public Vector3 scale 
    {
        get { return transform.lossyScale; }
    }

    [Header ("Scroll Options")]
    public float scrollSpeed;
    public Vector2 direction = new Vector2 ( 1f,0f);

    private Vector3 startPosition;

    void Start ()
    {
        startPosition = transform.position;
        limit = new Vector2 ( extends.x  * scale.x, extends.y * scale.y );
    }

    void Update ()
    {
        float newPosition = Mathf.Repeat(Time.time * scrollSpeed, ( limit.x * 0.5f) );
        transform.position = startPosition + (Vector3)direction * newPosition;
    }

    void OnDrawGizmosSelected ()
    {
        limit = new Vector2 ( extends.x  * scale.x, extends.y * scale.y );

        Gizmos.color = Color.red;
        Gizmos.DrawWireCube ( transform.position + (Vector3)( limit * 0.25f ) , limit * 0.5f );

    }
}
