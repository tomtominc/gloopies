﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SpriteShadowAnimator : MonoBehaviour 
{
    private Transform shadow;
    public float amount = -0.5f;
    public float duration = 0.5f;


    private void Start ()
    {
        shadow = transform.GetChild ( 0 );


        shadow.DOLocalMoveY ( shadow.localPosition.y + amount , duration ).SetLoops ( -1, LoopType.Yoyo ).Play ();
    }


}
