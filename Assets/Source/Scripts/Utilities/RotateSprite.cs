﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RotateSprite  : BoardItem
{
    private Tweener rotateTween;

    public override void DoEffect()
    {
        rotateTween = transform.DORotate ( new Vector3 (0f,0f,360f), 3f, RotateMode.FastBeyond360 ).SetLoops ( -1 , LoopType.Restart ).Play ();
    }

    public override void DoEndEffect()
    {
        

        transform.DOScale ( 0 , 1f ).SetEase ( Ease.InBack  ).OnComplete 
        ( 
            () => 
            {
                rotateTween.Kill ();
                transform.eulerAngles = Vector3.zero;
                gameObject.SetActive ( false );
                transform.localScale = Vector3.one;
            }
           
        ).Play ();
    }
}
