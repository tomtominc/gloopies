﻿using UnityEngine;
using System.Collections;

public class BoardItem : MonoBehaviour 
{
    public virtual void Init ()
    {
        
    }

    public virtual void DoStartEffect ()
    {
        
    }

    public virtual void DoEffect ()
    {
        
    }

    public virtual void DoEndEffect ()
    {
        
    }
}
