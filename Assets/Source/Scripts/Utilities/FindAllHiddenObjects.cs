﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FindAllHiddenObjects : MonoBehaviour 
{
	void Update () 
    {
        var objects = FindObjectsOfType < Transform > ();

        Debug.Log ( "working" );

        foreach ( var item in objects ) 
        {
            item.gameObject.hideFlags = HideFlags.None;
        }
	}
}
