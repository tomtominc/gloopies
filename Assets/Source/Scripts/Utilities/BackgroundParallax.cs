﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;
using UnityEngine.UI;

[RequireComponent ( typeof ( RawImage ) ) ]
public class BackgroundParallax : MonoBehaviour 
{
    public float scrollSpeed = 2f;

    public enum MoveDirectionX
    {
        None, Right , Left
    }

    public enum MoveDirectionY
    {
        None, Up , Down
    }

    public MoveDirectionX moveX;
    public MoveDirectionY moveY;

    private RawImage _image;
    private float _speed;

    void Start ()
    {
        _image = GetComponent < RawImage > ();

       
    }

	void Update () 
    {
        _speed = scrollSpeed * 0.1f;

        var x = Mathf.Repeat ( _speed * Time.time,  moveX != MoveDirectionX.None ? 1f : 0f );
        var y = Mathf.Repeat ( _speed * Time.time,  moveY != MoveDirectionY.None ? 1f : 0f );

        x = moveX == MoveDirectionX.Right ?  -x :  x;
        y = moveY == MoveDirectionY.Up ?  -y :  y;

        _image.uvRect = new Rect ( x, y , 1f, 1f );
	}
}
