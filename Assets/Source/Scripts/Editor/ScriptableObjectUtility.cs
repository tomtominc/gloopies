﻿using UnityEngine;
using UnityEditor;
using System.IO;

public static class ScriptableObjectUtility
{
    /// <summary>
    //  This makes it easy to create, name and place unique new ScriptableObject asset files.
    /// </summary>
    public static void CreateAsset<T> () where T : ScriptableObject
    {
        T asset = ScriptableObject.CreateInstance<T> ();

        string path = AssetDatabase.GetAssetPath (Selection.activeObject);
        if (path == "") 
        {
            path = "Assets";
        } 
        else if (Path.GetExtension (path) != "") 
        {
            path = path.Replace (Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New " + typeof(T).ToString() + ".asset");

        AssetDatabase.CreateAsset (asset, assetPathAndName);

        AssetDatabase.SaveAssets ();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow ();
        Selection.activeObject = asset;
    }

    [MenuItem("Assets/Create/4x4 Level Json")]
    public static void Create4x4Level ()
    {
        string path = AssetDatabase.GetAssetPath (Selection.activeObject);
        if (path == "") 
        {
            path = "Assets";
        } 
        else if (Path.GetExtension (path) != "") 
        {
            path = path.Replace (Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "5x5.json");


        string text = "{\n\t\"name\": \"4x4\",\n\t\"id\": 0,\n\t\"rows\": 4,\n\t\"columns\": 4,\n\n    \"tutorial\":\n    [\n    ],\n\n\t\"solution\": \n\t[{\n\t    \"row\": -1,\n\t    \"column\": -1\n\t}, {\n\t    \"row\": -1,\n\t    \"column\": -1\n\t}, {\n\t    \"row\": -1,\n\t    \"column\": -1\n\t}],\n\n\n\t\"piece\": [\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\"\n\n\n\t],\n\n\t\"item\": [\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\"\n\n\t],\n\n\t\"tile\": [\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\",\n\n\t   \t\"0\",\"0\",\"0\",\"0\"\n\n\t]\n}";

        File.WriteAllText (assetPathAndName, text);

        AssetDatabase.SaveAssets ();
        AssetDatabase.Refresh();
    }

    [MenuItem("Assets/Create/5x5 Level Json")]
    public static void Create5x5Level ()
    {
        string path = AssetDatabase.GetAssetPath (Selection.activeObject);
        if (path == "") 
        {
            path = "Assets";
        } 
        else if (Path.GetExtension (path) != "") 
        {
            path = path.Replace (Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");
        }

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "5x5.json");


        string text = "{\n\t\"name\": \"5x5\",\n\t\"id\": 0,\n\t\"rows\": 5,\n\t\"columns\": 5,\n\n    \"tutorial\":\n    [\n    ],\n\n\t\"solution\": \n\t[{\n\t    \"row\": -1,\n\t    \"column\": -1\n\t}, {\n\t    \"row\": -1,\n\t    \"column\": -1\n\t}, {\n\t    \"row\": -1,\n\t    \"column\": -1\n\t}],\n\n\n\t\"piece\": [\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t\t\"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t\t\"0\",\"0\",\"0\",\"0\",\"0\"\n\n\t],\n\n\t\"item\": [\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t\t\"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t\t\"0\",\"0\",\"0\",\"0\",\"0\"\n\n\t],\n\n\t\"tile\": [\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t    \"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t\t\"0\",\"0\",\"0\",\"0\",\"0\",\n\n\t\t\"0\",\"0\",\"0\",\"0\",\"0\"\n\n\t]\n}";


        File.WriteAllText (assetPathAndName, text);
        //TextAsset asset = new TextAsset ();
        //AssetDatabase.CreateAsset (asset, assetPathAndName);

        AssetDatabase.SaveAssets ();
        AssetDatabase.Refresh();
        // EditorUtility.FocusProjectWindow ();
       // Selection.activeObject = asset;
    }
}