﻿using UnityEngine;
using System.Collections;
using AsPerSpec;
using UnityEngine.UI;
using UnityEngine.Analytics;
using System.Linq;
using System;
using System.Collections.Generic;

public class SessionManager : MonoBehaviour 
{
    public CarouselToggler ageCarousel;
    public ToggleGroup genderGroup;

    private GameManager _gameManager
    {
        get { return GameManager.Instance; }
    }

    public void SetUserValues ()
    {
        var genderToggle = genderGroup.ActiveToggles ().FirstOrDefault ();
        var ageToggle = ageCarousel.targetToggle;

        int age = int.Parse ( ageToggle.name );
        string gender = genderToggle.name;

        User.age = age;
        User.gender = (Gender) Enum.Parse ( typeof(Gender), gender, true );

        _gameManager.StartSession ();

        _gameManager.LoadStartMenu ();


    }
}
