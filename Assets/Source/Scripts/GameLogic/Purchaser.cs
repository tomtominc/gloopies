﻿using UnityEngine;
using UnityEngine.Purchasing;
using System.Collections.Generic;
using System;
using com.ootii.Messages;
using System.Collections;
using System.Linq;

public enum PurchaseEvent
{
    OnFinishedInitialize,
    TryCompletePurchase,
    BuyItem,
    RestorePurchases
}

public enum Transaction
{
    Hint1, Hint5, Hint10
}

public class TransactionData
{
    public Transaction identifier;
}

public class Purchaser : MonoBehaviour, IStoreListener
{
    public GameObject TransactionPopup;
    public GameObject loadingIndicator;
    public DynamicButton removeAdsButton;

    private IStoreController controller;
    private IExtensionProvider extensions;

    private Dictionary < string , int > _universalProductCode = new Dictionary<string, int> ()
    {
        { kCoin80    , 80    },
        {kCoin500    , 500   },
        { kCoin1200  , 1200  },
        { kCoin2500  , 2500  },
        { kCoin6500  , 6500  },
        { kCoin14000  , 14000 }
    };

    public List < string > _itunesProducts = new List<string> ()
        {
            { "gloopies.coins.80"  },
            { "gloopies.coins.500"  },
            { "gloopies.coins.1200"},
            { "gloopies.coins.2500"  },
            { "gloopies.coins.6500"   },
            { "gloopies.coins.14000" }
        };

    public List < string > _androidProducts = new List<string> ()
        {
            { "gloopies.coins.80"       },
            { "gloopies.coins.500"    },
            { "gloopies.coins.x.1200"    },
            { "gloopies.coins.2500"   },
            { "gloopies.coins.x.6500"    },
            { "gloopies.coins.x.14000" }
        };

    public List <string> _universalProducts = new List < string > ()
        {
            {kCoin80},
            {kCoin500},
            {kCoin1200},
            {kCoin2500},
            {kCoin6500},
            {kCoin14000}
        };
        

    public const string kCoin80     = "80Coins";
    public const string kCoin500    = "500Coins";
    public const string kCoin1200   = "1200Coins";
    public const string kCoin2500   = "2500Coins";
    public const string kCoin6500   = "6500Coins";
    public const string kCoin14000   = "1400Coins";

    public const string kRemoveAds  = "gloopies.removeads";

    public int hint1Cost  = 50;
    public int hint5Cost  = 200;
    public int hint10Cost = 350;

    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    void Start()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }

        MessageDispatcher.AddListener ( PurchaseEvent.TryCompletePurchase.ToString () , OnTryCompleteTransaction , true );
        MessageDispatcher.AddListener ( PurchaseEvent.BuyItem.ToString ()             , OnBuyProductID           , true );
        MessageDispatcher.AddListener ( PurchaseEvent.RestorePurchases.ToString ()    , OnRestorePurchases       , true );
    }

    public void InitializePurchasing() 
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());


        for ( int i = 0; i < _itunesProducts.Count; i++ )
        {
            builder.AddProduct ( _universalProducts [i], ProductType.Consumable , new IDs
                {
                    { _itunesProducts[i], MacAppStore.Name },
                    { _androidProducts[i], GooglePlay.Name }
                });
        }

        builder.AddProduct ( kRemoveAds , ProductType.NonConsumable );

        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize(this, builder);

        StartCoroutine ( FinishInit () );
    }

    private IEnumerator FinishInit ()
    {
        while ( IsInitialized () == false )
        {
            yield return null;
        }

        var dataTable = new Hashtable ()
        {
            { "ProductList", m_StoreController.products.all.ToList () }
        };

        MessageDispatcher.SendMessage ( this,  PurchaseEvent.OnFinishedInitialize.ToString (), dataTable , 0f );

        yield return null;
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    
    private void OnTryCompleteTransaction ( IMessage message )
    {
        TransactionData transactionData = (TransactionData)message.Data;

        switch ( transactionData.identifier )
        {
            case Transaction.Hint1:  TryCompleteHintTransaction( hint1Cost  , 1  );  break;
            case Transaction.Hint5:  TryCompleteHintTransaction( hint5Cost  , 5  );  break;
            case Transaction.Hint10: TryCompleteHintTransaction( hint10Cost , 10 );  break;
                
        }
    }

    private void TryCompleteHintTransaction ( int cost , int numHints )
    {
        if ( User.coinAmount >= cost )
        {
            User.coinAmount -= cost;
            DisplaySuccessfulHintPopup ( numHints );
        }
        else
        {
            PurchaseEnoughCoins ();
        }
    }

    public void PurchaseEnoughCoins ()
    {
        BuyProductID ( kCoin500 );
    }

    void OnBuyProductID ( IMessage message )
    {
        BuyProductID ( (string)message.Data );
    }


    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void OnRestorePurchases ( IMessage message )
    {
        RestorePurchases ();
    }


    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }
        // If we are running on an Apple device ... 
        else if (Application.platform == RuntimePlatform.IPhonePlayer || 
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }


    //  
    // --- IStoreListener
    //

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args) 
    {
        if ( _universalProductCode.ContainsKey ( args.purchasedProduct.definition.id ) )
        {
            int coins = _universalProductCode [ args.purchasedProduct.definition.id ];
            GameManager.Instance.inventoryManager.DoCoinEffect ( Vector3.zero,  coins );
        }
        else if ( string.Equals(args.purchasedProduct.definition.id, kRemoveAds, StringComparison.Ordinal))
        {
            User.removedAds = true;
            removeAdsButton.DoDisable ();
            DisplaySuccessMessage ( "Successfully purchased Removed Ads! Thank you so much!" );
        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    private void DisplaySuccessfulHintPopup ( int numHints )
    {
        string title = string.Format ( I2.Loc.ScriptLocalization.Get ( "Popup/Hint/Success/Title") , numHints );
        string dialogue = I2.Loc.ScriptLocalization.Get ( "Popup/Hint/Success/Dialogue");
        string accept = I2.Loc.ScriptLocalization.Get ( "Popup/General/Yes");

        GameObject clone = Instantiate < GameObject > ( TransactionPopup );

        clone.transform.SetParent ( GameManager.Instance.overlayCanvas , false );

        Popup popup = clone.GetComponent < Popup > ();

        popup.Initialize (title, dialogue, accept, string.Empty, null, null );

        User.totalHints += numHints;
    }

    public void DisplaySuccessMessage ( string success )
    {  
        string title = "SUCCESS";
        string dialogue = success;
        string accept = "OK";

        GameObject clone = Instantiate < GameObject > ( TransactionPopup );

        clone.transform.SetParent ( GameManager.Instance.overlayCanvas , false );

        Popup popup = clone.GetComponent < Popup > ();

        popup.Initialize (title, dialogue, accept, string.Empty, null, null );

    }
}