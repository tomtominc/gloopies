﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class InputManager : Singleton < InputManager > 
{
    public bool interactable = true;
    public LayerMask touchMask;

    private Cell _modal;

    public class CellClickEvent : UnityEvent < Cell > { }

    public CellClickEvent OnClick = new CellClickEvent ();

    #if UNITY_EDITOR

    public bool takeScreenShots = true;
    public bool gameSlowed = true;

    #endif 

    public void SetModal ( Cell cell )
    {
        _modal = cell;    
    }

    private bool isTouchDown 
    {
        get 
        {
            //TODO: HANDLE MOBILE DEVICE
            return Input.GetMouseButton (0);
        }
    }

    private void Update ()
    {
        #if UNITY_EDITOR 

        if ( takeScreenShots && Input.GetKeyDown ( KeyCode.Space ) )
        {
            gameSlowed = !gameSlowed;
            Time.timeScale = gameSlowed ? 0.01f : 1f;
        }

        if ( Input.GetKeyDown ( KeyCode.X ) )
        {
            Time.timeScale = 0f;
        }

        #endif 


        if ( ! interactable ) return;

        if ( isTouchDown )
        {

            Vector2 origin = Camera.main.ScreenToWorldPoint ( Input.mousePosition );
            RaycastHit2D hitInfo = Physics2D.Raycast  ( origin, Vector2.zero , Mathf.Infinity, touchMask );

            var pointer = new PointerEventData(EventSystem.current);

            // convert to a 2D position
            pointer.position = Input.mousePosition;
            var raycastResults = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointer, raycastResults);

            if (raycastResults.Count > 0) 
            {
                bool hasFrontCanvas = false;

                foreach ( var result in raycastResults )
                {
                    if ( result.sortingOrder > 10 ) hasFrontCanvas = true;
                }

                if ( hasFrontCanvas ) return;
            }

            if ( hitInfo.transform )
            {
                Cell cell = hitInfo.transform.GetComponent < Cell > ();

                if ( _modal && cell != _modal ) return;

                cell.OnPointerClick ();
                OnClick.Invoke ( cell );


            }
        }
    }


}


