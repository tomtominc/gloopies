﻿using UnityEngine;
using System.Collections;
using PotionSoup.MenuManagement;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class DialogueManager : Singleton < DialogueManager > 
{
    public HintHelper hintHelper;
    public DialogueHelper writer;
    public GameObject cancelButton;

    private HintHelper _currentHint;
    private Queue < Dialogue > _currentQuery = new Queue <Dialogue> ();

    private BoardManager _boardManager;

    private bool _feedback = false;

    public void Inititalize ( BoardManager boardManager )
    {
        _boardManager = boardManager;
        _currentHint = hintHelper;
    }

    public void Write ( Dialogue[] dialogues , bool isSpecial )
    {
        if ( dialogues == null ) return;
        if ( dialogues.Length <= 0 ) return;



        if ( gameObject.IsActive () == false ) 
            gameObject.SetActive ( true );

        cancelButton.SetActive ( !isSpecial );

        foreach ( var dialogue in dialogues )
        {
            _currentQuery.Enqueue( dialogue );
        }

        StartCoroutine( Write() );
    }

    public void CellClicked ( Cell cell )
    {
        _feedback = true;
    }

    public bool GetFeedback ( Cell cell )
    {
        if ( cell == null && Input.GetMouseButtonDown(0)) return true;
        else if ( cell != null ) return _feedback;

        return false;
    }

    public void Cancel ()
    {
        _currentQuery.Clear ();
        _currentHint.Exit();

        writer.Exit ();

        _boardManager.SetModalCell ( null );
        _boardManager.SetInteractability(true);

        GameManager.Instance.GameMenuInteractibility = true;
    }

    private IEnumerator Write ()
    {
        GameManager.Instance.GameMenuInteractibility = false;

        while ( _currentQuery.Count > 0 )
        {
            yield return new WaitForEndOfFrame();

            Dialogue dialogue = _currentQuery.Dequeue();

            int row = dialogue.coordinate.row;
            int column =  dialogue.coordinate.column;

            Cell cell = _boardManager.CurrentBoardLevel.GetCell (row,column);

            _boardManager.SetInteractability ( false );

            writer.Enter ( cell );
            writer.StartTyping( dialogue.text );

            // see if this dialogue needs a tap hint on a cell.
            if ( cell != null )
            {
                if ( _currentHint.hasEntered == false )
                {
                    _currentHint.Enter( cell );
                }

                // move the hint to the cell apporpriate cell.
                _currentHint.MoveToCell( cell ); 

                while ( _currentHint.IsMoving )
                {
                    yield return null;
                }

            }
            else 
            {
                _currentHint.Exit();
            }

            _boardManager.SetModalCell ( cell );

            InputManager.Instance.OnClick.AddListener ( CellClicked );

            while ( GetFeedback ( cell ) == false )
            {
                yield return null; 
            }

           

            yield return new WaitForSeconds (  writer.DialogueExit () );

            _feedback = false;

            InputManager.Instance.OnClick.RemoveListener ( CellClicked );

        }
            
        _currentHint.Exit();

        writer.Exit ();

        _boardManager.SetModalCell ( null );
        _boardManager.SetInteractability(true);

        GameManager.Instance.GameMenuInteractibility = true;

    }


}
