﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;

public class AdHelper : MonoBehaviour
{
    public int restartsRequiredForAd = 5;

    public int totalRestartsDuringCurrentGame;

    public bool ShowInterstitial()
    {
        totalRestartsDuringCurrentGame++;

        if (totalRestartsDuringCurrentGame < restartsRequiredForAd)
        {
            return false;
        }

        totalRestartsDuringCurrentGame = 0;

        if (Advertisement.IsReady("shortVideo"))
        {
            Advertisement.Show("shortVideo");
        }

        return false;
    }
}
