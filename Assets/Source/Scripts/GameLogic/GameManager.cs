﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using PotionSoup.Persistent;
using PotionSoup.MenuManagement;
using PotionSoup.Engine2D;
using UnityEngine.Analytics;
using System.Xml;
using UnityEngine.UI;
using com.ootii.Messages;
using UI.Pagination;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class User
{
    private const string kUserAge = "kUserAge";
    private const string kUserGender = "kUserGender";
    private const string kUserAppOpenedCount = "kUserAppOpenedCount";
    private const string kUserTotalHints = "kUserTotalHints";
    private const string kUserCoinAmount = "kUserCoinAmount";
    private const string kRemovedAds = "kRemovedAds";
    private const string kFollowedFacebook = "kFollowedFacebook";
    private const string kFollowedTwitter = "kFollowedTwitter";
    private const string kFirstLevelsComplete = "kFirstLevelsComplete";
    private const string kRatedApp = "kRatedApp";
    private const string kDefaultPage = "kDefaultPage";


    public static Gender gender
    {
        get { return PersistentSettings.GetValue(kUserGender, Gender.Unknown); }
        set { PersistentSettings.SetValue(kUserGender, value); }
    }

    public static int age
    {
        get { return PersistentSettings.GetValue(kUserAge, 0); }
        set { PersistentSettings.SetValue(kUserAge, value); }
    }

    public static int appOpenedCount
    {
        get { return PersistentSettings.GetValue(kUserAppOpenedCount, 0); }
        set { PersistentSettings.SetValue(kUserAppOpenedCount, value); }
    }

    public static int totalHints
    {
        get { return PersistentSettings.GetValue(kUserTotalHints, 0); }
        set
        { 
            GameManager.Instance.hintCountDisplay.text = value.ToString();
            PersistentSettings.SetValue(kUserTotalHints, value); 
        }
    }

    public static bool removedAds
    {
        get { return PersistentSettings.GetValue(kRemovedAds, false); }
        set { PersistentSettings.SetValue(kRemovedAds, value); }
    }


    public static bool followedFacebook
    {
        get { return PersistentSettings.GetValue(kFollowedFacebook, false); }
        set { PersistentSettings.SetValue(kFollowedFacebook, value); }
    }

    public static bool followedTwitter
    {
        get { return PersistentSettings.GetValue(kFollowedTwitter, false); }
        set { PersistentSettings.SetValue(kFollowedTwitter, value); }
    }

    public static bool firstLevelsComplete
    {
        get { return PersistentSettings.GetValue(kFirstLevelsComplete, false); }
        set { PersistentSettings.SetValue(kFirstLevelsComplete, value); }
    }

    public static bool ratedApp
    {
        get { return PersistentSettings.GetValue(kRatedApp, false); }
        set { PersistentSettings.SetValue(kRatedApp, value); }
    }

    public static int coinAmount
    {
        get { return PersistentSettings.GetValue(kUserCoinAmount, 10); }
        set { PersistentSettings.SetValue(kUserCoinAmount, value); }
    }

    public static int defaultPage
    {
        get { return PersistentSettings.GetValue(kDefaultPage, 1); }
        set { PersistentSettings.SetValue(kDefaultPage, value); }
    }
}

public enum GameEvent
{
    SwitchMenu
}


public class GameManager : Singleton < GameManager >
{

    #if UNITY_EDITOR

    [MenuItem("Tools/Clear History")]
    public static void ClearUserHistory()
    {
        User.appOpenedCount = 0;
        User.totalHints = 0;
        User.removedAds = false;
        User.followedFacebook = false;
        User.followedTwitter = false;
        User.firstLevelsComplete = false;
        User.ratedApp = false;
        User.coinAmount = 0;
        User.defaultPage = 1;
    }

    #endif

    [ Header("Game Settings")]
    public Menu GameStartMenu;
    public Menu CurrentMenu;
    public bool levelEndEventOn = true;

    [ Header("Managers")]
    public BoardManager boardManager;
    public LevelManager LevelManager;
    public InventoryManager inventoryManager;

    [ Header("Helpers")]
    public AdHelper adHelper;

    [ Header("Screens and UI Components")]
    public WinScreen winScreen;
    public SplashScreen splashScreen;
    public PagedRect Pagination;
    public GameObject popupPrefab;
    public CanvasGroup gameControls;
    public Text hintCountDisplay;
    public ObjectiveToggleBox InGameFooterToggleBox;
    public Transform overlayCanvas;
    public IAPGridView iAPView;

    [Header("Reward and Purchase Buttons")]
    public DynamicButton twitterButton;
    public DynamicButton facebookButton;
    public DynamicButton removeAdsButton;

    public List < CollectionGridView > levelGridViews = new List<CollectionGridView>();


    public Dictionary < Cell.SolutionValue , int > coinValueMap = new Dictionary<Cell.SolutionValue, int>()
    {
        { Cell.SolutionValue.None ,   0 },
        { Cell.SolutionValue.One  ,  75 },
        { Cell.SolutionValue.Two  , 150 },
        { Cell.SolutionValue.Three, 300 }
    };

    public enum Menu
    {
        StartMenu,
        UserSettings,
        SelectMenu,
        GameMenu,
        SessionMenu,
        IAPHintPopup,
        SettingsMenu
    }


    public enum DeviceType
    {
        Unsupported,
        iPhone5,
        iPhone4,
        iPad,
        WXGA,
        WSVGA,
        WVGA
    }

    public DeviceType deviceType
    {
        get
        {
            float resolution = (float)((float)Screen.height / (float)Screen.width);

            if (resolution < 1.4f)
            {
                return DeviceType.iPad;
            }
            else if (resolution < 1.6f)
            {
                return DeviceType.iPhone4;
            }
            else if (resolution < 1.65f)
            {
                return DeviceType.WXGA;
            }
            else if (resolution < 1.67f)
            {
                return DeviceType.WVGA;
            }
            else if (resolution < 1.75f)
            {
                return DeviceType.WSVGA;
            }
            else if (resolution < 1.78f)
            { 
                return DeviceType.iPhone5;
            }
            else
            {
                return DeviceType.Unsupported;
            }
        }
    }

    public BoardManager CurrentBoard
    {
        get { return boardManager; }
    }

    public bool GameMenuInteractibility
    {
        set
        {
            gameControls.interactable = value;
            gameControls.blocksRaycasts = value;
        }
    }

    public void Awake()
    {
        StartCoroutine(LoadMenu(GameStartMenu, 0f, null, false));

        User.appOpenedCount++;

        LevelManager.Initialize();

        User.firstLevelsComplete = false;

        Pagination.DefaultPage = User.defaultPage;
    }

    public void GameOver(Cell lastTile)
    {
        // get the current level state
        LevelState state = LevelManager.CurrentLevelState;

        // get the current level data
        LevelData data = LevelManager.CurrentLevelData;

        var table = new Dictionary < string , object >();


        table.Add("SolutionValue", lastTile.solutionValue);
        table.Add("LevelData", data);
        table.Add("LevelState", state);
        table.Add("Gloopie", lastTile.Piece);

        // display different animations depending on which solution solved.
        winScreen.OnGameOver(table);

        if (state.levelNumber == 9)
            return;

        LevelManager.NextLevel.locked = false;

        if (state.levelNumber == 6 && !User.firstLevelsComplete)
        {
            User.firstLevelsComplete = true;

            string title = I2.Loc.ScriptLocalization.Get("Popup/HInt/Give/Title");
            string dialogue = I2.Loc.ScriptLocalization.Get("Popup/Hint/Give/Dialogue");
            string accept = I2.Loc.ScriptLocalization.Get("Popup/General/Yes");

            GameObject clone = Instantiate < GameObject >(popupPrefab);

            clone.transform.SetParent(overlayCanvas, false);

            Popup popup = clone.GetComponent < Popup >();

            popup.Initialize(title, dialogue, accept, string.Empty,

                () =>
                {
                    Debug.Log("invoking");
                    User.totalHints += 5;


                    string r_title = I2.Loc.ScriptLocalization.Get("Popup/Rate/Title");
                    string r_dialogue = I2.Loc.ScriptLocalization.Get("Popup/Rate/Dialogue");
                    string r_accept = I2.Loc.ScriptLocalization.Get("Popup/General/Yes");
                    string r_decline = I2.Loc.ScriptLocalization.Get("Popup/General/No");

                    GameObject r_clone = Instantiate < GameObject >(popupPrefab);

                    r_clone.transform.SetParent(overlayCanvas, false);

                    Popup r_popup = r_clone.GetComponent < Popup >();

                    r_popup.Initialize(r_title, r_dialogue, r_accept, r_decline,

                        () =>
                        {
                            RateApp();
                        },
                        null);
                }
                , null
            );
        }
    }

    public void DisableControls()
    {
        MenuManager.Instance.DisableControls();
    }

    public void EnableControls()
    {
        MenuManager.Instance.EnableControls();
    }

    public void ResetLevelUsingHints(int hint)
    {
        ResetCurrentLevel();
        User.totalHints--;
        CurrentBoard.DisplayHintGUI(hint);
    }

    public void GoToLevel(int collection, int levelNumber)
    {
        LevelManager.SetCurrentLevel(collection, levelNumber);
        LevelManager.NewSession();
        ResetCurrentLevel();
    }
    // used in button
    public void GoToNextLevel()
    {
        LevelManager.Next();

        GoToLevel(LevelManager.CurrentCollectionIndex, LevelManager.CurrentLevelState.levelNumber);
    }

    public const string kRemoveAds = "gloopies.removeads";

    public void PurchaseRemoveAds()
    {
        if (User.removedAds == false)
        {
            MessageDispatcher.SendMessage(this, PurchaseEvent.BuyItem.ToString(), kRemoveAds, 0f);
        }
    }

    public void RestoreCompletedTransactions()
    {
        MessageDispatcher.SendMessage(this, PurchaseEvent.RestorePurchases.ToString(), kRemoveAds, 0f);
    }

    public void debug_test_load()
    {
        #if USES_WEBVIEW
        webViewHelper.LoadRequest ( "https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fdev.twitter.com%2Fdocs%2Fapi%2F1.1&region=follow_link&screen_name=giant2studios&tw_p=followbutton&variant=2.0" );
        #endif
    }

    public void FollowTwitter()
    {
        if (!User.followedTwitter)
        {
            twitterButton.DoDisable();

            #if USES_WEBVIEW
            webViewHelper.LoadRequest ( "https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fdev.twitter.com%2Fdocs%2Fapi%2F1.1&region=follow_link&screen_name=giant2studios&tw_p=followbutton&variant=2.0" );
            #endif

            Application.OpenURL("https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fdev.twitter.com%2Fdocs%2Fapi%2F1.1&region=follow_link&screen_name=giant2studios&tw_p=followbutton&variant=2.0");
            User.followedTwitter = true;

            Timer timer = new Timer(0.5f, () => inventoryManager.DoCoinEffect(Vector3.zero, 100));

            timer.Start(this);
        }
        else
        {
            twitterButton.DoDisable();
        }

    }

    public void SetDefaultPage(Page currentPage, Page previousPage)
    {
        if (Pagination)
            User.defaultPage = Pagination.CurrentPage;
    }

    public void FollowFacebook()
    {
        if (!User.followedFacebook)
        {
            facebookButton.DoDisable();

            Application.OpenURL("https://www.facebook.com/giant2studios/");

            User.followedFacebook = true;

            Timer timer = new Timer(0.5f, () => inventoryManager.DoCoinEffect(Vector3.zero, 100));

            timer.Start(this);
        }
        else
        {
            facebookButton.DoDisable();
        }
    }

    public void RateApp()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.tomferrer.gloopies");
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Application.OpenURL("http://itunes.apple.com/app/id1098312234");
        }

        User.ratedApp = true;

        Timer r_timer = new Timer(0.5f, () => inventoryManager.DoCoinEffect(Vector3.zero, 100));

        r_timer.Start(this);
    }

    public void DisplayHintPopup(int hint)
    {
        if (User.totalHints > 0)
        {
            string title = I2.Loc.ScriptLocalization.Get("Popup/Hint/Use/Title");
            string dialogue = string.Format("{0} {1}", I2.Loc.ScriptLocalization.Get("Popup/Hint/Use/Dialogue"), User.totalHints);
            string accept = I2.Loc.ScriptLocalization.Get("Popup/General/Yes");
            string decline = I2.Loc.ScriptLocalization.Get("Popup/General/No");

            GameObject clone = Instantiate < GameObject >(popupPrefab);

            clone.transform.SetParent(MenuManager.Instance.transform, false);

            Popup popup = clone.GetComponent < Popup >();

            popup.Initialize(title, dialogue, accept, decline,

                () =>
                {
                    ResetLevelUsingHints(hint);
                }
                , null
            );
        }
        else
        {

            StartCoroutine(LoadPopup(Menu.IAPHintPopup));
        }
    }

    public void LoadStartMenu()
    {
        StartCoroutine(LoadMenu(Menu.StartMenu, 0f, null));
    }

    public void LoadSettingsMenu()
    {
        StartCoroutine(LoadMenu(Menu.SettingsMenu, 0f, null, true));
    }

    public void LoadSessionMenu()
    {
        StartCoroutine(LoadMenu(Menu.SessionMenu, 0f, null, false));
    }

    public void LoadSelectMenu()
    {
        StartCoroutine(LoadMenu(Menu.SelectMenu, 1f, () =>
                {
                    foreach (var grid in levelGridViews)
                    {
                        grid.RefreshLayout();
                    }

                }));
    }

    public void LoadLevel(LevelData data)
    {
        StartCoroutine(LoadMenu(Menu.GameMenu, 0.5f, () =>
                {
                    CurrentBoard.Initialize(data);
                }
            ));
    }

    public void ResetCurrentLevel()
    {
        User.totalHints = User.totalHints;

        StartCoroutine(LoadMenu(Menu.GameMenu, 0.5f, () =>
                {
                    LevelManager.CurrentLevelState.restartCount++;
                    CurrentBoard.Initialize(LevelManager.CurrentLevelData);
                    InGameFooterToggleBox.Initialize(LevelManager.CurrentLevelState);
                }
            ));
    }

    public void BuyHint(string id)
    {
        MessageDispatcher.SendMessage(this, PurchaseEvent.TryCompletePurchase.ToString(), new TransactionData() { identifier = (Transaction)Enum.Parse(typeof(Transaction), id) }, 0f);
    }

    public void StartSession()
    {
        int birthYear = DateTime.Now.Year - User.age;
        Analytics.SetUserGender(User.gender);
        Analytics.SetUserBirthYear(birthYear);
    }

    private IEnumerator LoadPopup(Menu menu)
    {
        CurrentMenu = menu;

        DisableControls();

        yield return new WaitForSeconds(0.3f);
        MenuManager.Instance.OpenPopup(menu.ToString(), null);

        EnableControls();
    }

    private IEnumerator LoadMenu(Menu menu, float callBackDelay, Action callback, bool useSplashScreen = true)
    {
        SoundManager.Instance.InternalMusicEnabled = false;

        MessageDispatcher.SendMessage(this, GameEvent.SwitchMenu.ToString(), menu, 0f);

        if (CurrentMenu == Menu.GameMenu)
        {
            CurrentBoard.DisableAll();
        }

        CurrentMenu = menu;

        DisableControls();

        float duration = 0f;

        if (useSplashScreen)
        {
            duration = splashScreen.DoStartSplashEffect();
        }
        if (menu == Menu.GameMenu && !User.removedAds)
        {
            if (adHelper)
            {
                if (adHelper.ShowInterstitial())
                {
                    Debug.Log("Trying to show an AD.");
                }
            }
        }

        yield return new WaitForSeconds(duration);

        MenuManager.Instance.SetMenuAllMenusActive(false);

        MenuManager.Instance.OpenMenu(menu.ToString(), 0.0f);

        yield return new WaitForSeconds(0.1f);

        if (callback != null)
            callback.Invoke();

        if (useSplashScreen)
        {
            duration = splashScreen.DoEndSplashEffect();
        }

        yield return new WaitForSeconds(duration);

        if (menu != Menu.GameMenu)
        {
            SoundManager.Instance.InternalMusicEnabled = true;
            SoundManager.Instance.PlayMusic(menu.ToString());
        }

        if (MenuManager.Instance.GetMenu(menu.ToString()).gameObject.IsActive() == false)
        {

            MenuManager.Instance.GetMenu(menu.ToString()).gameObject.SetActive(true);
        }

        EnableControls();


    }





}




