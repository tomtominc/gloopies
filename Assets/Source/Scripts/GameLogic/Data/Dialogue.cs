﻿using UnityEngine;

[System.Serializable]
public class Dialogue
{
    public Dialogue () { }

    public Dialogue ( int row, int column, string text )
    {
        this.row = row;
        this.column = column;
        this.text = text;
    }

    public int row = -1;
    public int column = -1;
    public string text;

    public Coordinate coordinate 
    {
        get { return new Coordinate(row,column); }
    }
}
