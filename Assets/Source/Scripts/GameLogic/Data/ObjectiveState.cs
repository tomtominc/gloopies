﻿using UnityEngine;
using System.Collections;
using PotionSoup.Persistent;

[System.Serializable]
public class ObjectiveState 
{
    public bool complete;
}
