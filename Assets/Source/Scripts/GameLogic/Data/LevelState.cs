﻿using System;
using UnityEngine;
using PotionSoup.Persistent;

[Serializable]
public class LevelState
{
    public string levelName = string.Empty;
    public int levelNumber = 0;
    public bool tempLock = true;

    public string kUndoCountKey    { get { return string.Format ("{0}-kUndoCount"      , key ); } }
    public string kRestartCountKey { get { return string.Format ("{0}-kRestartCount"   , key ); } } 
    public string kClearedCountKey { get { return string.Format ("{0}-kClearedCount"   , key ); } } 
    public string kLockedKey       { get { return string.Format ("{0}-kLocked"         , key ); } }
    public string kObjectivesKey   { get { return string.Format ("{0}-kObjectives"     , key ); } }

    public int undoCount 
    {
        get { return PersistentSettings.GetValue ( kUndoCountKey, 0 ); }
        set { PersistentSettings.SetValue ( kUndoCountKey, value); }
    }
    public int restartCount
    {
        get { return PersistentSettings.GetValue ( kRestartCountKey, 0 ); }
        set { PersistentSettings.SetValue ( kRestartCountKey, value); }
    }
    public int clearedCount
    {
        get { return PersistentSettings.GetValue ( kClearedCountKey, 0 ); }
        set { PersistentSettings.SetValue ( kClearedCountKey, value); }
    }
    public bool locked
    {
        get { return PersistentSettings.GetValue ( kLockedKey, true ); }
        set { PersistentSettings.SetValue ( kLockedKey, value); }
    }

    public ObjectiveState[] objectiveStates = new ObjectiveState[0];

    public int GetObjectiveCount ()
    {
        if ( objectiveStates == null )
        {
            objectiveStates = new ObjectiveState[0];
        }

        return objectiveStates.Length;
    }

    public bool AllObjectivesComplete ()
    {
        int completeCount = 0;

        for ( int i = 0; i < GetObjectiveCount(); i++ )
        {
            bool complete = GetObjectiveIsComplete ( (Cell.SolutionValue) (i+1) );

            if ( complete )
            {
                completeCount++;
            }
        }

        if ( completeCount >= GetObjectiveCount () )
        {
            return true;
        }

        return false;
    }

    public bool GetObjectiveIsComplete ( Cell.SolutionValue value )
    {
        return PersistentSettings.GetValue ( string.Format ( "{0}--{1}", kObjectivesKey, value), false );
    }

    public void SetObjective ( Cell.SolutionValue value, bool complete )
    {
        PersistentSettings.SetValue ( string.Format ("{0}--{1}", kObjectivesKey, value )  ,  complete );
    }

    public string key
    {
        get { return string.Format ("{0}-{1}", levelName, levelNumber); }
    }

    public LevelState()
    {

    }

    public LevelState(string levelName, int levelNumber)
    {
        this.levelName = levelName;
        this.levelNumber = levelNumber;
    }

    public void NewSession ()
    {
        undoCount = 0;
        restartCount = 0;
    }
}