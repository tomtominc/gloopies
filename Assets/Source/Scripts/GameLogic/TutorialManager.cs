﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.MenuManagement;

public class TutorialManager : Singleton < TutorialManager > 
{
    public List < GameObject > tutorials = new List<GameObject> ();

    public GameObject DoSpecialTutorial ( int index )
    {
        if ( index >= tutorials.Count ) return null;

        var tut = Instantiate < GameObject > ( tutorials[index] );

        tut.transform.SetParent ( MenuManager.Instance.transform, false );

        tut.transform.SetAsLastSibling ();

        return tut;
    }
}
