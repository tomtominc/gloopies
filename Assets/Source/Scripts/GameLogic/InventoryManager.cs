﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using com.ootii.Messages;
using PotionSoup.Persistent;
using UI.Pagination;
using System;
using DG.Tweening;

public class InventoryManager : MonoBehaviour 
{
    public Text coinField;

    public GameObject coinEmitter;
    public Transform target;
    public PagedRect selectMenuPages;

    private float maximumSeconds = 2f;

    private bool _isDoingCoinEffect = false;

    public void Start ()
    {
        SetCoinCount ( User.coinAmount );
    }

    public void SetCoinCount ( int coins )
    {
        User.coinAmount = coins;
        coinField.text = User.coinAmount.ToString ();
    }

    public void DoCoinEffect ( Vector3 startPoint, int maxToGive )
    {
        User.coinAmount = User.coinAmount + maxToGive;

        var go = Instantiate < GameObject > ( coinEmitter );
        go.transform.position = startPoint;

        foreach ( Transform t in go.transform )
        {
            t.GetComponent < CurrencyParticle > ().DoEffect ( target.position );
        }
    }

    public void CoinParticle_OnComplete ( )
    {
        if ( _isDoingCoinEffect == false )
        {
            _isDoingCoinEffect = true;

            DOTween.To ( () => coinField.text.ToInt (0), x => coinField.text  = x.ToString() , User.coinAmount , maximumSeconds )
                .OnComplete ( OnCompeteCoinEffect ).OnKill ( OnCompeteCoinEffect ).Play ();
        }
    }

    public void OnCompeteCoinEffect ()
    {
        coinField.text = User.coinAmount.ToString ();
        _isDoingCoinEffect = false;
    }

    public void MoveToCoinPurchaseScreen ()
    {
        if ( GameManager.Instance.CurrentMenu == GameManager.Menu.SelectMenu )
        {

            selectMenuPages.ShowLastPage ();
           // selectMenuPages.SetCurrentPage ( selectMenuPages.NumberOfPages );   
        }
    }
}
