﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[ExecuteInEditMode]

public class SpriteGridLayout : MonoBehaviour 
{
    [Header ("Grid")]
    public Vector2 gridSize = new Vector2 (5f,5f);
    public Color gridColor = Color.green;
    public bool flipGridX = false;

    [Header ("Layout Options")]
    public RectOffset padding;

    public bool useSpriteSize = true;

    public Vector2 cellSize;
    public Vector2 spacing;
    public GridLayoutGroup.Corner startCorner;
    public GridLayoutGroup.Axis startAxis;
    public TextAnchor childAlignment;
    public GridLayoutGroup.Constraint constraint;
    public int constraintCount;

    private Vector2 topLeft;
    private Vector2 topRight;
    private Vector2 bottomLeft;
    private Vector2 bottomRight;

    private void Update ()
    {
        var children = transform.childCount;

        if ( children  <= 0 ) return;

        Vector3 p0 = transform.position;

        topLeft     = new Vector2 ( p0.x - (gridSize.x * 0.5f), p0.y + (gridSize.y * 0.5f) );
        topRight    = new Vector2 ( p0.x + (gridSize.x * 0.5f), p0.y + (gridSize.y * 0.5f) );
        bottomLeft  = new Vector2 ( p0.x - (gridSize.x * 0.5f), p0.y - (gridSize.y * 0.5f) );
        bottomRight = new Vector2 ( p0.x + (gridSize.x * 0.5f), p0.y - (gridSize.y * 0.5f) );

        if ( constraintCount <= 0 ) return;

        int columns = constraintCount; // (int) ( gridSize.x / cellSize.x );
        int rows    = (children + constraintCount - 1) / constraintCount; //(int) ( ( ( children * cellSize.x ) / gridSize.x  ) + 1 );


        for ( int row = 0; row < rows; row++ )
        {
            for ( int column = 0; column < columns; column++ )
            {
                var index = NumberExtensions.GetIndex ( row, column, columns );

                if  ( index >= children ) break;

                var child = transform.GetChild ( index );

                CalculateSize ( child );

                var sX = ( columns - ( ( column + 1 ) + column ) ) * ( (cellSize.x + spacing.x) * 0.5f );

                sX = flipGridX ? -sX : sX;

                var pX = p0.x  + sX;
                var pY = p0.y + ( rows    - ( ( row    + 1 ) + row    ) ) * ( (cellSize.y + spacing.y) * 0.5f );

                child.position = new Vector3 ( pX , pY );
            }
        }
    }

   
    private void CalculateSize ( Transform child )
    {
        if ( child == null ) return;

        if ( useSpriteSize )
        {
            SpriteRenderer renderer = child.GetComponentInChildren < SpriteRenderer > ();

            if ( renderer == null ) return;

            Sprite sprite = renderer.sprite;

            if ( sprite == null ) return;

            cellSize = sprite.bounds.size;

            cellSize.x = cellSize.x * renderer.transform.localScale.x;
            cellSize.y = cellSize.y * renderer.transform.localScale.y;
        }
    }

    public void OnDrawGizmosSelected ()
    {
        Gizmos.color = gridColor;

        Gizmos.DrawLine ( topLeft ,     topRight );
        Gizmos.DrawLine ( bottomLeft,   bottomRight );
        Gizmos.DrawLine ( topLeft,      bottomLeft );
        Gizmos.DrawLine ( topRight,     bottomRight );
    }

    public void SetAsParent ( Transform other )
    {
        other.SetParent ( transform, false );
        other.SetAsLastSibling ();
    }
}
