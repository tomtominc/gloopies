﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Engine2D;
using UnityEngine.Analytics;
using System;

public class FinishLevelEvent : BoardEvent
{
    private const string kWinSound = "Win";
    private const string kPopupSound = "Popup";

    public FinishLevelEvent(Cell target)
    {
        this.target = target;
    }

    public override float Resolve()
    {
        if (!GameManager.Instance.levelEndEventOn)
            return 0f;

        float sequenceDuration = 1f;

        SoundManager.Instance.PlayRandomSoundFX(string.Format("{0}_Win", target.Data.PieceType), 0.1f);
        SoundManager.Instance.PlaySoundFX("WinGame");

        bool setGO = false;

        if (target.Piece)
        {
            target.Piece.OnGameOver();
        }

        Timer timer = new Timer(sequenceDuration, () =>
            {
                if (!setGO && target.Piece != null)
                    target.Piece.OnGameOver();


                Game.GameOver(target);
            });

        timer.Start(GameManager.Instance);

        return sequenceDuration;
    }
}
