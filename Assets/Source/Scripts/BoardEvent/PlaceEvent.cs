﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlaceEvent : BoardEvent
{
    public PlaceEvent(Cell target )
    {
        this.target = target;
    }

    public override float Resolve()
    {
        if ( target == null ) return 0f;
        if ( target.Data == null ) return 0f;

        if ( target.Data.PieceType == Piece.Type.None ) return 0f;

        if ( target.Data.TileType != Tile.Type.NONE )
        {
            BoardEvent e = target.Tile.GetEvent ( target );
            CurrentBoard.AddEvent( e );
        }

        return 0f;
    }
}
