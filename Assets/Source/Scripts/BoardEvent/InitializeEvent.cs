﻿using UnityEngine;
using System.Collections;

public class InitializeEvent : BoardEvent 
{
    Layer.KeyType type;

    public InitializeEvent ( Cell target, Layer.KeyType type )
    {
        this.target = target;
        this.type = type;
    }

    // this is a visual initialize, meaning we need to create the whatever it is that's on the board. in order
    public override float Resolve()
    {
        float duration = 0f;

        if ( type == Layer.KeyType.TILE && target.Data.TileType != Tile.Type.NONE )
        {
            Tile tile = CellFactory.Instance.Create<Tile> ( target.Data.tileKey );
            if ( tile == null ) return 0f;

            duration = tile.DoInitalEnter();

            target.SetTile(tile);
        }

        else if ( type == Layer.KeyType.ITEM && target.Item != null )
        {
            duration = target.Item.DoInitalEnter ();
        }

        else if ( type == Layer.KeyType.PIECE && target.Data.PieceType != Piece.Type.None )
        {
            Piece piece = CellFactory.Instance.Create<Piece> ( target.Data.pieceKey );

            if (piece == null ) return 0f;

            duration = piece.DoInitalEnter();
            target.SetPiece(piece);

            GameManager.Instance.CurrentBoard.RegisterPlacement(target);
        }

        return duration;
    }


}
