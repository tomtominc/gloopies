﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using PotionSoup.Engine2D;

public class MatchEvent : BoardEvent 
{
    public Cell[] removals;
    private const string kMatchSound = "Match";
    public Piece.Type targetColor;

    public MatchEvent ( Cell target, Cell[] removals )
    {
        this.target = target;
        this.removals = removals;
    }

    public override float Resolve()
    {
        float duration = 0.2f;

        if ( removals.Length > 0 ) 
        {
            targetColor = removals[0].Piece.Color;
        }

        foreach ( var cell in removals )
        {
            Vector2 targetPos = (Vector2) target.position + (cell.Piece.offset * 2f);
            cell.Piece.transform.DOMove( targetPos , duration ).OnComplete( cell.Refresh ).Play ();

            Cell.Direction direction = cell.GetDirection ( target );

            if ( direction != Cell.Direction.None )
            {
                cell.Piece.OnSlide ( direction );
            }
        }

        target.StartCoroutine ( DoCombineAnimation ( duration * 0.8f ) );

        duration += 0.5f;
        GameManager.Instance.CurrentBoard.RegisterPlacement(target);

        return duration;
    }

    private IEnumerator DoCombineAnimation ( float duration )
    {
        yield return new WaitForSeconds ( duration );

        SoundManager.Instance.PlaySoundFX ( "MatchSplat" );
        target.Refresh();

        yield return new WaitForEndOfFrame ();
        target.Piece.OnCombine ( target, removals );
    }

}
