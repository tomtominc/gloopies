﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BoardEvent 
{
    public Cell target;
    public GameManager Game
    {
        get { return GameManager.Instance; }
    }
    public BoardManager CurrentBoard 
    {
        get { return Game.CurrentBoard; }
    }
    public LevelManager LevelManager
    {
        get { return Game.LevelManager; }
    }
    public abstract float Resolve ();
}
