﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public partial class SROptions
{
    [Category ("Game Settings") ]
    public int Coins
    {
        get { return User.coinAmount; }

        set { GameManager.Instance.inventoryManager.SetCoinCount ( value ); }
    }
}
