﻿using UnityEngine;
using System.Collections;
using PotionSoup.Persistent;
using UnityEngine.SceneManagement;
using System.ComponentModel;

public partial class SROptions
{
    [Category("Persistent Settings")] 
    public void ClearAllSaveSettings ()
    {
        PersistentSettings.Clear ();

        SceneManager.LoadScene ( SceneManager.GetActiveScene ().name );
    }
}
