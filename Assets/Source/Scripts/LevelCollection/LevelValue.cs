﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelValue
{
    [SerializeField]
    public LevelData data;

    [SerializeField]
    public LevelState state;

    [SerializeField]
    public TextAsset asset;

    [SerializeField]
    public int order;

    [SerializeField]
    public string path;
}
