﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ObjectiveData 
{
    public Dialogue[] solution;

    public ObjectiveData  ()
    {
        solution = new Dialogue[0];
    }

    public ObjectiveData ( ObjectiveData data )
    {
        solution = new Dialogue[data.solution.Length];

        for (int i = 0; i < data.solution.Length; i++)
        {
            Dialogue dialogue = data.solution[i];

            if ( dialogue == null )
            {
                Debug.LogWarning ( "Dialogue is null");
                continue;
            }

            solution[i] = new Dialogue (-1,-1,"");

            solution[i].row = dialogue.row;
            solution[i].column = dialogue.column;
            solution[i].text = dialogue.text;
        }
    }

    public Dialogue Last ()
    {
        if ( solution == null ) return null;
        if ( solution.Length <= 0 ) return null;

        return solution [ solution.Length - 1 ];
    }
}
