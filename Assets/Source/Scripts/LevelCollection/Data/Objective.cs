﻿using UnityEngine;
using System.Collections;

public class Objective : MonoBehaviour
{
    public Transform _coin;
    public Transform _slot;

    public void Set ( bool complete )
    {
        _slot.gameObject.SetActive ( true );
        _coin.gameObject.SetActive ( complete );
    }

    public void Disable ()
    {
        _coin.gameObject.SetActive ( false );
        _slot.gameObject.SetActive ( false );
    }
}

