﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class DialogueHelper : MonoBehaviour 
{
    public Image dialogue;
    public TextField dialogueField;
    public GameObject tileMask;

    public void Enter ( Cell cell )
    {
        dialogue.rectTransform.DOAnchorPos ( new Vector2 ( 0f, -50f ), 0.5f ).SetEase ( Ease.OutBack ).Play ();

        gameObject.SetActive(true);

        if ( cell != null )
        {
            tileMask.SetActive (true);
            tileMask.transform.position = cell.position;
        }
        else
        {
            tileMask.SetActive(false);
        }
    }


    public void StartTyping ( string text )
    {
        if ( string.IsNullOrEmpty ( text ) == false )
        {
            dialogue.gameObject.SetActive( true );
            dialogueField.Text = I2.Loc.ScriptLocalization.Get ( text );
        }
        else
        {
            dialogue.gameObject.SetActive ( false );
        }

    }

    public float DialogueExit ()
    {
        dialogue.rectTransform.DOAnchorPos ( new Vector2 ( 0f, -250f ), 0.5f ).SetEase ( Ease.InBack ).Play ();

        return 0.6f;
    }

    public void Exit ()
    { 
        dialogue.rectTransform.DOAnchorPos ( new Vector2 ( 0f, -250f ), 0.5f ).SetEase ( Ease.InBack ).OnComplete ( () => gameObject.SetActive ( false ) ).Play ();
      
    }

}
