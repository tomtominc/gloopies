﻿[System.Serializable]
public class Coordinate 
{
    public Coordinate () { }

    public Coordinate ( int row, int column )
    {
        this.row = row;
        this.column = column;
    }

    public int row = 0;
    public int column = 0;
}
