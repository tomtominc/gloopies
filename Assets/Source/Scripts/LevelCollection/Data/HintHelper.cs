﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class HintHelper : MonoBehaviour 
{
    public float offsetY = 2f;

    public bool hasEntered { get; set; }

    private Animator _animator
    {
        get { return GetComponent < Animator > (); }   
    }

    private bool _moving = false;

    private float _speed = 0.05f;

    public bool IsMoving 
    {
        get { return _moving; }
    }

    public void Exit ()
    {
        gameObject.SetActive( false );

        hasEntered = false;
    }

    public void Enter ( Cell cell )
    {
        hasEntered = true;

        gameObject.SetActive( true );

        // have the hint enter on the current cell
        transform.position =  GetMovePosition ( cell );
    }

    public void MoveToCell ( Cell cell )
    {
        _moving = true;


        if ( gameObject.IsActive () == false ) return;


        Vector3 position = GetMovePosition ( cell );

        GetComponent < RectTransform > ().DOMove 
        ( 
            position,
            Vector3.Distance ( transform.position, position ) * _speed
        ).
        SetEase (Ease.InOutQuad).
        OnComplete ( () =>
            {
                _moving = false; 
            }).Play ();
    }

    private Vector3 GetMovePosition ( Cell cell )
    {
        return new Vector3 ( cell.position.x, cell.position.y + offsetY );
    }

}
