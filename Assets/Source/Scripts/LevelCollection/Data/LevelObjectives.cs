﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using PotionSoup.Persistent;
using System;

public class LevelObjectives : MonoBehaviour 
{
    public Sprite completeSprite;
    public GameObject completeIcon;

    private Image image 
    {
        get { return GetComponent < Image > (); }
    }

    public void Set ( LevelState state )
    {
        ObjectiveState[] objectives = state.objectiveStates;

        if ( objectives.Length <= 0 || state.locked )
        {
            gameObject.SetActive ( false );
            completeIcon.SetActive ( false );
            return;
        }

        Objective[] objectiveViews = GetComponentsInChildren < Objective > (true);

        if ( state.AllObjectivesComplete () )
        {
            image.sprite = completeSprite;
            completeIcon.SetActive (true);

            foreach ( var objective in objectiveViews )
            {
                objective.Disable ();
            }

            return;
        }

        completeIcon.SetActive ( false );

        for (int i = 0; i < transform.childCount; i++)
        {
            Objective objective = objectiveViews[i];

            if ( objectives.Length <= i ) 
            {
                objective.Disable ();

                continue;
            }

            Cell.SolutionValue value = (Cell.SolutionValue)(i+1);

            bool complete = state.GetObjectiveIsComplete ( value );

            objective.Set (complete);
        }
    }
}
