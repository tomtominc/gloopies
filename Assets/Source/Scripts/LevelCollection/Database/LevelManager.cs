﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    public string levelCollectionFolderPath = "LevelAssets/Collections";

    public static string ResourceFolder
    {
        get { return string.Format ("{0}/Resources/", Application.dataPath ); }
    }

    [SerializeField]
    public List < LevelCollection > levelCollections = new List < LevelCollection > ();

    private int _currentCollection = 0;
    private int _currentLevel = 0;

    public bool doReload = false;


    public int CurrentCollectionIndex 
    {
        get { return _currentCollection; }
    }

    public int TotalCollections
    {
        get { return levelCollections.Count; }
    }

    public LevelCollection[] Collections
    {
        get { return levelCollections.ToArray (); }
    }

    public LevelCollection CurrentCollection
    {
        get { return levelCollections [_currentCollection]; }
    }

    public LevelData CurrentLevelData
    {
        get { 
            return CurrentCollection.GetLevelData (_currentLevel ); }
    }

    public LevelState CurrentLevelState
    {
        get { return CurrentCollection.GetLevelState ( _currentLevel ); }
    }

    public LevelState NextLevel
    {
        get 
        {
            LevelState state = null;

            if ( _currentLevel >= CurrentCollection.Count - 1 && _currentCollection >= TotalCollections - 1 )
            {
                state = null;
            }
            else if ( _currentLevel >= CurrentCollection.Count - 1 )
            {
                state = GetLevelState ( _currentCollection + 1, 0 );
            }
            else
            {
                state = GetLevelState ( _currentCollection, _currentLevel + 1 );    
            }

            return state;
        }
    }

    public void Initialize ()
    {
        foreach ( var collection in levelCollections )
        {
            collection.Initialize ();
        }
    }

    public LevelState GetLevelState ( int collection, int level )
    {
        if ( levelCollections.Count <= collection )
        {
            Debug.LogWarningFormat ( "Could not retrieve level at collection {0}", collection);
            return null;
        }

        return levelCollections [ collection ].GetLevelState ( level );
    }

    public const int MAX_LEVEL_COUNT = 25;

    public int GetLevelNumber ( int collection, int level )
    {
       
        int realLevel = 0;

        for ( int i = 0; i < collection; i++ )
        {
            realLevel += MAX_LEVEL_COUNT;
        }

        realLevel += level;

        return realLevel;
    }

    public void SetCurrentLevel ( int collection, int levelNumber )
    {
        _currentCollection = collection;
        _currentLevel = levelNumber;
    }

    public void NewSession ()
    {
        CurrentLevelState.NewSession ();
    }

    public void Next ()
    {
        if ( _currentLevel >= CurrentCollection.Count - 1 && _currentCollection >= TotalCollections - 1 )
        {
            // nothing here
        }
        else if ( _currentLevel >= CurrentCollection.Count - 1 )
        {
            _currentLevel = 0;
            _currentCollection++;
        }
        else
        {
            _currentLevel++;    
        }
    }

    public bool LevelsRemain
    {
        get { return !( _currentLevel >= CurrentCollection.Count - 1 && _currentCollection >= TotalCollections - 1 );}
    }

    public bool NextIsGated
    {
        get { return _currentLevel == 9; }
    }



    public void Reload ()
    {
        levelCollections = levelCollectionFolderPath.LoadAllResources < LevelCollection > (true);
    }

}

public enum Difficulty
{
    None, Easy, Moderate, Difficult, Advanced, Extreme, Remove
}