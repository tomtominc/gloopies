﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PotionSoup.Persistent;
using System.Linq;
using UnityEngine.Analytics;
using System;
using System.IO;

[System.Serializable]
public class LevelCollection : ScriptableObject 
{
    private bool _locked = true;
    private bool _gateLocked = true;

    [SerializeField]
    public string collectionName = string.Empty;
    [SerializeField]
    public int id = 0;
    [SerializeField]
    public int cost = 0;
    [SerializeField]
    public int gateCost = 100;
    [SerializeField]
    public string folderPath = "Levels/";

    public bool locked
    {
        get { return PersistentSettings.GetValue( string.Format ("kLocked_{0}", folderPath ) , true ); }

        set 
        {
            if ( _locked != value )
            {
                _locked = value;
                PersistentSettings.SetValue( string.Format ("kLocked_{0}", folderPath ) , _locked );
            }
        }
    }

    public bool gateLocked
    {
        get { return PersistentSettings.GetValue( string.Format ("kGateLocked_{0}", folderPath ) , true ); }

        set 
        {
            if ( _gateLocked != value )
            {
                _gateLocked = value;
                PersistentSettings.SetValue( string.Format ("kGateLocked_{0}", folderPath ) , _gateLocked );
            }
        }
    }

    public string folderName 
    {
        get { return folderPath.Split ('/')[1];  }
    }

    [SerializeField]
    public List < LevelValue > LevelValues = new List<LevelValue> ();

    public int Count
    {
        get { return LevelValues.Count; }
    }

    public void Initialize ()
    {
        GenerateData ();
    }

    public void UnlockAllLevels ()
    {
        foreach ( var value in LevelValues  )
        {
            value.state.locked = false;
        }
    }

    public LevelData GetLevelData ( int index )
    {
        return LevelValues [ index ].data;
    }

    public LevelState GetLevelState ( int level )
    {
        if ( level >= Count )
        {
            Debug.LogWarningFormat ("Trying to get level state at index: {0} when the count is {1}.", level, Count );
            return null;
        }
        return LevelValues[level].state;
    }

    public bool ContainsLevel ( string level )
    {
        return PersistentSettings.Contains ( level );
    }

    public void LevelEvent ( LevelState state )
    {
//        Analytics.CustomEvent ( Key ( state.levelName ), 
//            new Dictionary<string, object> ()
//            {
//                { "Restart Count", state.restartCount },
//                { "Undo Count", state.undoCount },
//                { "Cleared Count", state.clearedCount },
//                { "Perfect Clear", state.perfectClear }
//            }
//        );
    }

    private string Key ( string level )
    {
        return string.Format ("{0}/{1}", collectionName, level );
    }


    public void Reload ()
    {
        List<TextAsset> levelAssets = folderPath.LoadAllResources< TextAsset >(true);

        LevelValues = new List<LevelValue> ();

        foreach ( var asset in levelAssets )
        {
            LevelValues.Add 
            ( 
                new LevelValue ()
                {
                    asset = asset,
                    path  = asset.name
                }
            );
        }
    }

    public void DeleteLevel ( LevelValue value )
    {
        string path = string.Format ( "{0}/{1}", LevelManager.ResourceFolder, folderPath );
        string deletePath = string.Format ( "{0}/{1}", Application.dataPath, "Delete" );

        string oldPath = string.Format ("{0}/{1}.json", path, value.data.jsonFile );
        string newPath = string.Format ("{0}/{1}.json", deletePath, value.data.jsonFile );

        if (File.Exists ( newPath ) )
        {
            File.Delete ( oldPath );  
        }
        else 
        {
            File.Move ( oldPath , newPath );
        }
    }

    public void MoveLevel ( LevelValue value )
    {
        string path = string.Format ( "{0}/{1}", LevelManager.ResourceFolder, folderPath );

        string oldPath = string.Format ("{0}/{1}.json", path, value.asset.name );
        string newPath = string.Format ("{0}/{1}.json", path, value.data.jsonFile );

        if (File.Exists ( newPath ) )
        {
            File.Delete ( oldPath );  
        }
        else 
        {
            File.Move ( oldPath , newPath );
        }
    }

    public void GenerateData ()
    {

        foreach ( var value in  LevelValues )
        {
            value.data = JSONSerializer.Deserialize < LevelData > ( value.asset.text );

            if ( value.data.collection != folderName )
            {
                value.data.collection = folderName;
            }

            if ( value.asset.name != value.data.jsonFile )
            {
                MoveLevel ( value );
            }

        }

        LevelValues = LevelValues.OrderBy ( x => x.data.id ).ToList ();

        foreach ( var value in LevelValues )
        {
            LevelState state = new LevelState ( value.data.name, value.data.id);

            state.levelName = value.data.name;
            state.levelNumber = value.data.id;

            var list = value.data.objectives.ToList ();

            list.RemoveAll ( x => x.solution.Length <= 0 );

            value.data.objectives = list.ToArray ();

            state.objectiveStates = new ObjectiveState[ value.data.objectives.Length ];

            value.state = state;
        }
    }

}
