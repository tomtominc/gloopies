﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using PotionSoup.Persistent;
using System.Collections.Generic;
using System.Linq;
using UnityEditorInternal;
using Rotorz.ReorderableList;
using UnityEditor.Graphs;
using System.IO;
using System.Diagnostics;

[CustomEditor(typeof(LevelCollection))]

public class LevelCollectionInspector : Editor 
{
    private LevelCollection collection;
    private SerializedProperty levelStates;

    enum EditMode 
    {
        None, Load, Save, Lock, Unlock
    }


    public void OnEnable ()
    {  
        collection = target as LevelCollection;
        levelStates = serializedObject.FindProperty("LevelValues");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUIExtensions.DrawHeader("Collection Settings", true, false );

        EditorGUIExtensions.BeginContent(Color.white);
        {

            collection.collectionName = EditorGUILayout.TextField("Collection Name", collection.collectionName );
            collection.id = EditorGUILayout.IntField("Collection ID", collection.id);
            collection.cost = EditorGUILayout.IntField( "Collection Cost", collection.cost);
            collection.gateCost = EditorGUILayout.IntField( "Gate Cost", collection.gateCost );

            EditorGUILayout.BeginHorizontal();
            {
                collection.folderPath = EditorGUILayout.TextField("Folder Path", collection.folderPath);

                EditorGUIExtensions.SetBackgroundColor( EditorColor.green );

                if ( GUILayout.Button( "R" , GUILayout.Width( 32f ) ) )
                {
                    collection.Reload ();
                }

                EditorGUIExtensions.RestoreBackgroundColor();
            }
            EditorGUILayout.EndHorizontal();


        }
        EditorGUIExtensions.EndContent();

        EditMode editMode = EditMode.None;

        bool levelSettingsFoldout = EditorGUIExtensions.DrawHeader( "Level Settings", true, false );

        if ( ! levelSettingsFoldout ) return;

        EditorGUIExtensions.BeginContent( Color.white);
        {

            EditorGUILayout.BeginHorizontal();
            {
                if ( GUILayout.Button ( "Load" , EditorStyles.toolbarButton) )
                {
                    collection.GenerateData();
                }

                if ( GUILayout.Button ( "Save" , EditorStyles.toolbarButton ) )
                {
                    editMode = EditMode.Save;
                }

                if ( GUILayout.Button ( "Lock", EditorStyles.toolbarButton ) )
                {
                    editMode = EditMode.Save;

                    foreach ( var value in collection.LevelValues )
                    {
                        value.state.locked = true;
                    }
                }

                if ( GUILayout.Button ( "Unlock" , EditorStyles.toolbarButton))
                {
                    editMode = EditMode.Save;

                    foreach ( var value in collection.LevelValues )
                    {
                        value.state.locked = false;
                    }
                }

            }
            EditorGUILayout.EndHorizontal();

           
            ReorderableListGUI.Title( "Level States" );
            ReorderableListGUI.ListField(levelStates);



        }
        EditorGUIExtensions.EndContent();

        if ( editMode == EditMode.Save )
        {
            Save(); 
        }


        serializedObject.ApplyModifiedProperties();



    }

    public void Save ()
    {
        for ( int i = 0; i < collection.LevelValues.Count; i++ )
        {
            LevelValue value = collection.LevelValues [i];

            value.state.levelNumber = i;

            value.data.name = value.state.levelName;
            value.data.id   = value.state.levelNumber;

            string json = JSONSerializer.Serialize < LevelData > ( value.data, true );

            json = json.Replace ( '*', '0' );

            string path = string.Format( "{0}/{1}/{2}/{3}.json", Application.dataPath, "Resources", collection.folderPath, value.data.jsonFile );
            File.WriteAllText( path, json  );
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        collection.Reload ();
        collection.GenerateData();
    }

    [MenuItem("Assets/Create/Level Collection")]
    public static void CreateAsset ()
    {
        ScriptableObjectUtility.CreateAsset<LevelCollection> ();
    }
}
