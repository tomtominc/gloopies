﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using PotionSoup.Persistent;

[CustomPropertyDrawer(typeof(LevelState))]
public class LevelStateDrawer : PropertyDrawer 
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 16f * 2;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var levelName = property.FindPropertyRelative("levelName");
        var levelNumber = property.FindPropertyRelative("levelNumber");
        var locked = property.FindPropertyRelative("locked");
        //var difficulty = property.FindPropertyRelative()
        float defaultHeight = 16f;

        Rect rect = new Rect (position.x, position.y, position.width - 16f,position.height * 0.5f );

        EditorGUI.PropertyField(rect, levelName);

        rect.y += defaultHeight;

        EditorGUI.PropertyField(rect,levelNumber);

        Rect button_rect = new Rect(position.width + 32f, position.y, 16f, 32f );

        GUI.color = locked.boolValue ? Color.red : EditorColor.green;

        locked.boolValue =  GUI.Toggle ( button_rect, locked.boolValue , string.Empty, "IN LockButton");

        GUI.color = Color.white;
    }
}

[CustomPropertyDrawer(typeof(LevelValue))]
public class LevelValueDrawer : PropertyDrawer 
{
    int nProperties = 6;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 16f * nProperties;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        
        var levelState = property.FindPropertyRelative ("state");
        var levelData = property.FindPropertyRelative ("data");
        var levelName = levelState.FindPropertyRelative("levelName");
        var levelNumber = levelState.FindPropertyRelative("levelNumber");
        var levelAsset = property.FindPropertyRelative ( "asset");


        var key =  string.Format ("{0}-{1}", levelName.stringValue, levelNumber.intValue );

        var lockedKey = string.Format ( "{0}-kLocked", key );

        var locked = PersistentSettings.GetValue ( lockedKey, true );

        var _locked = locked;

        float defaultHeight = 16f;

        Rect rect = new Rect (position.x, position.y, position.width - defaultHeight , position.height / nProperties );


        EditorGUI.PropertyField ( rect, levelAsset );

        rect.y += defaultHeight;

        EditorGUI.PropertyField(rect, levelName);

        rect.y += defaultHeight;

        EditorGUI.PropertyField(rect,levelNumber);

        rect.y += defaultHeight;

        _locked = EditorGUI.Toggle ( rect, "Locked", _locked );

        if ( _locked != locked )
            PersistentSettings.SetValue ( lockedKey, _locked );

        rect.y += defaultHeight;

        var difficulty = levelData.FindPropertyRelative ( "difficulty" );

        EditorGUI.LabelField ( rect, "Difficulty", ((Difficulty) difficulty.intValue).ToString ());
    }
}
