﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using PotionSoup.Persistent;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(LevelManager))]
public class LevelManagerInspector : Editor 
{
    private LevelManager manager;

    public void OnEnable ()
    {
        manager = target as LevelManager;
    }

    public void GenerateData ()
    {
        manager.levelCollections = manager.levelCollections.OrderBy ( x => x.id ).ToList ();

        for (int i = 0; i < manager.levelCollections.Count; i++ )
        {
            var collection = manager.levelCollections[i];

            string title = "Reloading Collections";
            string info = string.Format ( "Loading Collection {0}", collection.collectionName );
            float progress = (float) (((float)i) / ((float) manager.levelCollections.Count));

            EditorUtility.DisplayProgressBar (title, info, progress);

            collection.Reload ();
            collection.GenerateData ();
            SaveCollection ( collection );
        }


        EditorUtility.ClearProgressBar ();

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

    }

    [MenuItem ("Tools/Save Settings/Clear")]
    public static void ClearSaveSettings ()
    {
        PersistentSettings.Clear ();
    }

    public void SaveCollection ( LevelCollection collection )
    {
        for ( int i = 0; i < collection.LevelValues.Count; i++ )
        {
            LevelValue value = collection.LevelValues [i];

            value.state.levelNumber = i;

            value.data.name = value.state.levelName;
            value.data.id   = value.state.levelNumber;

            string json = JSONSerializer.Serialize < LevelData > ( value.data, true );


            string path = string.Format( "{0}/{1}/{2}/{3}.json", Application.dataPath, "Resources", collection.folderPath, value.data.jsonFile );
            File.WriteAllText( path, json  );
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

        collection.Reload ();
        collection.GenerateData();
    }


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector ();

        manager.levelCollections.RemoveAll ( x => x == null );

        EditorGUIExtensions.DrawHeader ( "Level Manager Settings" );

        EditorGUIExtensions.BeginContent ( Color.white );

        EditorGUILayout.BeginHorizontal();
        {
            manager.levelCollectionFolderPath = EditorGUILayout.TextField ("Collection Path", manager.levelCollectionFolderPath );

            EditorGUIExtensions.SetBackgroundColor( EditorColor.green );

            if ( GUILayout.Button( "R" , GUILayout.Width( 32f ) ) )
            {
                manager.doReload = true;
            }

            EditorGUIExtensions.RestoreBackgroundColor();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUIExtensions.EndContent ();

        EditorGUIExtensions.DrawHeader ( "Collection Settings" );

        EditorGUIExtensions.BeginContent ( Color.white );

        DrawCollections ();

        EditorGUIExtensions.EndContent ();

        if ( manager.doReload )
        {
            manager.doReload = false;

            manager.Reload ();
            GenerateData ();
        }


        if (  GUILayout.Button ( "Swap Color Pieces") )
        {
            string[] swapKeys = new string[3]
                {
                    "CS:GP:BP", "CS:GP:RP", "CS:BP:RP"  
                };
            
            foreach ( var collection in manager.levelCollections )
            {
                foreach ( var value in collection.LevelValues )
                {
                    for ( int i = 0; i < value.data.tile.Length; i++ )
                    {
                        string key = value.data.tile [i];

                        for ( int j = 0; j < swapKeys.Length; j++ )
                        {
                            if ( key.Equals ( swapKeys [j] ) )
                            {
                                Debug.Log ( key );
                                value.data.piece [i] = DoSwap ( value.data.piece[i], key );
                            }
                        }
                    }
                }

                SaveCollection ( collection );
            }
        }
       
    }

    private string DoSwap ( string pieceKey, string tileKey )
    {
        string [] splitTileKey = tileKey.Split (':');

        for ( int i = 0; i < splitTileKey.Length; i++ )
        {
            if ( splitTileKey[i].Equals ( pieceKey ) )
            {
                Debug.LogFormat ( "Swap the from {0} to {1} using the tile key {2}", pieceKey, splitTileKey [ i > 1 ? 1 : 2 ], tileKey );
                return splitTileKey [ i > 1 ? 1 : 2 ];
            }
        }

        Debug.LogWarningFormat ( "Not a valid swap. Piece Key = {0} , Tile Key = {1}", pieceKey , tileKey );

        return pieceKey;
    }

    private void DrawCollections ()
    {
        EditorGUILayout.BeginHorizontal ();

        EditorGUIExtensions.SetBackgroundColor ( EditorColor.green );

        if ( GUILayout.Button ( "Refresh", EditorStyles.toolbarButton ) )
        {
            manager.doReload = true;
        }

        EditorGUIExtensions.RestoreBackgroundColor ();

        EditorGUILayout.EndHorizontal ();

        foreach ( var state in manager.levelCollections )
        {
            bool selected = EditorGUIExtensions.DrawHeader ( state.name , false );

            if ( selected )
            {
                EditorGUIExtensions.BeginContent ( EditorColor.grey );

                state.name       = EditorGUILayout.TextField ( "Collection Name"      , state.name );
                state.id         = EditorGUILayout.IntField  ( "Collection Number"    , state.id   );
                state.locked     = EditorGUILayout.Toggle    ( "Locked"               , state.locked );
                state.cost       = EditorGUILayout.IntField  ( "Cost"                 , state.cost );
                state.gateLocked = EditorGUILayout.Toggle    ( "Gate Locked"          , state.gateLocked );
                state.gateCost   = EditorGUILayout.IntField  ( "Gate Cost"            , state.gateCost );

                EditorGUIExtensions.EndContent ();

            }
        }
    }
}
