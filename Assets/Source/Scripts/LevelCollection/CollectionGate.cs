﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using PotionSoup.MenuManagement;

public class CollectionGate : MonoBehaviour 
{
    public LevelCollection collection;
    public Text costField;
    public bool isGate;

    public GameObject gate;
    public Image whiteShimmer;
    public float fadeDuration = 0.5f;

    public GameObject betaIcon;
    public GameObject betaObject;

    private CollectionGridView view;

    public GameObject popupPrefab;

    private GameManager _gameManager
    {
        get { return GameManager.Instance; }
    }

    private RectTransform rectTransform
    {
        get { return GetComponent < RectTransform > (); }
    }

    public int cost 
    {
        get 
        {
            #if BETA

            return isGate ? collection.gateCost : 0;

            #else 

            return isGate ? collection.gateCost : collection.cost;

            #endif
        }
    }

    public bool locked 
    {
        get 
        {
            #if BETA 

            return isGate ? collection.gateLocked : false;

            #else


            return isGate ? collection.gateLocked : collection.locked;

            #endif
        }
        set 
        {
            if ( isGate )
            {
                collection.gateLocked = value;
            }
            else
            {
                collection.locked = value;
            }
        }
    }

    public int firstLevel 
    {
        get 
        {
            return isGate ? 10 : 0;
        }
    }


    private void Start ()
    {
       
        TryRefresh ();
  
    }

    public void TryRefresh ( )
    {

        view = transform.parent.GetComponentInChildren < CollectionGridView > ();

        RefreshGate ();

        CheckForUnlocked ();

    }

    public void CheckForUnlocked ()
    {
        #if BETA

        if ( betaObject )
            betaObject.SetActive ( true );

        if ( betaIcon )
            betaIcon.SetActive ( true );

        gameObject.SetActive ( locked  );

        if ( locked == false ) view.UnlockLevel ( 0 );

        costField.text = cost.Pretty();

        #else

        if ( betaObject )
            betaObject.SetActive ( false );

        if ( betaIcon )
            betaIcon.SetActive ( false );

        costField.text = cost.Pretty();

        gameObject.SetActive ( locked );

        #endif
    }

    public void RefreshGate ()
    {
        Vector2 offset = new Vector3 ( 0f, -25f );
        switch ( _gameManager.deviceType )
        {
            case GameManager.DeviceType.iPhone4:

                if ( isGate )
                {
                    rectTransform.offsetMin = new Vector2 ( 24f, 86f ) + offset;
                    rectTransform.offsetMax = new Vector2 ( -24f, -196f )  + offset ;
                }
                else
                {
                    rectTransform.offsetMin = new Vector2 ( 24f, 295f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -24f, -58f )+ offset;
                }

                break;
            case GameManager.DeviceType.iPhone5:

                if ( isGate )
                {
                    rectTransform.offsetMin = new Vector2 ( 24f, 80f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -24f, -235f )+ offset;
                }
                else
                {

                    rectTransform.offsetMin = new Vector2 ( 24f,   350f)+ offset;
                    rectTransform.offsetMax = new Vector2 ( -24f,  -65f)+ offset;
                }

                break;

            case GameManager.DeviceType.iPad:

                if ( isGate )
                {
                    rectTransform.offsetMin = new Vector2 ( 48f, 72f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -48f, -215f )+ offset;
                 
                }
                else
                {
                    rectTransform.offsetMin = new Vector2 ( 48f, 312f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -48f, -57f )+ offset;
                }

                break;

            case GameManager.DeviceType.WSVGA:

                if ( isGate )
                {
                    rectTransform.offsetMin = new Vector2 ( 20f, 72f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -20f, -237f )+ offset;
                }
                else
                {
                    rectTransform.offsetMin = new Vector2 ( 20f, 338f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -20f, -63f )+ offset;
                }

                break;

            case GameManager.DeviceType.WVGA:

                if ( isGate )
                {
                    rectTransform.offsetMin = new Vector2 ( 20f, 72f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -20f, -230f )+ offset;
                }
                else
                {
                    rectTransform.offsetMin = new Vector2 ( 20f, 325f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -20f, -60f )+ offset;
                }

                break;

            default:

                if ( isGate )
                {
                    rectTransform.offsetMin = new Vector2 ( 20f, 72f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -20f, -215f )+ offset;
                }
                else
                {
                    rectTransform.offsetMin = new Vector2 ( 20f, 312f )+ offset;
                    rectTransform.offsetMax = new Vector2 ( -20f, -60f )+ offset;
                }

                break;
        }
    }

    public void Disable ()
    {
        gameObject.SetActive( false );
    }

    // plugged into a button
    public void UnlockGate ()
    {
        #if BETA

        string title = "NO BETA ACCESS";
        string dialogue = string.Format ( "The full version of Gloopies comes out in July! Keep checking back here for updates!" );
        string accept = "Okay";
        string decline = string.Empty;

        GameObject clone = Instantiate < GameObject > ( popupPrefab );

        clone.transform.SetParent ( MenuManager.Instance.transform, false );

        Popup popup = clone.GetComponent < Popup > ();

        popup.Initialize (title, dialogue, accept, decline, null , null );

        #else

        if ( User.coinAmount >= cost )
        {
            GameManager.Instance.inventoryManager.SetCoinCount ( User.coinAmount - cost );
            whiteShimmer.raycastTarget = true;

            locked = false;

            StartCoroutine ( UnlockSequence () );
        }
        else 
        {
            GameManager.Instance.Pagination.ShowLastPage ();
        }

        #endif
    }

    // FIX : SHOULD UNLOCK THE GATE PERMENANTLY

    public IEnumerator UnlockSequence ()
    {
        whiteShimmer.DOFade ( 1f, fadeDuration ).Play ();

        yield return new WaitForSeconds ( fadeDuration + 1f);

        gate.SetActive ( false );

        whiteShimmer.DOFade ( 0f, fadeDuration ).Play ();

        view.UnlockLevel ( firstLevel );

        yield return new WaitForSeconds ( fadeDuration );

        gameObject.SetActive ( false );

    }
}
