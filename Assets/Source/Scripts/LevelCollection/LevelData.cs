﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class LevelData
{
    public string name = string.Empty;
    public string collection = string.Empty;
    public int id = 1;
    public int rows = 6;
    public int columns = 9;
    public int difficulty = 1;
    public bool isSpecialTutorial = false;
    public int specialTutorialIndex = 0;

    [SerializeField]
    public Dialogue[] tutorial;
    [SerializeField]
    public ObjectiveData[] objectives;

    public string[] piece;
    public string[] tile;
    public string[] item;

    public LevelData ()
    {
        tutorial = new Dialogue[0];
        objectives = new ObjectiveData[0];

        piece = new string[0];
        tile = new string[0];
        item = new string[0];
    }

    public string path = string.Empty;

    public string jsonFile
    {
        get 
        { 
            return string.Format ( "{0}_{1}_{2}.json", collection, string.Format ("{0}x{1}", rows, columns ), name.Replace (" ", "") ); 
        }
    }

    public bool IsTutorialLevel ()
    {
        return ( tutorial != null && tutorial.Length > 0 );
    }

    public Dictionary < string , object > TryGetSolution ( int row, int column )
    {
        if ( objectives == null ) return null;

        var table = new Dictionary < string , object > ();

        table.Add("ObjectiveData" , null ); 

        for (int i = 0; i < objectives.Length; i++ )
        {
            var obj = objectives[i];

            for ( int j = 0; j < obj.solution.Length; j++ )
            {
                var dialogue = obj.solution[j];

                if ( dialogue.row == row && dialogue.column == column )
                {
                    table[ "ObjectiveData" ] = obj;
                }
            }
        }

        return table; 
    }

    public string GetPieceCode ( int row, int column )
    {
        int index = ( row * columns ) + column;
        if ( index >= piece.Length || index < 0 ) return "0";

        string key = piece [ index ];
        return key;
    }

    public string GetTileCode (int row, int column )
    {
        int index = ( row * columns ) + column;

        if ( index >= tile.Length || index < 0 ) return "0";

        string key = tile [ index ];
        return key;
    }

    public string GetItemCode (int row, int column )
    {
        if (item == null) return "0";

        int index = ( row * columns ) + column;

        if ( index >= item.Length || index < 0 ) return "0";

        string key = item [ index ];
        return key;
    }
}
