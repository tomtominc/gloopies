﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class BaseDrawer : PropertyDrawer 
{
    public float defaultHeight = 16f;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        float height = 0f;

        foreach ( SerializedProperty prop in property ) 
        {
            if ( prop.isExpanded )
            {

            }

            height += defaultHeight;
        }

        return height;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var rect = position;

        rect.height = defaultHeight;

        foreach ( SerializedProperty prop in property )
        {
            EditorGUI.PropertyField (rect, prop );

            rect.y += defaultHeight;
        }
    }    
}
