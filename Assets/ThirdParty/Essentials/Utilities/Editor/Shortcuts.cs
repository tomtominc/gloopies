﻿using UnityEngine;
using UnityEditor;

static class Shortcuts
{
    [MenuItem ("Tools/Clear Console &#c")] // ALT + SHIFT + C
    static void ClearConsole () 
    {
        // This simply does "LogEntries.Clear()" the long way:
        var logEntries = System.Type.GetType("UnityEditorInternal.LogEntries,UnityEditor.dll");
        var clearMethod = logEntries.GetMethod("Clear", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
        clearMethod.Invoke(null,null);
    }

}