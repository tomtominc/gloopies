﻿using UnityEngine;
using System.Collections;
using EasyJSON;

public class JSONSerializer 
{
    public const string password = "k+3>a\\&?1#N3j[K\n";

    public static T Deserialize < T > ( string text )
    {
        //string json = Encryptor.Decrypt ( text , password );
        return Serializer.Deserialize < T > ( text );
    }

    public static string Serialize < T >  ( T value, bool prettyPrint )
    {
        //string encrypted = Encryptor.Encrypt ( Serializer.Serialize < T > ( value, prettyPrint ), password );
        return  Serializer.Serialize < T > ( value, prettyPrint );
    }
}
