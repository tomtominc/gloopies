﻿using UnityEngine;
using System.Collections;
using System;

public class Timer
{
    private float delay;
    private Action action;
    private MonoBehaviour behaviour; 

    public Timer (float delay, Action action)
    {
        this.delay = delay;
        this.action = action; 
    }

    public void Start ( MonoBehaviour behaviour )
    { 
        behaviour.StartCoroutine(DoAction());
    }

    private IEnumerator DoAction ()
    {
        yield return new WaitForSeconds(delay);

        action.Invoke();
    }
}

