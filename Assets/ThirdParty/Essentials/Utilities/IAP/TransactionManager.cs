﻿using UnityEngine;
using System.Collections;
using com.ootii.Messages;
using PotionSoup.MenuManagement;
using System.Collections.Generic;
using TextFx;
using System.Linq;
using System;


//public class TransactionManager : Singleton < TransactionManager >  
//{
//    public GameObject TransactionPopup;
//    public GameObject loadingIndicator;
//    public DynamicButton removeAdsButton;
//
//    public int hint1Cost  = 50;
//    public int hint5Cost  = 200;
//    public int hint10Cost = 350;
//
//    private Dictionary < string , int > _androidProductCodes = new Dictionary<string, int> ()
//    {
//        { "gloopies.coins.80"    , 80    },
//        { "gloopies.coins.500"   , 500   },
//        { "gloopies.coins.x.1200"  , 1200  },
//        { "gloopies.coins.2500"  , 2500  },
//        { "gloopies.coins.x.6500"  , 6500  },
//        { "gloopies.coins.x.14000" , 14000 },
//        { "gloopies.removeads"    , 0  }
//    };
//
//    private Dictionary < string , int > _itunesProductCodes = new Dictionary<string, int> ()
//    {
//        { "gloopies.coins.80"    , 80    },
//        { "gloopies.coins.500"   , 500   },
//        { "gloopies.coins.1200"  , 1200  },
//        { "gloopies.coins.2500"  , 2500  },
//        { "gloopies.coins.6500"  , 6500  },
//        { "gloopies.coins.14000" , 14000 },
//        { "gloopies.removeads"    , 0  }
//    };
//
//
//    private Dictionary < string , int > _productCodes;
//
//    //private List < BillingProduct > productList;
//
//    #region MONOBEHAVIOR_CALL_BACKS
//
//
//    private void Awake ()
//    {
//        if ( Application.platform == RuntimePlatform.Android )
//        {
//            _productCodes = new Dictionary < string , int > ( _androidProductCodes );
//        }
//        else
//        {
//            _productCodes = new Dictionary < string , int > ( _itunesProductCodes );
//        }
//
//
//        MessageDispatcher.AddListener ( TransactionEvent.TryCompleteTransaction.ToString () , OnTryCompleteTransaction , true );
//        MessageDispatcher.AddListener ( GameEvent.SwitchMenu.ToString (), RequestBillingProducts , true );
//    }
//
//    private void OnEnable ()
//    {
//        #if USES_BILLING
//
//        Billing.DidFinishRequestForBillingProductsEvent += OnDidFinishProductsRequest;
//
//        Billing.DidFinishProductPurchaseEvent += OnDidFinishTransaction;
//
//        Billing.DidFinishRestoringPurchasesEvent += OnDidFinishRestoringPurchases;
//
//        #endif
//
//    }
//
//    private void OnDisable ()
//    {
//        #if USES_BILLING
//
//        Billing.DidFinishRequestForBillingProductsEvent -= OnDidFinishProductsRequest;
//
//        Billing.DidFinishProductPurchaseEvent -= OnDidFinishTransaction;
//
//        Billing.DidFinishRestoringPurchasesEvent -= OnDidFinishRestoringPurchases;   
//
//        #endif
//    }
//
//    #endregion
//
//
//    #region API_WRAPPERS
//
//    public void RequestBillingProducts ( IMessage message )
//    {
////        GameManager.Menu menu =  (GameManager.Menu)message.Data;
////
////        if ( productList == null && menu == GameManager.Menu.SelectMenu )
////        {
////            #if USES_BILLING
////
////            NPBinding.Billing.RequestForBillingProducts ( NPSettings.Billing.Products );
////
////            #endif
////        }
//
//    }
//
//    public void RestoreCompletedTransactions ()
//    {
//        #if USES_BILLING
//
//        NPBinding.Billing.RestorePurchases ();
//
//        #endif
//    }
//
//    #endregion
//
//
//    #region EVENT_LISTENTERS
//
////    private void OnDidFinishProductsRequest (BillingProduct[] _regProductsList, string _error)
////    {   
////        
////        loadingIndicator.SetActive ( false );
////
////        if (_error != null)
////        {        
////            DisplayErrorMessage ( _error );
////
////        }
////        else 
////        {
////            productList = new List<BillingProduct> ();
////
////            for ( int i = 0; i < _regProductsList.Length; i++ )
////            {
////                BillingProduct product = _regProductsList[i];
////
////                productList.Add ( product );
////            }
////
////            var dataTable = new Hashtable ()
////                {
////                    { "ProductList", productList }
////                };
////
////
////            
////            MessageDispatcher.SendMessage ( this,  TransactionEvent.FinishProductRequest.ToString (), dataTable , 0f );
////         
////        }
////    }
////
////
////    private void OnDidFinishRestoringPurchases (BillingTransaction[] _transactions, string _error)
////    {
////
////        if (_transactions != null)
////        {               
////            foreach (BillingTransaction _currentTransaction in _transactions)
////            {
////                if ( _currentTransaction.ProductIdentifier == "gloopies.removeads" )
////                {
////                    User.removedAds = true;
////
////                    removeAdsButton.DoDisable ();
////
////                    DisplaySuccessMessage ( "Successfully restored your Remove Ads purchase!" );
////                }
////            }
////        }
////        else
////        {
////            DisplayErrorMessage ( _error );
////        }
////    }
////
////    private void OnDidFinishTransaction (BillingTransaction _transaction)
////    {
////        loadingIndicator.SetActive ( false );
////
////        if (_transaction != null)
////        {
////
////            if (_transaction.VerificationState == eBillingTransactionVerificationState.SUCCESS)
////            {
////                if (_transaction.TransactionState == eBillingTransactionState.PURCHASED)
////                {
////                    if ( _transaction.ProductIdentifier.Equals ( "gloopies.removeads") )
////                    {
////                        User.removedAds = true;
////                        removeAdsButton.DoDisable ();
////                        DisplaySuccessMessage ( "Successfully purchased Removed Ads! Thank you so much!" );
////                    }
////                    else
////                    {
////                        int coins = _productCodes [ _transaction.ProductIdentifier ];
////
////                        GameManager.Instance.inventoryManager.DoCoinEffect ( Vector3.zero,  coins );
////                    }
////                }
////                else if ( _transaction.TransactionState == eBillingTransactionState.RESTORED )
////                {
////                    if ( _transaction.ProductIdentifier.Equals ( "gloopies.removeads") )
////                    {
////                        removeAdsButton.DoDisable ();
////                        User.removedAds = true;
////                        DisplaySuccessMessage ( "Successfully restored your Remove Ads purchase!" );
////                    }
////                }
////
////            }
////            else if ( _transaction.VerificationState == eBillingTransactionVerificationState.FAILED )
////            {
////                DisplayErrorMessage ( string.Format ( "{3}n\nVerification State: {0} \nTransaction State: {1} \nTransaction ID: {2}" , _transaction.VerificationState, _transaction.TransactionState , _transaction.TransactionIdentifier, _transaction.Error ) );
////            }
////            else
////            {
////                DisplayErrorMessage ( string.Format ( "{3}\nSomething went wrong please try again.}\nVerification State: {0} \nTransaction State: {1} \nTransaction ID: {2}" , _transaction.VerificationState, _transaction.TransactionState , _transaction.TransactionIdentifier, _transaction.Error )  );
////            }
////        }
////    }
//
//    #endregion
//
//    public void BuyItem ( string id )
//    {
//        #if USES_BILLING
//
//        if (productList == null || productList.Count <= 0 ) 
//        {
//            DisplayErrorMessage ( "The product list came back with nothing!" );
//            return;
//        }
//
//        BillingProduct product = productList.Find ( x => x.ProductIdentifier.Equals ( id ) );
//
//        if ( product == null )
//        {
//            DisplayErrorMessage ( string.Format ( "Could not find product with ID {0}", id  ) );
//            return;
//        }
//
//        BuyItem ( product );
//        #endif
//    }
//
//    public void BuyItem (/*BillingProduct _product*/)
//    {
//        #if USES_BILLING
//        if (NPBinding.Billing.IsProductPurchased(_product))
//        {
//            DisplayErrorMessage ( "Product has already been purchased!" );
//
//            return;
//        }
//
//        NPBinding.Billing.BuyProduct(_product);
//
//        if ( loadingIndicator )
//        {
//            loadingIndicator.GetComponentInChildren < TextFxUGUI > ().SetText ( "Purchasing" );
//            loadingIndicator.SetActive (true);
//        }
//
//        #endif
//    }
//
//   
//
//
//    private void OnTryCompleteTransaction ( IMessage message )
//    {
//        TransactionData transactionData = (TransactionData)message.Data;
//
//        switch ( transactionData.identifier )
//        {
//            case Transaction.Hint1:  TryCompleteHintTransaction( hint1Cost  , 1  );  break;
//            case Transaction.Hint5:  TryCompleteHintTransaction( hint5Cost  , 5  );  break;
//            case Transaction.Hint10: TryCompleteHintTransaction( hint10Cost , 10 );  break;
//                
//        }
//    }
//
//    private void TryCompleteHintTransaction ( int cost , int numHints )
//    {
//        if ( User.coinAmount >= cost )
//        {
//            User.coinAmount -= cost;
//            DisplaySuccessfulHintPopup ( numHints );
//        }
//        else
//        {
//            PurchaseEnoughCoins ();
//        }
//    }
//
//    private void DisplaySuccessfulHintPopup ( int numHints )
//    {
//        string title = string.Format ( I2.Loc.ScriptLocalization.Get ( "Popup/Hint/Success/Title") , numHints );
//        string dialogue = I2.Loc.ScriptLocalization.Get ( "Popup/Hint/Success/Dialogue");
//        string accept = I2.Loc.ScriptLocalization.Get ( "Popup/General/Yes");
//
//        GameObject clone = Instantiate < GameObject > ( TransactionPopup );
//
//        clone.transform.SetParent ( MenuManager.Instance.transform, false );
//
//        Popup popup = clone.GetComponent < Popup > ();
//
//        popup.Initialize (title, dialogue, accept, string.Empty, null, null );
//
//        User.totalHints += numHints;
//    }
//
//    private void PurchaseEnoughCoins ()
//    {
//        BuyItem ( "gloopies.coins.500" );
//    }
//
//    public void PurchaseRemoveAds ()
//    {
//        BuyItem ( "gloopies.removeads" );
//    }
//
//
//    #region UTILITIES
//
//    public void DisplayErrorMessage ( string error )
//    {
//        string title = "ERROR";
//        string dialogue = error;
//        string accept = "OK";
//
//        GameObject clone = Instantiate < GameObject > ( TransactionPopup );
//
//        clone.transform.SetParent ( GameManager.Instance.overlayCanvas , false );
//
//        Popup popup = clone.GetComponent < Popup > ();
//
//        popup.Initialize (title, dialogue, accept, string.Empty, null, null );
//    }
//
//    public void DisplaySuccessMessage ( string success )
//    {  
//        string title = "SUCCESS";
//        string dialogue = success;
//        string accept = "OK";
//
//        GameObject clone = Instantiate < GameObject > ( TransactionPopup );
//
//        clone.transform.SetParent ( GameManager.Instance.overlayCanvas , false );
//
//        Popup popup = clone.GetComponent < Popup > ();
//
//        popup.Initialize (title, dialogue, accept, string.Empty, null, null );
//        
//    }
//
//    #endregion
//
//    public void LoadSelectMenu ()
//    {
//        
//        StartCoroutine ( LoadMenu ( GameManager.Menu.SelectMenu , 0f, null ) );
//    }
//
//
//    private IEnumerator LoadMenu ( GameManager.Menu menu, float callBackDelay, Action callback, bool useSplashScreen = true )
//    {
//        SoundManager.Instance.InternalMusicEnabled = false;
//
//        MessageDispatcher.SendMessage ( this, GameEvent.SwitchMenu.ToString (), menu, 0f );
//
//        if ( GameManager.Instance.CurrentMenu == GameManager.Menu.GameMenu )
//        {
//            GameManager.Instance.CurrentBoard.DisableAll ();
//        }
//
//        GameManager.Instance.CurrentMenu = menu;
//
//        GameManager.Instance.DisableControls ();
//
//        float duration = 0f;
//
//        if ( useSplashScreen )
//        {
//            duration = GameManager.Instance.splashScreen.DoStartSplashEffect ();
//        }
//
//        yield return new WaitForSeconds (duration);
//
//        MenuManager.Instance.SetMenuAllMenusActive ( false );
//
//        MenuManager.Instance.OpenMenu (menu.ToString(), 0.0f);
//
//        if ( MenuManager.Instance.GetMenu ( menu.ToString () ).gameObject.IsActive () == false )
//        {
//
//            MenuManager.Instance.GetMenu ( menu.ToString () ).gameObject.SetActive ( true );
//        }
//
//
//        yield return new WaitForSeconds ( 0.1f );
//
//        if ( callback != null ) callback.Invoke ();
//
//        if ( useSplashScreen )
//        {
//            duration = GameManager.Instance.splashScreen.DoEndSplashEffect();
//        }
//
//        yield return new WaitForSeconds( duration );
//
//        if ( menu != GameManager.Menu.GameMenu )
//        {
//            SoundManager.Instance.InternalMusicEnabled = true;
//            SoundManager.Instance.PlayMusic ( menu.ToString () );
//        }
//
//        GameManager.Instance.EnableControls ();
//    }
//
//}
//
//public enum TransactionEvent
//{
//    TryCompleteTransaction, FinishProductRequest
//}
//
//public enum Transaction
//{
//    Hint1, Hint5, Hint10
//}
//
//public class TransactionData
//{
//    public Transaction identifier;
//}
