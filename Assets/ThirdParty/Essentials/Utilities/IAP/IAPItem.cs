﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Purchasing;

public class IAPItem : MonoBehaviour 
{
    public Text productName;
    public Text productCost;
    public Button buyButton;
    public Image icon;

    private string _uid;
    private ProductMetadata _metadata;
    private Sprite _iconSprite;

    [SerializeField]
    public List < ItemContent > IAPContents = new List<ItemContent> ();

    public void Initialize ( Product product, Action < string > buyAction )
    {
        _uid = product.definition.id;
        _metadata = product.metadata;

        _iconSprite = IAPContents.FirstOrDefault ( x => x.key == _uid ).icon;

        buyButton.onClick.RemoveAllListeners ();

        buyButton.onClick.AddListener 
        ( 
            () => 
            {
                buyAction.Invoke ( _uid );
            }
        );

        Refresh ();
    }

    public void Refresh ()
    {
        name = string.Format ( "Product: {0}", _metadata.localizedTitle );

        productName.text = _metadata.localizedTitle;
        productCost.text = _metadata.localizedPriceString;

        icon.sprite = _iconSprite;

        icon.GetComponent < DynamicIcon > ().SetAllSprites ( _iconSprite, _iconSprite , _iconSprite );
    }

    [System.Serializable]
    public class ItemContent
    {
        public string key;
        public Sprite icon;
    }
}

