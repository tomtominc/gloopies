﻿using UnityEngine;
using UnityEngine.UI;
using com.ootii.Messages;
using System.Collections;
using UnityEngine.Purchasing;
using System;
using System.Collections.Generic;
using System.Linq;


[RequireComponent ( typeof (GridLayoutGroup)) ]
public class IAPGridView : MonoBehaviour
{
    public GameObject iAPItem;
    public GameObject displayNoBillingGUI;
    public const string kRemoveAds  = "gloopies.removeads";

    private void Awake ()
    {
        MessageDispatcher.AddListener ( PurchaseEvent.OnFinishedInitialize.ToString () , OnFinishedProductRequest , true );
    }

    private void OnFinishedProductRequest ( IMessage message )
    {
        var dataTable = message.Data as Hashtable;

        var productList = dataTable ["ProductList"] as List < Product >;

        transform.DestroyChildren();

        productList = productList.OrderBy ( x => x.metadata.localizedPrice ).ToList();


        foreach ( Product product in productList )
        {
            if ( string.Equals(product.definition.id , kRemoveAds, StringComparison.Ordinal) ) continue;

            var go = Instantiate < GameObject > (iAPItem);

            go.transform.SetParent ( transform, false );

            var item = go.GetComponent < IAPItem > ();

            item.Initialize ( product , BuyItem );
        }
    }

    private void BuyItem ( string productID )
    {
        MessageDispatcher.SendMessage ( this , PurchaseEvent.BuyItem.ToString () , productID , 0f );
    }
  
}
