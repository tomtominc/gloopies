﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class EditorColor
{
    public static Color red { get { return HexToColor("FF5252FF"); } }

    public static Color pink { get { return HexToColor("FF8F8FFF"); } }

    public static Color green { get { return HexToColor("48FF00FF"); } }

    public static Color greenyellow { get { return HexToColor ("70FF83FF"); } }

    public static Color blue { get { return HexToColor("00F4FFFF"); } }

    public static Color bluelight { get { return HexToColor ("BFF2FDFF"); }}

    public static Color orange { get { return HexToColor("F39A69FF"); } }

    public static Color yellow { get { return HexToColor("FFD700FF"); } }

    public static Color greyblue { get { return HexToColor("A5B7BBFF"); } }

    public static Color grey { get { return HexToColor("8C8C8C05"); } }

    public static Color HexToColor(string hex)
    {
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        return new Color32(r, g, b, 255);
    }
}

public static class EditorGUIExtensions
{
    public static string ToolsPath = "Assets/PotionSoup.Essentials/Tools/";

    static public int DrawSelectableHeader(string aSelectionID, params string[] aText)
    {
        GUILayout.Space(3f);
        GUILayout.BeginHorizontal();
        GUILayout.Space(3f);
		
        GUI.changed = false;

        int selectedHeader = EditorPrefs.GetInt("SelectableHeader_" + aSelectionID, 0);
        for (int i = 0; i < aText.Length; i++)
        {
            string text = "<size=11>" + aText[i].ToString() + "</size>";
            if (i == selectedHeader)
            {
                text = "<b>" + text + "</b>";
            }
            if (!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f)))
            {
                selectedHeader = i;
            }
        }
        EditorPrefs.SetInt("SelectableHeader_" + aSelectionID, selectedHeader);

        GUILayout.Space(2f);
        GUILayout.EndHorizontal();

        return selectedHeader;
    }

    public static int DrawSelectableHeader(string aSelectionID, GUIStyle style, Color activeColor, params object[] aText)
    {
        GUILayout.Space(3f);
        GUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        GUI.changed = false;

        int selectedHeader = EditorPrefs.GetInt("ToolbarHeader_" + aSelectionID, 0);
        for (int i = 0; i < aText.Length; i++)
        {
            string text = "<size=11>" + aText[i].ToString() + "</size>";
            Color headerColor = i == selectedHeader ? activeColor : Color.white;
            SetBackgroundColor(headerColor);

            if (i == selectedHeader)
            {
                text = "<b>" + text + "</b>";
            }
            if (!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f)))
            {
                selectedHeader = i;
            }

            RestoreBackgroundColor();
        }
        EditorPrefs.SetInt("ToolbarHeader_" + aSelectionID, selectedHeader);

        GUILayout.Space(2f);
        GUILayout.EndHorizontal();

        return selectedHeader;
    }

    public static string DrawSearchToolbar(string m_BrowserFilter)
    {
        GUILayout.Space(2f);
        EditorGUIExtensions.SetBackgroundColor(Color.white);
        EditorGUILayout.BeginHorizontal((GUIStyle)"Toolbar");
        m_BrowserFilter = EditorGUILayout.TextField(m_BrowserFilter, new GUIStyle("ToolbarSeachTextField"), GUILayout.ExpandWidth(true));
        if (GUILayout.Button(string.Empty, "ToolbarSeachCancelButton"))
        {
            m_BrowserFilter = string.Empty;
        }
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(2f);
        EditorGUIExtensions.RestoreBackgroundColor();

        return m_BrowserFilter;
    }

    //public static bool IsPressingKey ()

    public static GUIStyle GetFontStyle(string baseStyle, int fontSize, TextAnchor anchor, FontStyle fontStyle)
    {
        GUIStyle _fontStyle = new GUIStyle(baseStyle);
        _fontStyle.richText = true;
        _fontStyle.fontSize = fontSize;
        _fontStyle.alignment = anchor;
        _fontStyle.fontStyle = fontStyle;

        return _fontStyle;
    }

    public static bool ToggleButton(string buttonName, string key, GUIStyle style,  Color onColor, params GUILayoutOption[] options)
    {
        bool toggle = EditorPrefs.GetBool("ToggleButtonAuto" + key, false);

        Color bg = toggle ? onColor : Color.white;
		
        EditorGUIExtensions.SetBackgroundColor(bg);
		
        if (GUILayout.Button(buttonName, style, options))
        {
            toggle = !toggle;
            EditorPrefs.SetBool("ToggleButtonAuto" + key, toggle);
        }
		
        EditorGUIExtensions.RestoreBackgroundColor();
		
        return toggle;
    }

    public static void DisableToggleGroup ( string[] buttonNames )
    {
        foreach ( var buttonName in buttonNames )
        {
            EditorPrefs.SetBool ("ToggleButtonGroup" + buttonName, false );
        }

    }

    public static string ToggleButtonGroup(string[] buttonNames, GUIStyle style,  Color onColor, params GUILayoutOption[] options)
    {
        string currentButton = string.Empty;

        foreach ( var buttonName in buttonNames )
        {
            bool toggle = EditorPrefs.GetBool("ToggleButtonGroup" + buttonName, false);

            Color bg = toggle ? onColor : Color.white;

            if ( toggle )
                currentButton = buttonName;

            EditorGUIExtensions.SetBackgroundColor(bg);

            if (GUILayout.Button(buttonName, style, options))
            {
                foreach ( var key in buttonNames )
                {
                    EditorPrefs.SetBool ("ToggleButtonGroup" + key, false );
                }

                EditorPrefs.SetBool("ToggleButtonGroup" + buttonName, true );

                currentButton = buttonName;
            }

            EditorGUIExtensions.RestoreBackgroundColor();
        }

        return currentButton;
    }



    public static void DisplayProperty(bool display, SerializedProperty property)
    {
        if (display)
        {
            EditorGUILayout.PropertyField(property, true);
        }
    }

    public static string LabelField ( string label, string text, GUIStyle style, Color backgroundColor )
    {

        SetBackgroundColor(backgroundColor);

        text = EditorGUILayout.TextField(label,text,style);

        RestoreBackgroundColor();

        return text;
    }

    public static void DrawScriptField (SerializedObject serializedObject )
    {
        serializedObject.Update();
        SerializedProperty prop = serializedObject.FindProperty("m_Script");
        EditorGUILayout.PropertyField(prop, true, new GUILayoutOption[0]);
        serializedObject.ApplyModifiedProperties();
    }

    static public bool DrawHeader(string aText, bool aDefaultValue = true, bool aForceDefault = false)
    {
        if (aForceDefault)
        {
            EditorPrefs.SetBool("DrawHeader_" + aText, aDefaultValue);
        }
        bool clicked = EditorPrefs.GetBool("DrawHeader_" + aText, aDefaultValue);
        GUILayout.Space(3f);
        GUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        string text = "<size=11>" + aText + "</size>";
        if (clicked)
        {
            text = "<b>" + text + "</b>";
        }
        if (!GUILayout.Toggle(true, text, "dragtab", GUILayout.MinWidth(20f)))
        {
            clicked = !clicked;
            EditorPrefs.SetBool("DrawHeader_" + aText, clicked);
        }
		
        GUILayout.Space(2f);
        GUILayout.EndHorizontal();

        return clicked;
    }

    static public bool DrawHeader(string aText, Color backgroundColor, bool aDefaultValue = true, bool aForceDefault = false)
    {
        EditorGUIExtensions.SetBackgroundColor(backgroundColor);

        bool value = DrawHeader(aText,aDefaultValue,aForceDefault);

        EditorGUIExtensions.RestoreBackgroundColor();

        return value;
    }

    public static void DrawTitle(string title)
    {
		
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField(title, new GUIStyle("OL Title"));

        EditorGUILayout.Space();
        EditorGUILayout.Space();
    }

    public static Rect DrawBox(string aLabel, float aHeight, GUIStyle style, Color aBackgroundColor, params GUILayoutOption[] options)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        style.richText = true;
        style.alignment = TextAnchor.MiddleCenter;
		
        SetBackgroundColor(aBackgroundColor);
        Rect boxArea = GUILayoutUtility.GetRect(0.0f, aHeight, options);
        GUI.Box(boxArea, aLabel, style);
        RestoreBackgroundColor();
		
        GUILayout.Space(2f);
        EditorGUILayout.EndHorizontal();
		
        return boxArea;
    }

    static public void BeginContent(Color aBackgroundColor, params GUILayoutOption[] options)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(3f);
		
        GUIStyle style = new GUIStyle("TextArea");
        style.richText = true;
        style.margin = new RectOffset(0, 0, 0, 0);
		
        SetBackgroundColor(aBackgroundColor);
        EditorGUILayout.BeginVertical(style, options);
    }

    public static bool Button(string text, GUIStyle style, Color color, params GUILayoutOption[] options)
    {
        EditorGUIExtensions.SetBackgroundColor(color);
        if (GUILayout.Button(text, style, options))
        {
            return true;
        }
        EditorGUIExtensions.RestoreBackgroundColor();

        return false;
    }

    public static void BeginContent(GUIStyle style, Color aBackgroundColor, params GUILayoutOption[] options)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(3f);

        style.richText = true;
        style.margin = new RectOffset(0, 0, 0, 0);

        SetBackgroundColor(aBackgroundColor);
        EditorGUILayout.BeginVertical(style, options);
    }

    static public void EndContent()
    {
        EditorGUILayout.EndVertical();

        RestoreBackgroundColor();
		
        GUILayout.Space(2f);
        EditorGUILayout.EndHorizontal();
    }

    public static bool DrawFoldout(bool value, Color aBackgroundColor, string header, string key)
    {
        value = EditorPrefs.GetBool("FoldoutValue_" + key, false);

        SetBackgroundColor(aBackgroundColor);
        EditorGUILayout.BeginHorizontal((GUIStyle)"TE NodeBox");
        SetBackgroundColor(Color.white);
        GUILayout.Space(16f);
        value = EditorGUILayout.Foldout(value, header);

        RestoreBackgroundColor();
        EditorGUILayout.EndHorizontal();
        RestoreBackgroundColor();
        EditorPrefs.SetBool("FoldoutValue_" + key, value);

        return value;
    }

    public static bool DrawFoldoutAuto(string header, string key, GUIStyle style, Color aBackgroundColor, int textSize)
    {
        bool value = EditorPrefs.GetBool("FoldoutValue_" + key, false);

        SetBackgroundColor(aBackgroundColor);
        EditorGUILayout.BeginHorizontal(style);
        SetBackgroundColor(Color.white);
        GUILayout.Space(16f);

        value = EditorGUILayout.Foldout(value, header );

        RestoreBackgroundColor();
        EditorGUILayout.EndHorizontal();

        RestoreBackgroundColor();

        EditorPrefs.SetBool("FoldoutValue_" + key, value);

        return value;
    }

    public static void SetEditorPrefColor(string aKey, Color aColor)
    {
        EditorPrefs.SetFloat(aKey + "_r", aColor.r);
        EditorPrefs.SetFloat(aKey + "_g", aColor.g);
        EditorPrefs.SetFloat(aKey + "_b", aColor.b);
        EditorPrefs.SetFloat(aKey + "_a", aColor.a);
    }

    public static Color GetEditorPrefColor(string aKey)
    {
        Color color = Color.white;
        color.r = EditorPrefs.GetFloat(aKey + "_r", 1f);
        color.g = EditorPrefs.GetFloat(aKey + "_g", 1f);
        color.b = EditorPrefs.GetFloat(aKey + "_b", 1f);
        color.a = EditorPrefs.GetFloat(aKey + "_a", 1f);
        return color;
    }


	

    private static Stack<Color> s_BackgroundColors = new Stack<Color>();

    public static void SetBackgroundColor(Color aBackground)
    {
        s_BackgroundColors.Push(GUI.backgroundColor);
        GUI.backgroundColor = aBackground;
    }

    public static void RestoreBackgroundColor()
    {
        GUI.backgroundColor = s_BackgroundColors.Pop();
    }

}



