﻿using System;
using System.Linq;
using System.Collections.Generic;
using PotionSoup.Engine2D;
/* *****************************************************************************
 * File:    EnumExtensions.cs
 * Author:  Philip Pierce - Wednesday, September 24, 2014
 * Description:
 *  Extensions for Enums
 *  
 * History:
 *  Wednesday, September 24, 2014 - Created
                                                                                 * ****************************************************************************/
using System.ComponentModel;

/// <summary>
/// Extensions for Enums
/// </summary>
public static class EnumExtensions
{
    #region ToEnum

    /// <summary>
    /// Converts a string to an enum
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="s"></param>
    /// <param name="ignoreCase">true to ignore casing in the string</param>
    public static T ToEnum<T>(this string s, bool ignoreCase) where T : struct
    {
        // exit if null
        if (s.IsNullOrEmpty())
            return default(T);

        Type genericType = typeof(T);
        if (!genericType.IsEnum)
            return default(T);

        try
        {
            return (T) Enum.Parse(genericType, s, ignoreCase);
        }

        catch (Exception)
        {
            // couldn't parse, so try a different way of getting the enums
            Array ary = Enum.GetValues(genericType);
            foreach (T en in ary.Cast<T>()
                .Where(en => 
                    (string.Compare(en.ToString(), s, ignoreCase) == 0) ||
                    (string.Compare((en as Enum).ToString(), s, ignoreCase) == 0)))
                    {
                        return en;
                    }

            return default(T);
        }
    }

	/// <summary>
	/// Checks if at least any pFlags bits are set on pEnums (have to be marked with'[Flags]').
	/// </summary>
	public static bool IsSet<T>(this T pEnum, T pFlags) where T : struct, IConvertible
	{
		if (!typeof(T).IsEnum)
			throw new ArgumentException("Type must be an enum name");
		
		var lEnum = Convert.ToInt64(pEnum);
		var lFlags = Convert.ToInt64(pFlags);
		
		return ((lEnum & lFlags) > 0);
	}

//	public static bool IsNotSet(this CharacterEventHandler.ShowingStateEvents stateEvent, CharacterEventHandler.ShowingStateEvents flags)
//	{
//		return (stateEvent & (~flags)) == 0;
//	}


    /// <summary>
    /// Converts a string to an enum
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="s"></param>
    public static T ToEnum<T>(this string s) where T : struct
    {
        return s.ToEnum<T>(false);
    }

    public static int Count (this Enum e)
    {
        return Enum.GetNames(e.GetType()).Length;
    }


    public static bool TryParse<T>(this Enum theEnum, string valueToParse, out T returnValue)
    {
        returnValue = default(T);    
        if (Enum.IsDefined(typeof(T), valueToParse))
        {
            TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
            returnValue = (T)converter.ConvertFromString(valueToParse);
            return true;
        }
        return false;
    }

    // ToEnum
    #endregion
}