﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace PotionSoup.DoTweenPlugin
{
	[CustomEditor (typeof(TweenMotion), true)]
	public class TweenMotionEditor : Editor
	{
		protected TweenMotion _tweenMotion;
		protected string _key = string.Empty;
		protected TweenType _valueMotion = TweenType.LocalMove;

		protected Dictionary < TweenType, MotionPresent> presentMap
        = new Dictionary<TweenType, MotionPresent > () {
			{ TweenType.LocalMove, new MotionPresent (TweenType.LocalMove) }
		};

		protected SerializedProperty sequenceList;

		public void OnEnable ()
		{
			_tweenMotion = target as TweenMotion;
			sequenceList = serializedObject.FindProperty ("sequences");
		}

		public override void OnInspectorGUI ()
		{
			DrawDefaultInspector ();

			EditorGUIExtensions.DrawTitle ("Global Settings");

			List<string> headers = new List<string> ();
			foreach (var motion in _tweenMotion.sequences) {
				headers.Add (motion.key);
			}

			EditorGUILayout.BeginHorizontal ();


			_key = EditorGUILayout.TextField ("New Sequence ID", _key);
			_valueMotion = (TweenType)EditorGUILayout.EnumPopup (_valueMotion);

			EditorGUIExtensions.SetBackgroundColor( EditorColor.bluelight );
			if ( GUILayout.Button("ADD","minibutton",GUILayout.Width(32f)))
			{
				if (_key != string.Empty) {
					_tweenMotion.CreateNewSequence (_key, presentMap [_valueMotion]);
				} else {
					Debug.LogWarning ("Not a valid string"); 
				}
			}
			EditorGUIExtensions.RestoreBackgroundColor();

			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();
			_tweenMotion.autoPlayIndex = EditorGUILayout.Popup ("Auto Play Sequence",_tweenMotion.autoPlayIndex, headers.ToArray());
			_tweenMotion.autoPlay = EditorGUILayout.Toggle(_tweenMotion.autoPlay,GUILayout.Width(16f));
			EditorGUILayout.EndHorizontal ();

			if (_tweenMotion.sequences.Count <= 0)
			{
				EditorGUILayout.HelpBox("No Sequences, add a sequence using the GUI Above.", MessageType.Info);
				return;
			}

			EditorGUIExtensions.DrawTitle ("Sequence Editor");


			int i = EditorGUIExtensions.DrawSelectableHeader ("TweenMotionEditorSelectableHeader", new GUIStyle ("dragtab"),
				EditorColor.greenyellow, headers.ToArray ());

			if ( i < 0 || i >= _tweenMotion.sequences.Count) return;

			MotionSequence currentSequence = _tweenMotion.sequences[i];
			EditorGUIExtensions.BeginContent (Color.white);
			List<MotionPresent> presents = currentSequence.value;
			List<MotionPresent> motionsToDelete = new List<MotionPresent>();

			int newIndex = -1;
			int oldIndex = -1;

			for (int j = 0; j < presents.Count; j++)
			{
				MotionPresent present = presents[j];

				present.ID = _tweenMotion.sequences [i].key;
				Color color = (j % 2) < 1 ? new Color(0.95f,1f,0.95f) : new Color(0.95f,0.95f,1f);
				EditorGUIExtensions.BeginContent(color);
				EditorGUILayout.BeginHorizontal();

				EditorGUILayout.LabelField (string.Format("[{0}]",j),GUILayout.Width(18f));
				Color editColor = present.editMode ? EditorColor.red : Color.grey;
				EditorGUIExtensions.SetBackgroundColor( editColor );

				if ( GUILayout.Button("EDIT","minibutton",GUILayout.Width(32f)))
				{
					present.editMode = !present.editMode;
				}
				EditorGUIExtensions.RestoreBackgroundColor();

				EditorGUILayout.LabelField (string.Format("{0} [{1}]",present.tweenType.ToString(),present.ID));
				EditorGUIExtensions.SetBackgroundColor( Color.grey );
				if ( GUILayout.Button("UP","minibutton",GUILayout.Width(32f)))
				{
					oldIndex = j;
					newIndex = j - 1;
				}
				//EditorGUIExtensions.RestoreBackgroundColor();
				if ( GUILayout.Button("DN","minibutton",GUILayout.Width(32f)))
				{
					oldIndex = j;
					newIndex = j + 1;
				}
				EditorGUIExtensions.RestoreBackgroundColor();
				EditorGUIExtensions.SetBackgroundColor( EditorColor.red);
				if ( GUILayout.Button("X","minibutton",GUILayout.Width(32f)))
				{
					motionsToDelete.Add (present);
				}
				EditorGUIExtensions.RestoreBackgroundColor();
				EditorGUILayout.EndHorizontal();

				if (!present.editMode)
				{
					EditorGUIExtensions.EndContent(); continue;
				}
				else 
				{
					CommonDrawers.DrawMotionPresent (present, _tweenMotion.transform);
					EditorGUIExtensions.EndContent();
				}
			}

			if (newIndex >= 0 && newIndex < presents.Count)
			{
				presents.Move<MotionPresent> (oldIndex,newIndex);
			}

			foreach ( var present in motionsToDelete)
			{
				presents.Remove(present);
			}

			EditorGUIExtensions.EndContent ();

			EditorGUIExtensions.DrawTitle(string.Format("Sequence Settings [{0}]",currentSequence.key));

			EditorGUIExtensions.BeginContent(Color.white);
			EditorGUILayout.BeginHorizontal ();

			bool hasNext = currentSequence.nextSequenceIndex > -1;
			string nextSequenceName = "None"; 

			if (hasNext )
			{
				nextSequenceName = _tweenMotion.sequences[currentSequence.nextSequenceIndex].key;
			}

			var buttonName = hasNext? "<color=#D6FFB5FF>"+ nextSequenceName +"</color>" : "<color=#B0B0B0FF>"+nextSequenceName+"</color>";
			var buttonColor = hasNext ? EditorColor.greenyellow : Color.grey ;

			EditorGUIExtensions.SetBackgroundColor(buttonColor);

			if (GUILayout.Button(string.Format("Next Sequence => {0}",buttonName),"button"))
			{
				currentSequence.nextSequenceIndex = hasNext ? -1 : 0;
			}

			EditorGUIExtensions.RestoreBackgroundColor();

			EditorGUIExtensions.SetBackgroundColor( EditorColor.red );
			if (GUILayout.Button(string.Format("DELETE [{0}] SEQUENCE", currentSequence.key), "button"))
			{
				_tweenMotion.sequences.RemoveAt(i);
			}
			EditorGUIExtensions.RestoreBackgroundColor();
			EditorGUIExtensions.SetBackgroundColor( EditorColor.bluelight );
			if ( GUILayout.Button("ADD","button",GUILayout.Width(32f)))
			{
				_tweenMotion.CreateNewSequence(currentSequence.key, new MotionPresent(TweenType.LocalMove));
			}
			EditorGUIExtensions.RestoreBackgroundColor();
			EditorGUILayout.EndHorizontal();



			if (hasNext )
				currentSequence.nextSequenceIndex = EditorGUILayout.Popup("Next Sequence: ",currentSequence.nextSequenceIndex, headers.ToArray());


			EditorGUIExtensions.EndContent ();

		}

	}
}