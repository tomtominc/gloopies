﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace PotionSoup.DoTweenPlugin
{
    public class TweenMotion : MonoBehaviour 
    {
		public static MotionPresent globalCopy;

        [HideInInspector]
        public List < MotionSequence > sequences = new List<MotionSequence> ();

        // cached sequences set up by ID
        protected Dictionary < string , Sequence > motionSequenceMap = new Dictionary<string, Sequence >();

		protected MotionSequence _currentMotion;
        protected Sequence _currentSequence;

		[HideInInspector] public bool autoPlay = false;
		[HideInInspector] public int autoPlayIndex = 0;

		public void Start ()
		{
			if (autoPlay && autoPlayIndex >= 0 && autoPlayIndex < sequences.Count)
			{
				DoPlayByID(sequences [autoPlayIndex].key);
			}	
		}

        public bool Contains ( string key )
        {
            return sequences.Contains(Get(key));
        }

        public MotionSequence Get ( string key )
        {
            return sequences.Find(x => x.key == key);
        }

        public void CreateNewSequence ( string key, MotionPresent value )
        {
            MotionSequence sequenceFind = Get(key); 
            if ( sequenceFind != null )
            {
                sequenceFind.value.Add ( value );
                return;
            }

            MotionSequence sequence = new MotionSequence ( key , value );
            sequences.Add( sequence );
        }

        public void DoPlayByID ( string ID )
        {
			_currentMotion = Get(ID); 

			if ( _currentMotion == null ) return;

            if ( _currentSequence != null )
            {
                _currentSequence.Pause();
            }

            Sequence sequence = null;
     
            if (motionSequenceMap.ContainsKey(ID) != false)
            {
                sequence = motionSequenceMap [ ID ];
                sequence.Restart(true);
                return;
            }
            sequence = DOTween.Sequence();
		

			foreach ( var m in _currentMotion.value  )
            {
                Tweener tweener = m.CreateTween < RectTransform > ( GetComponent < RectTransform > ());

                if ( tweener == null ) continue;

                if ( m.append == true ) sequence.Append(tweener);
                else sequence.Insert(m.sequenceIndex, tweener );
            }

            sequence.SetId(ID);
			sequence.SetAutoKill (false);
			sequence.SetRecyclable(true);
			sequence.OnComplete ( DoNextMotion );

            motionSequenceMap.Add(ID,sequence); 

            _currentSequence = sequence.Play();
        }

		public void DoNextMotion ()
		{
			if (_currentMotion == null || _currentMotion.nextSequenceIndex < 0 )
			{
				return;
			}

			DoPlayByID (sequences[_currentMotion.nextSequenceIndex].key);
		}
    }
}
