﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DG.Tweening;
using Rotorz.ReorderableList;

namespace PotionSoup.DoTweenPlugin
{
    [ CustomPropertyDrawer (typeof (MotionSequence),true)]
    public  class MotionSequenceDrawer : PropertyDrawer
    {
        public float propertyHeight = 0f;

        public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
        {
            var itemsProperty = property.FindPropertyRelative("value");
            return ReorderableListGUI.CalculateListFieldHeight(itemsProperty);
        }

        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
        {
            //EditorGUI.BeginProperty(position,label,property);

            position = EditorGUI.PrefixLabel(position, label);

            var itemsProperty = property.FindPropertyRelative("value");
            var sequenceKey = property.FindPropertyRelative("key");
            ReorderableListGUI.Title("[Sequence] ID: " + sequenceKey.stringValue);
            ReorderableListGUI.ListFieldAbsolute(position, itemsProperty);

            //EditorGUI.EndProperty();
        }
    }

    public class CommonDrawers 
    {
        public static void DrawMotionPresent ( MotionPresent present, Transform obj )
        {
            var buttonStyle = EditorGUIExtensions.GetFontStyle("button",11,TextAnchor.MiddleCenter,FontStyle.Normal);
            var toolbuttonStyle = EditorGUIExtensions.GetFontStyle("button",10,TextAnchor.MiddleCenter,FontStyle.Normal);

            EditorGUILayout.BeginHorizontal();

            if ( GUILayout.Button(present.append ? "Append"  : "Insert", GUILayout.ExpandWidth(true), GUILayout.Width(128f) ))
            {
                present.append = !present.append;
            }

            if ( present.append == false)
            {
				Color color = EditorColor.blue;
                EditorGUIExtensions.SetBackgroundColor(color);
                EditorGUILayout.IntField( present.sequenceIndex );
                EditorGUIExtensions.RestoreBackgroundColor();
            }
               

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            present.enabled = EditorGUILayout.Toggle(present.enabled,GUILayout.Width(16f));
            present.tweenType = (TweenType)EditorGUILayout.EnumPopup(present.tweenType);
            EditorGUIExtensions.SetBackgroundColor(EditorColor.greenyellow);
            if ( GUILayout.Button("<color=#D6FFB5FF>Copy</color>",buttonStyle))
            {
				TweenMotion.globalCopy = present;
            }

            EditorGUIExtensions.RestoreBackgroundColor();
			EditorGUIExtensions.SetBackgroundColor(EditorColor.bluelight);
            if ( GUILayout.Button("<color=#E5FFFFFF>Paste</color>",buttonStyle))
            {
				if (TweenMotion.globalCopy != null)
				{
					present = GenericExtensions.CopyComponent<MotionPresent>(TweenMotion.globalCopy, present);
				}
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUIExtensions.RestoreBackgroundColor();

            present.duration = EditorGUILayout.FloatField("Duration",present.duration);
            var buttonName = present.speedbased ? "<color=#D6FFB5FF>Speed Based</color>" : "<color=#B0B0B0FF>Speed Based</color>";
			var buttonColor = present.speedbased ? EditorColor.greenyellow  : Color.grey ;

            EditorGUIExtensions.SetBackgroundColor(buttonColor);

            if ( GUILayout.Button( buttonName ,toolbuttonStyle, GUILayout.Width(80f) ))
            {
                present.speedbased = !present.speedbased;
            }

            EditorGUIExtensions.RestoreBackgroundColor();

            EditorGUILayout.EndHorizontal();

            present.delay = EditorGUILayout.FloatField("Delay",present.delay);
            present.ignoreTimescale = EditorGUILayout.Toggle("Ignore Timescale", present.ignoreTimescale);
            present.ease = (Ease)EditorGUILayout.EnumPopup("Ease",present.ease);
            present.loops = EditorGUILayout.IntField("Loops",present.loops);
            if ( present.loops < 0 )
            {
                EditorGUI.indentLevel++;
                present.loopType = (LoopType) EditorGUILayout.EnumPopup("Loop Type", present.loopType);
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(false));
            buttonName = present.isFrom ? "FROM" : "TO";

            if ( GUILayout.Button( buttonName ,buttonStyle, GUILayout.Width(80f)))
            {
                present.isFrom = !present.isFrom;
            }

            switch ( present.tweenType )
            {
            case TweenType.LocalMove :
                present.localMoveValue = EditorGUILayout.Vector3Field("", present.localMoveValue); 

				EditorGUIExtensions.SetBackgroundColor(EditorColor.greenyellow);
                if (GUILayout.Button("<color=#D6FFB5FF>C</color>", buttonStyle, GUILayout.Width(20f)))
                {
                    present.localMoveValue = obj.localPosition;
                }
                EditorGUIExtensions.RestoreBackgroundColor();
                break;
            }

            EditorGUILayout.EndHorizontal();

            switch ( present.tweenType )
            {
            case TweenType.LocalMove:
                EditorGUI.indentLevel++;
                present.snapping = EditorGUILayout.Toggle("Snapping", present.snapping);
                present.relative = EditorGUILayout.Toggle("Relative", present.relative);
                EditorGUI.indentLevel--;
                break;
            }


        }
    }
}