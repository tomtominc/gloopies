﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.Events;
using System.Collections.Generic;

namespace PotionSoup.DoTweenPlugin
{
    public enum TweenType 
    {
        LocalMove
    }

    [System.Serializable]
    public class MotionSequence 
    {
        public string key;
		public List < MotionPresent > value = new List<MotionPresent  >();


		public int nextSequenceIndex = -1;

        public MotionSequence () {}
        public MotionSequence ( string key, MotionPresent value ) 
        {
            this.key = key;
            this.value.Add(value);
        }
    }

    [System.Serializable]
    public class MotionPresent
    {
        public MotionPresent ( TweenType tweenType )
        {
            this.tweenType = tweenType;
        }

        // hack so we can save edit mode
        public bool editMode = false;

        public TweenType tweenType = TweenType.LocalMove;

        public bool enabled = true;

        public bool append = false;
        public int sequenceIndex = 0;

        public float duration = 1.0f;
        public bool speedbased = false;

        public float delay = 1.0f;
        public bool ignoreTimescale = false;

        public Ease ease = Ease.OutQuad;

        public int loops = 1;
        public LoopType loopType = LoopType.Yoyo;

        public string ID = string.Empty;

        public bool isFrom = true;

        public Vector3 localMoveValue;

        public UnityEvent onCreated = new UnityEvent();
        public UnityEvent onStart = new UnityEvent();
        public UnityEvent onPlay = new UnityEvent();
        public UnityEvent onUpdate = new UnityEvent();
        public UnityEvent onStep = new UnityEvent();
        public UnityEvent onComplete = new UnityEvent();

        // Local Move Independent!!

        public bool snapping = false;
        public bool relative = false;

        public virtual Tweener CreateTween < U > (U component) where U : Component
        {
            if ( tweenType == TweenType.LocalMove )
            {
                RectTransform rect = component as RectTransform;
                return LocalMovePresent.CreateTween(rect,this);
            }

            return null;
        }

    }

    public class LocalMovePresent
    {
        public static Tweener CreateTween( RectTransform rectTransform, MotionPresent present)
        {
            if ( present.enabled == false ) return null;
            if ( rectTransform == null ) return null;

            Tweener tween = rectTransform.DOLocalMove(present.localMoveValue,present.duration,present.snapping);
            tween.SetAutoKill(false);
            tween.SetDelay(present.delay);
            tween.SetEase(present.ease);
			tween.SetLoops(present.loops < 0 ? 1000000 : present.loops,present.loopType);
            tween.SetId(present.ID);
            tween.SetRelative(present.relative);
            tween.SetSpeedBased(present.speedbased);
            tween.Pause();

            return tween;
        }
    }
}
