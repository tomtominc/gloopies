﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TopDown2DBounceEmitter : MonoBehaviour 
{
    public Vector2 radiusMinMax = new Vector2 ( 2f, 5f );
    public Vector2 spawnMinMax = new Vector2 ( 5f, 10f );
    public GameObject particle;

    public GameObject hitEffect;

    private List < TopDown2DParticle > particles = new List<TopDown2DParticle> ();

    public virtual void Emit ( Vector2 force , Transform target )
    {

        var hit = Instantiate < GameObject > ( hitEffect );

        hit.transform.SetParent ( target , false );
        hit.transform.localPosition = new Vector2 ( 0f, 1f );

        float limit = 1f;
        int particleCount = Random.Range ((int)spawnMinMax.x, (int)spawnMinMax.y );

        for (int i = 0; i < particleCount; i++ )
        {
            var go = Instantiate < GameObject > ( particle );

            go.transform.position = transform.position;
            TopDown2DParticle p = go.GetComponent < TopDown2DParticle > ();

            Vector2 dir = new Vector2 ( Random.Range ( force.x - limit , force.x  + limit ), Random.Range ( force.y - limit, force.y + limit ));

            p.Move ( dir.normalized );
            particles.Add ( p );
        }

        foreach ( var p in particles )
        {
            foreach ( var par in particles )
            {
                Physics2D.IgnoreCollision (p.particleCollider, par.particleCollider );
                Physics2D.IgnoreCollision (p.particleCollider, par.shadowCollider );
            }

            p.gameObject.SetActive ( true );
        }

        Destroy( gameObject, 2f );
    }
}
