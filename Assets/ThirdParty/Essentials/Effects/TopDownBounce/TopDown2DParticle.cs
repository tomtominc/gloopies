﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TopDown2DParticle : MonoBehaviour 
{
    public Transform particle;
    public Transform shadow;
    public LayerMask groundMask;

    public List < Sprite > particleSprites = new List<Sprite> ();

    public float speed = 1f;
    public float mod = 2f;

    public float lifeTime = 3f;

    public Vector3 dir;

    public Rigidbody2D body 
    {
        get { return particle.GetComponent < Rigidbody2D > (); }
    }

    public BoxCollider2D particleCollider
    {
        get { return particle.GetComponent < BoxCollider2D >  (); }
    }

    public BoxCollider2D shadowCollider
    {
        get { return shadow.GetComponent < BoxCollider2D > (); }
    }
    public SpriteRenderer particleRenderer
    {
        get { return particle.GetComponent < SpriteRenderer > (); }
    }
    public SpriteRenderer shadowRenderer
    {
        get { return shadow.GetComponent < SpriteRenderer > (); }
    }

    private bool fellOffGround = false;

    private void Start ()
    {
        particleRenderer.sprite = particleSprites [ Random.Range (0,particleSprites.Count) ];

        Vector2 S = particleRenderer.sprite.bounds.size;
        particleCollider.size = S;
        particleCollider.offset = new Vector2 ( 0f , - (S.x / 2));

        Physics2D.IgnoreCollision ( particleCollider, shadowCollider, false );
    }

    public void Move ( Vector2 dir )
    {
        this.dir = dir;
    }

    private void Update ()
    {
        if (particle.localPosition.y <= 0 && !fellOffGround ) particle.localPosition = new Vector3 (particle.localPosition.x, 0f , 0f);

        transform.Translate ( dir * (speed * Time.deltaTime) );

        if ( dir.y > 0 && fellOffGround )
        {
            particleRenderer.sortingOrder = 0;
        }
        else
        {
            particleRenderer.sortingOrder = (int) ( Mathf.Abs ( transform.position.y ) + 4f ); 
            shadowRenderer.sortingOrder = particleRenderer.sortingOrder - 1;
        }

        RaycastHit2D hit = Physics2D.Raycast ( shadow.position, Vector3.zero, Mathf.Infinity, groundMask );

        // check for intersecting ground
        if ( hit.transform )
        {
            shadow.localPosition = new Vector3 ( particle.localPosition.x, 0f, 0f);
            float scale = 1f / (0.8f + particle.localPosition.y);
            shadow.localScale = new Vector3 (scale,scale,1f); 
        }
        else
        {
            fellOffGround = true;
            shadow.gameObject.SetActive ( false );
        }



        lifeTime -= Time.deltaTime;
        speed -= Time.deltaTime * mod;

        if ( lifeTime <= 1f )
        {
            Blink ();

            if ( lifeTime < 0 )
            {
                Destroy ( gameObject );
            }
        }

    }


    public void Blink ()
    {
        particleRenderer.color = particleRenderer.color.a > 0 ? Color.clear : Color.white;
    }
}
