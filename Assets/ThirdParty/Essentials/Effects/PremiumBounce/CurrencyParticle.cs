﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using com.ootii.Messages;

public class CurrencyParticle : MonoBehaviour 
{
    public SpriteRenderer spriteRenderer
    {
        get { return GetComponent < SpriteRenderer > (); }
    }

    public Vector3 scale 
    {
        get { return transform.localScale; }
    }

    public Vector3 punchForce = new Vector3 (0f,1f,0f);
    public float punchScaleDuration = 0.3f;

    public Vector2 moveDurationBounds = new Vector2 ( 0.8f, 1.2f );
    public Vector2 moveDelayBounds = new Vector2 ( 0.6f, 1f );

    public float scatterMoveDuration = 0.3f;
    public Vector2 scatterRadiusBounds =  new Vector2 (0.1f, 3f );

    public GameObject hitEffect;

    public void DoEffect ( Vector3 target )
    {
        StartCoroutine ( DoEffectRoutine ( target ) );
    }

    public IEnumerator DoEffectRoutine ( Vector3 target )
    {
        transform.DOMove ( UnityMathExtensions.PointOnCircle ( Random.Range(scatterRadiusBounds.x,scatterRadiusBounds.y) 
            , Random.Range (1f,360f), transform.position ) , scatterMoveDuration ).Play ();

        yield return new WaitForSeconds (scatterMoveDuration);

        transform.DOPunchScale ( punchForce,punchScaleDuration ).Play ();

        target.z = 0;

        transform.DOMove ( target, Random.Range ( moveDurationBounds.x, moveDurationBounds.y ) )
            .SetDelay ( Random.Range ( moveDelayBounds.x, moveDelayBounds.y ) ).OnComplete ( OnComplete ).Play ();

    }

    public void OnComplete ()
    {
        GameManager.Instance.inventoryManager.CoinParticle_OnComplete ();

        SoundManager.Instance.PlaySoundFX ( "Collect_Coin" );

        spriteRenderer.color = Color.clear;

        var go = Instantiate < GameObject > ( hitEffect );

        go.transform.position = UnityMathExtensions.PointOnCircle ( Random.Range (0.1f,1f),
            Random.Range (1f, 360f), transform.position );

        Destroy ( gameObject );

        Destroy ( go, 2f );
    }
}
