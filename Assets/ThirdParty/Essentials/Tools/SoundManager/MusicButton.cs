﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MusicButton : DynamicButton 
{
    public DynamicToggle[] toggles;

    protected override void Start()
    {
        base.Start();

        toggles = GetComponentsInChildren < DynamicToggle > ();

        RefreshSprite ();
    }

    public virtual void RefreshSprite ()
    {
        for ( int i = 0; i < toggles.Length; i++ )
            toggles[i].IsOn = SoundManager.Instance.MusicEnabled;
    }

    public virtual void ToggleMusic ()
    {
        SoundManager.Instance.ToggleMusic ();

        RefreshSprite ();
    }
}
