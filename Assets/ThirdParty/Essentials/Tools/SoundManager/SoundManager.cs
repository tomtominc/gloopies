﻿using UnityEngine;
using System.Collections;
using PotionSoup.Engine2D;
using PotionSoup.Persistent;

public class SoundManager : Singleton < SoundManager > 
{
    public SoundBank soundBank;

    private const string kMusicIsOn = "MusicIsOn";
    private const string kSoundIsOn = "SoundIsOn";

    public enum Track 
    {
        One , Two
    }


    public AudioSource track1
    {
        get { return GetComponent < AudioSource > (); }   
    }

    public AudioSource track2
    {
        get { return transform.GetChild (0).GetComponent < AudioSource > (); }
    }

    private float MusicVolume 
    {
        set 
        {
            track1.volume = value;
            soundBank.MusicVolume = value;
            track2.volume = value;
        }
    }

    private float SoundVolume
    {
        set 
        {
            soundBank.SoundVolume = value;
        }
    }

    public bool MusicEnabled 
    {
        get 
        {
            return  PersistentSettings.GetValue ( kMusicIsOn, true );    
        }
        set 
        {
            track1.mute = !value;
            track2.mute = !value;
            PersistentSettings.SetValue ( kMusicIsOn, value );
        }
    }

    public bool SoundEnabled 
    {
        get 
        {
            return  PersistentSettings.GetValue ( kSoundIsOn, true );    
        }

        set 
        {
            PersistentSettings.SetValue ( kSoundIsOn, value );
        }
    }

    private bool _internalMusicEnabled = true;

    public bool InternalMusicEnabled
    {
        get
        {
            return MusicEnabled;
        }
        set 
        {
            if ( MusicEnabled == false ) return;

            _internalMusicEnabled = value;

            track1.mute = !value;
            track2.mute = !value;
        }
    }

    private bool _internalSoundEnabled = true;
    public bool InternalSoundEnabled
    {
        get
        {
            return SoundEnabled;
        }
        set 
        {
            if ( SoundEnabled == false ) return;

            _internalSoundEnabled = value;
        }
    }

    private void Awake ()
    {
        Instance.soundBank.Load ();

        track1.mute = !MusicEnabled;
        track2.mute = !MusicEnabled;

        track1.volume = soundBank.MusicVolume;
        track2.volume = soundBank.MusicVolume;
    }

    public bool CanPlaySound ()
    {
        if ( Instance == null ) return false;
        if ( Instance.soundBank == null ) return false;

        if ( Instance.soundBank.runtimeMusic == null 
            || Instance.soundBank.runtimeSoundFX == null 
            || Instance.soundBank.runtimeGroups == null )
            Instance.soundBank.Load ();

        if ( _internalSoundEnabled == false ) return false;
        if ( Instance.SoundEnabled == false ) return false;

        return true;
            
    }

    public bool CanPlayMusic ()
    {
        if ( Instance == null ) return false;
        if ( Instance.soundBank == null ) return false;

        if ( Instance.soundBank.runtimeMusic == null 
            || Instance.soundBank.runtimeSoundFX == null 
            || Instance.soundBank.runtimeGroups == null )
            Instance.soundBank.Load ();

        if ( _internalMusicEnabled == false ) return false;
        
        if ( Instance.MusicEnabled == false ) return false;

        return true;

    }

    public void ToggleMusic ()
    {
        MusicEnabled = !MusicEnabled;
    }

    public void ToggleSound ()
    {
        SoundEnabled = !SoundEnabled;
    }

    public void PlaySoundFX ( string key )
    {
        if ( CanPlaySound () ==  false ) return;

        Sound sound;

        if ( Instance.soundBank.runtimeSoundFX.TryGetValue ( key, out sound ) )
        {
            if ( sound.clip == null ) return;

            AudioSource.PlayClipAtPoint (sound.clip,Camera.main.transform.position, sound.volume * Instance.soundBank.SoundVolume );
        }
    }

    public void PlaySoundFX ( string key, float delay )
    {
        if ( CanPlaySound () ==  false ) return;

        Sound sound;

        if ( Instance.soundBank.runtimeSoundFX.TryGetValue ( key, out sound ) )
        {
            if ( sound.clip == null ) return;

            Timer timer = new Timer ( delay,  () => 
                {
                    AudioSource.PlayClipAtPoint (sound.clip,Camera.main.transform.position, 
                        sound.volume * Instance.soundBank.SoundVolume );
                });

            timer.Start ( Instance );
        }
    }

    public void PlayMusic ( string key , float delay = 0f )
    {
        PlayMusic ( key, track1, delay );
    }

    public void PlayMusic ( string key , Track track, float delay = 0f )
    {
        if ( track == Track.One ) PlayMusic ( key, track1, delay );
        else if ( track == Track.Two ) PlayMusic ( key, track2, delay );
    }

    public void PlayMusic ( string key , AudioSource track, float delay = 0f )
    {
        if ( CanPlayMusic () ==  false ) return;

        Sound sound;

        if ( Instance.soundBank.runtimeMusic.TryGetValue ( key, out sound ) )
        {
            if ( sound.clip == null ) return;

            Timer timer = new Timer ( delay, () =>
                {
                    track.clip = sound.clip;
                    track.volume = Instance.soundBank.MusicVolume * sound.volume;
                    track.Play ();
                });



            timer.Start ( Instance );
        }
    }

    public void PlayRandomSoundFX ( string groupName, float delay = 0f )
    {
        if ( CanPlaySound () == false ) return;

        SoundGroup soundGroup;

        if ( Instance.soundBank.runtimeGroups.TryGetValue ( groupName, out soundGroup ))
        {
            if ( soundGroup.sounds.Count > 0 )
            {
                Sound sound = soundGroup.sounds [ Random.Range ( 0, soundGroup.sounds.Count ) ];

                if ( sound.clip == null ) return;


                Timer timer = new Timer ( delay,  () => 
                    {
                        AudioSource.PlayClipAtPoint (sound.clip,Camera.main.transform.position, 
                            sound.volume * Instance.soundBank.SoundVolume );
                    });

                timer.Start ( Instance );

            }
        }
        else
        {
            Debug.LogWarningFormat ( "Could not find sound fx by the name of {0}", groupName );    
        }
    }
}
