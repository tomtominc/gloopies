﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundButton : DynamicButton 
{
    public DynamicToggle[] toggles;

    protected override void Start()
    {
        base.Start();

        toggles = GetComponentsInChildren < DynamicToggle > ();

        RefreshSprite ();
    }

    public virtual void RefreshSprite ()
    {
        for ( int i = 0; i < toggles.Length; i++ )
            toggles[i].IsOn = SoundManager.Instance.SoundEnabled;
    }

    public virtual void ToggleSound ()
    {
        SoundManager.Instance.ToggleSound ();

        RefreshSprite ();
    }
}
