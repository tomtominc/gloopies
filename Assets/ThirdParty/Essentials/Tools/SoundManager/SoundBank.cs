﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PotionSoup.Persistent;

public enum AudioType
{
    Music, Sound
}

public class SoundBank : ScriptableObject 
{
    
    [SerializeField]
    public List < Sound > serializedMusic   = new List<Sound> ();

    [SerializeField]
    public List < Sound > serializedSoundFX = new List<Sound> ();

    [SerializeField]
    public List < SoundGroup > serializedGroups = new List<SoundGroup> ();

    public Dictionary < string , Sound  > runtimeMusic;
    public Dictionary < string , Sound  > runtimeSoundFX;
    public Dictionary < string , SoundGroup > runtimeGroups;

    private float _musicVolume = 0.1f;
    private float _soundVolume = 0.1f;

    public float MusicVolume
    {
        get { return _musicVolume; }
        set { _musicVolume = value; }
    }

    public float SoundVolume
    {
        get { return _soundVolume; }
        set { _soundVolume = value; }
    }


    public void Load ()
    {
        runtimeMusic    = new Dictionary < string, Sound > ();
        runtimeSoundFX  = new Dictionary < string, Sound > ();
        runtimeGroups   = new Dictionary < string, SoundGroup > ();

        if ( serializedMusic != null || serializedMusic.Count > 0 )
            runtimeMusic.AddRange ( serializedMusic.ToDictionary ( x => x.name, x => x ) );

        if ( serializedSoundFX != null || serializedSoundFX.Count > 0 )
            runtimeSoundFX.AddRange ( serializedSoundFX.ToDictionary ( x => x.name, x => x ) );

        if ( serializedGroups != null || serializedGroups.Count > 0 )
            runtimeGroups.AddRange ( serializedGroups.ToDictionary ( x => x.name, x => x ) );
    }
}
