﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Rotorz.ReorderableList;

[CustomEditor(typeof(SoundGroup))]
public class SoundGroupDrawer : Editor 
{
    private SoundGroup _soundGroup;
    private SerializedProperty _sounds;

    private void OnEnable ()
    {
        _soundGroup = target as SoundGroup;  
        _sounds = serializedObject.FindProperty ("sounds");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update ();

        EditorGUIExtensions.DrawHeader ( "Settings" );

        EditorGUIExtensions.BeginContent ( Color.white );
        {
            _soundGroup.groupName = EditorGUILayout.TextField ( "Name", _soundGroup.groupName );   
        }
        EditorGUIExtensions.EndContent ();


        ReorderableListGUI.Title ( "Sounds" );

        ReorderableListGUI.ListField ( _sounds );

        serializedObject.ApplyModifiedProperties ();

    }

    [MenuItem ("Assets/Create/Sound Group")]
    public static void CreateAsset ()
    {
        ScriptableObjectUtility.CreateAsset < SoundGroup > ();
    }
}
