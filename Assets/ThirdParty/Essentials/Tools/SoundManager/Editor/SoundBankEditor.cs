﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Rotorz.ReorderableList;

[CustomEditor (typeof(SoundBank))]
public class SoundBankEditor : Editor 
{
    private SoundBank _soundBank;

    private SerializedProperty _serializedMusic;
    private SerializedProperty _serializedSoundFX;
    private SerializedProperty _serializedGroups;

    private void OnEnable ()
    {
        _soundBank = target as SoundBank;

        _serializedMusic   = serializedObject.FindProperty ("serializedMusic");
        _serializedSoundFX = serializedObject.FindProperty ("serializedSoundFX");
        _serializedGroups = serializedObject.FindProperty ("serializedGroups");

    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update ();

        EditorGUIExtensions.DrawHeader ( "Settings" );

        EditorGUIExtensions.BeginContent ( Color.white );
        {
            _soundBank.MusicVolume = EditorGUILayout.Slider ( "Music Volume",    _soundBank.MusicVolume, 0f, 1f );
            _soundBank.SoundVolume = EditorGUILayout.Slider ( "Sound FX Volume", _soundBank.SoundVolume, 0f, 1f );
        }
        EditorGUIExtensions.EndContent ();

        ReorderableListGUI.Title ("Music");
        ReorderableListGUI.ListField ( _serializedMusic );

        ReorderableListGUI.Title ("Sound FX");
        ReorderableListGUI.ListField ( _serializedSoundFX );

        ReorderableListGUI.Title ("Sound Groups");
        ReorderableListGUI.ListField ( _serializedGroups );

        serializedObject.ApplyModifiedProperties ();
    }

    [MenuItem("Assets/Create/Sound Bank")]
    public static void CreateAsset ()
    {
        ScriptableObjectUtility.CreateAsset<SoundBank> ();
    }
}
