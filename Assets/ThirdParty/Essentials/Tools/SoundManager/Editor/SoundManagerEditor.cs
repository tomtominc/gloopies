﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(SoundManager))]
public class SoundManagerEditor : Editor 
{
    private SoundManager _soundManager;

    private void OnEnable ()
    {
        _soundManager = target as SoundManager;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update ();

        EditorGUIExtensions.DrawHeader ( "Settings" );

        EditorGUIExtensions.BeginContent ( Color.white );
        {

            EditorGUILayout.PropertyField (serializedObject.FindProperty ("soundBank") );

            if ( _soundManager.soundBank == null )
            {
                EditorGUILayout.HelpBox ("Create a SoundBank and input it into the soundbank field to get started.", MessageType.Info );

                return;
            }

            _soundManager.MusicEnabled = EditorGUILayout.Toggle ( "Music Enabled", _soundManager.MusicEnabled );
            _soundManager.SoundEnabled = EditorGUILayout.Toggle ( "Sound Enabled", _soundManager.SoundEnabled );

            float musicVolume = _soundManager.soundBank.MusicVolume;
            float soundVolume = _soundManager.soundBank.SoundVolume;

            musicVolume = EditorGUILayout.Slider ("Music Volume", musicVolume, 0f, 1f );
            soundVolume = EditorGUILayout.Slider ("Sound FX Volume", soundVolume, 0f, 1f );

            _soundManager.soundBank.MusicVolume = musicVolume;
            _soundManager.soundBank.SoundVolume = soundVolume;

        }
        EditorGUIExtensions.EndContent ();

        serializedObject.ApplyModifiedProperties ();
    }
}
