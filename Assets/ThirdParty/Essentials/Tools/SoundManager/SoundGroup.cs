﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SoundGroup : ScriptableObject
{
    [SerializeField]
    public string groupName;

    [SerializeField]
    public List < Sound > sounds = new List<Sound> ();
}
