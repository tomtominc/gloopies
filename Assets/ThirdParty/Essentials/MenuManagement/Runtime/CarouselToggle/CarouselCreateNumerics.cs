﻿using UnityEngine;
using System.Collections;
using System;

public class CarouselCreateNumerics : MonoBehaviour 
{
    public GameObject numericPrefab;

    public int startNumeric = 1;
    public int endNumeric   = 99;

    private void Awake ()
    {
        if ( numericPrefab == null ) return;

        for ( int i = startNumeric; i < endNumeric + 1; i++ )
        {
            GameObject clone = Instantiate < GameObject > ( numericPrefab );
            clone.transform.SetParent ( transform, false );
            clone.name = i.ToString ();
            TextField field = clone.GetComponent < TextField > ();
            field.Text = i.Pretty ();
        }
    }
}
