﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using PotionSoup.Engine2D;

namespace PotionSoup.MenuManagement
{
	public class MenuInterface : MonoBehaviour
	{      
		private List < TweenAnimator > _tweens = new List<TweenAnimator> ();
		private List < DOTweenAnimation > _doTweens = new List< DOTweenAnimation > ();

		public bool tweensEnabled = true;

		public virtual void Initialize(object data = null) 
		{
			if (tweensEnabled)
			{
				_tweens = GetComponentsInChildren<TweenAnimator>(true).ToList ();
				_doTweens = GetComponentsInChildren < DOTweenAnimation > (true).ToList ();

				foreach (var t in _tweens)
	            {
	                t.Initialize();
	            }
			}
		}

        public virtual void Refresh ( object data = null )
        {

        }

		public virtual void Reset (bool active)
        {
			foreach (var tween in _tweens)
			{
				tween.ResetAnimator();
			}

            gameObject.SetActive(active);
        }

		public virtual float GetEnterDuration ()
		{
			var durations = new List<float> ();

			foreach ( var tween in _tweens )
			{
				durations.Add ( tween.GetEnterDuration());
			}

			return Mathf.Max ( durations.ToArray () );
		}

		public virtual float Enter ()
        {
			Reset(true);

			float maxDuration = 0f;

            foreach (var animator in _tweens)
            {
                float duration = animator.DoInTween();

				if (duration > maxDuration)
				{
					maxDuration = duration;
				}
            }

			foreach (var tween in _doTweens)
			{
				tween.DOPlayAllById ("Enter");

				float duration = tween.duration;

				if (duration > maxDuration)
				{
					maxDuration = duration;
				}
			}

			return maxDuration;
        }

		public virtual float Exit ()
        {
			float maxDuration = 0f;

            foreach (var animator in _tweens)
            {
                float duration = animator.DoOutTween();

				if (duration > maxDuration)
				{
					maxDuration = duration;
				}
            }

			foreach (var tween in _doTweens)
			{
				tween.DOPlayAllById ("Exit");
				float duration = tween.duration;

				if (duration > maxDuration)
				{
					maxDuration = duration;
				}
			}

			if (!gameObject.IsActive()) gameObject.SetActive(true);

			StartCoroutine (Exit (maxDuration));

			return maxDuration;
        }

		protected IEnumerator Exit (float duration)
		{
			yield return new WaitForSeconds (duration);
			Reset (false);
		}
	}
}
