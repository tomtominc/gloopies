﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using PotionSoup.Engine2D;

namespace PotionSoup.MenuManagement
{
    public enum NavigationOption
    {
        Disabled, Initialization, Focused, FixedTime
    }

   
    [RequireComponent(typeof(CanvasGroup))]
	public class MenuController : MonoBehaviour 
	{
        public bool keepOnWhenLoading = false;
		public SplashScreen splashScreen;
        public NavigationOption NavigationOption;
		public Selectable initiallyFocusedSelectable;
        public float defaultNavigationEnableDelay = 0.1f;

        public MenuController backMenu;

		protected bool _initialized = false;
		protected MenuInterface _menuInterface;
        protected CanvasGroup _canvasGroup;

        public virtual void Initialize (object data = null)
		{
            if (_initialized == true )
            {
                Refresh ( data );
                return;
            }

            _menuInterface = GetComponent<MenuInterface>();
            if (!_menuInterface) Debug.LogError ("No Menu Interface Found!");

            _canvasGroup = GetComponent<CanvasGroup>();
            if (!_canvasGroup) Debug.LogError ("No Canvas group found on View controller");


            _initialized = true;
            _menuInterface.Initialize( data );

            Refresh ( data );
		}

		public virtual bool HasInitialized ()
		{
			return _initialized;
		}

		public virtual float GetEnterDuration ()
		{
			if (_menuInterface == null)
			{
				Initialize ( null );
			}

			return _menuInterface.GetEnterDuration();
		}

        public virtual void Refresh (object data = null )
        {
			_menuInterface.Refresh ( data );
        }

        public virtual void SetInteractability (bool isInteractable)
        {
            if ( _canvasGroup == null )
            {
                _canvasGroup = GetComponent < CanvasGroup > ();
            }

            _canvasGroup.interactable = isInteractable;
        }

        public virtual float Enter ()
		{
			_canvasGroup.interactable = true;

			float enterTime = _menuInterface.Enter();

			if (splashScreen)
				splashScreen.DoStartSplashEffect ();
			
			return enterTime;
		}

        public virtual float Exit ()
        {
			_canvasGroup.interactable = false;

            return _menuInterface.Exit();
        }

		public virtual void DisableControls ()
		{
			_canvasGroup.interactable = false;
		}

		public virtual void EnableControls ()
		{
			_canvasGroup.interactable = true;
		}

        public virtual void Reset ( bool control )
        {
            _menuInterface.Reset ( control );
        }

		public virtual Selectable[] GetSelectableComponents ()
		{
			return  GetComponentsInChildren < Selectable > (true);
		}

        public virtual void SetLastSelection ( Selectable selectable )
        {
            initiallyFocusedSelectable = selectable;
        }

        public virtual bool ContainsSelectable ( Selectable selection )
        {
			List<Selectable> selectables = GetSelectableComponents().ToList();
            return selectables.Contains ( selection );
        }

        public virtual Transition TryFindTransition ( MenuController controller )
        {
            Transition[] transitions = GetComponentsInChildren < Transition > (true);

            if ( transitions.Length < 1 ) return null;

            Transition transition = transitions.FirstOrDefault( x => x.nextMenu == controller );

            return transition;
        }

        public virtual void DoUpdate ()
        {
            // nothing goes into the update, override for frame control
        }

        public virtual float GetNavigationEnableDelay ( float defaultDelay )
        {
            if ( NavigationOption == NavigationOption.FixedTime )
            {
                return defaultNavigationEnableDelay;
            }

            if ( NavigationOption == NavigationOption.Initialization )
            {
                return 0f;
            }

            return defaultDelay;
        }

       
        public void Update ()
        {
            DoUpdate();
        }
	}
}
