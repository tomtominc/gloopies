﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using PotionSoup.Engine2D;
using System.Linq;

namespace PotionSoup.MenuManagement
{
    public class MenuManager : Singleton < MenuManager >
    {
        public MenuController initialView;
        protected MenuController _currentMenu;
        protected MenuController _previousMenu;
        protected CanvasGroup _group
        {
            get { return GetComponent < CanvasGroup > (); }
        }

        protected Dictionary < string , MenuController > Menus = new Dictionary<string, MenuController> ();

        private void Start()
        {
            var menus = GetComponentsInChildren < MenuController > ( true ).ToList ();

            foreach ( var menu in menus )
            {
                if ( Menus.ContainsKey( menu.name ) )
                {
                    Debug.LogError("Menu with the same name exists, please change name of " + menu.name );
                    return;
                }

                menu.gameObject.SetActive ( menu.keepOnWhenLoading );
                Menus.Add(menu.name, menu); 
            }

            if (initialView != null)
            {
                OpenMenu(null, initialView, null,null);
            }
        }

        public void SetMenuAllMenusActive ( bool control )
        {
            foreach ( var pair in Menus )
            {
                //if ( pair.Value.keepOnWhenLoading ) continue;
                pair.Value.gameObject.SetActive ( control );
            }
        }

		public float OpenPopup ( string popup , object data )
        {
            if ( ! Menus.ContainsKey(popup) ) return 0f;

            MenuController controller = Menus[ popup ];

			if ( controller == null ) return 0f;

            return OpenPopup(controller, data);
          
        }

		public float OpenPopup ( MenuController controller , object data )
        {
            if ( controller == null ) return 0f;

            return OpenMenu(null, controller, null, data ); 
        }


        public float OpenMenu ( Transition transition , object data  )
        {
            if (transition == null || transition.ExitBehaviour == ExitBehaviour.Disabled)
                return 0f;
            
            if (transition.ExitBehaviour == ExitBehaviour.ExitOnly)
            {
                return CloseMenu(_currentMenu);
            }
            
			return OpenMenu(transition, transition.nextMenu, _currentMenu, data);
        }

		public float OpenMenu(Transition transition, MenuController enterMenu, MenuController exitMenu, object data )
        {
            if (enterMenu == null) return 0f;

            enterMenu.Initialize ( data );

            float exitDuration = transition == null ? (exitMenu == null) ? 0f : exitMenu.Exit() : 
                (exitMenu == null || transition.ExitBehaviour == ExitBehaviour.OpenNextMenu) ? 0f : exitMenu.Exit();
            
            float enterDelay = transition == null ? 0f : transition.GetEnterDelay(exitDuration);

            _previousMenu = _currentMenu;
            _currentMenu = enterMenu;

			float enterDuration = 0f;

            Timer enterTransitionTimer = new Timer(enterDelay, () =>
                {
                    enterDuration = enterMenu.Enter();

                    if (enterMenu.NavigationOption == NavigationOption.Disabled)
                        return;

                });

            enterTransitionTimer.Start(this);

			return Mathf.Max (exitDuration, enterDuration);

        }

        public float OpenMenu ( string menu, float enterDelay )
        {
            MenuController enterMenu;
            MenuController exitMenu = _currentMenu;

           

            if ( Menus.TryGetValue ( menu, out enterMenu ) == false ) return 0f;

            enterMenu.Initialize ( null );

            _previousMenu = _currentMenu;
            _currentMenu = enterMenu;

            if ( _previousMenu == _currentMenu ) return 0f;

            float exitDuration = (exitMenu == null) ? 0f : exitMenu.Exit();

            float enterDuration = 0f;

            Timer enterTransitionTimer = new Timer(enterDelay, () =>
                {
                    enterDuration = enterMenu.Enter();

                    if (enterMenu.NavigationOption == NavigationOption.Disabled)
                        return;

                });

            enterTransitionTimer.Start(this);

            return Mathf.Max (exitDuration, enterDuration);
        }

		public float OpenPreviousMenu ()
        {
            if ( _currentMenu == null ) return 0f;

            MenuController backMenu = _currentMenu.backMenu;

            if ( backMenu == null ) return 0f;

            Transition transition = _currentMenu.TryFindTransition(backMenu);

            if ( transition == null )
            {
                return CloseMenu(_currentMenu);
            }
            
			return OpenMenu(transition, backMenu, _currentMenu, null );

        }

        public MenuController GetMenu ( string name )
        {
            if ( Menus.ContainsKey ( name ) )
            {
                return Menus [ name ];
            }


            Debug.LogWarning ( "Trying to get a menu that does not exist" );

            return null;
        }

        public float CloseMenu ( MenuController menu )
        {
            if (menu == null)
                return 0f;

			if (_previousMenu == null )
			{
				return _currentMenu.Exit ();
			}

            float navigationEnabledDelay = _previousMenu.GetNavigationEnableDelay(_currentMenu.Exit()); 

            _currentMenu = _previousMenu;

            _previousMenu = null;

            if (_currentMenu.NavigationOption == NavigationOption.Disabled)
			{
				return navigationEnabledDelay;
			}

			return navigationEnabledDelay;

        }

		public void DisableControls ()
		{
            if ( _group == null ) return;

            _group.interactable = false;
            _group.blocksRaycasts = false;
		}

		public void EnableControls ()
		{
            if ( _group == null ) return;

            _group.interactable = true;
            _group.blocksRaycasts = true;
		}

    }
}
