﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasHelper : MonoBehaviour 
{
    public float resolutionScale = 0.5f;

    //private Canvas _canvas;
    private CanvasScaler _canvasScaler;

    public Vector2 screenSize = new Vector2 (0,0);

    public bool useRealSize = true;
    public bool debug = false;

    public Vector2 Resolution
    {
        get 
        {
            
            return  new Vector2 ( screenSize.x * resolutionScale, screenSize.y * resolutionScale ); 
        }
    }

    private void Awake ()
    {
        
        //_canvas = GetComponent < Canvas > ();
        _canvasScaler = GetComponent < CanvasScaler > ();
    }

    private void Start ()
    {
        RefreshResolution ();
    }

    public void RefreshResolution ()
    {
        switch ( GameManager.Instance.deviceType )
        {
            case GameManager.DeviceType.iPhone4 :

                screenSize = new Vector3 (640,960);

                break;

            case GameManager.DeviceType.iPhone5 :

                screenSize = new Vector3 (640,1136);
                break;

            case GameManager.DeviceType.iPad :

                screenSize = new Vector3 (768,1024);
                break;

            default:  screenSize = new Vector3 (640,960);
                break;
        }

        _canvasScaler.referenceResolution = Resolution;//new Vector2 ( 180f, 800f );
    }

    private void Update ()
    {
        RefreshResolution ();
    }

    public void OnGUI ()
    {
        if (debug)
        GUI.TextField ( new Rect (Screen.width * 0.5f, Screen.height * 0.95f, Screen.width, Screen.height ), string.Format ("Screen Width: {0} Screen Height: {1}", Screen.width, Screen.height) );
    }

}
