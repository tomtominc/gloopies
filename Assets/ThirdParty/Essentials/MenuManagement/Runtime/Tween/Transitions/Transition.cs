﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

namespace PotionSoup.MenuManagement
{
    public enum NextMenuEnterBehaviour
    {
        Immediately, AfterCloseFinish,  FixedTime
    }

    public enum ExitBehaviour
    {
        Disabled, ExitOnly, ExitAndOpenNextMenu, OpenNextMenu
    }

	public class Transition : MonoBehaviour 
	{
        public ExitBehaviour ExitBehaviour = ExitBehaviour.Disabled;

        public NextMenuEnterBehaviour NextMenuEnterBehaviour = NextMenuEnterBehaviour.Immediately;
        public MenuController nextMenu;
        public float nextMenuDelay;

        protected Button _button;

		private void Start ()
		{
			_button = GetComponent<Button>();
			_button.onClick.AddListener ( StartTransiton );
		}

        public virtual void StartTransiton ()
        {
            MenuManager.Instance.OpenMenu ( this , null );
		}

        public virtual float GetEnterDelay ( float defaultDelay )
        {

            if ( NextMenuEnterBehaviour == NextMenuEnterBehaviour.Immediately )
            {
                return 0f;
            }

            if ( NextMenuEnterBehaviour == NextMenuEnterBehaviour.FixedTime )
            {
                return nextMenuDelay;
            }

            return defaultDelay;
        }

	}
}
