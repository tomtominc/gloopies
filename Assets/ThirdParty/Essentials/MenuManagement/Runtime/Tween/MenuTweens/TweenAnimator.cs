﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.Events;

namespace PotionSoup.MenuManagement
{
	public class TweenAnimator : MonoBehaviour 
	{
		[HideInInspector]
		public TweenData MoveIn;

		[HideInInspector]
		public TweenData RotateIn;

		[HideInInspector]
		public TweenData ScaleIn;

		[HideInInspector]
		public TweenData MoveIdle;

		[HideInInspector]
		public TweenData RotateIdle;

		[HideInInspector]
		public TweenData ScaleIdle;

		[HideInInspector]
		public TweenData MoveOut;

		[HideInInspector]
		public TweenData RotateOut;

		[HideInInspector]
		public TweenData ScaleOut;

		private RectTransform _rectTransform;

		private Vector3 _originalPosition;
		private Vector3 _originalScale;
		private Vector3 _originalRotation;

		private Sequence _enterSequence;
		private Sequence _exitSequence;

        private bool isEnterComplete = false;
        private bool isExitComplete = false;

		public void Initialize ()
		{
			_rectTransform = GetComponent<RectTransform>();
			_originalPosition = _rectTransform.localPosition;
			_originalScale = _rectTransform.localScale;
			_originalRotation = _rectTransform.localEulerAngles;
		}

		public void ResetAnimator()
		{
			_rectTransform.localPosition = _originalPosition;
			_rectTransform.localScale = _originalScale;
			_rectTransform.localEulerAngles = _originalRotation;

			if (_enterSequence != null)
			{
				_enterSequence.Kill (true);
				_enterSequence = null;
			}

			if (_exitSequence  != null)
			{
				_exitSequence.Kill (true);
				_exitSequence = null;
			}
		}

		public void EnterCompleted()
		{
			isEnterComplete = true;
		}

		public bool IsEnterComplete()
		{
			return isEnterComplete;
		}

		public void ExitCompleted()
		{
			isExitComplete = true;
		}

		public bool IsExitComplete ()
		{
			return isExitComplete;
		}

		public void PlaySound ( AudioClip clip )
		{
			if ( clip == null )
			{
				return;
			}

			AudioSource.PlayClipAtPoint ( clip , Vector3.zero , 1f );
		}

		public float GetEnterDuration ()
		{
			float moveInDuration = 0f;
			float rotateInDuration = 0f;
			float scaleInDuration = 0f;

			if (MoveIn.isOn)
				moveInDuration = MoveIn.duration + MoveIn.delay;
			if (RotateIn.isOn)
				rotateInDuration = RotateIn.duration + RotateIn.delay;
			if (ScaleIn.isOn)
				scaleInDuration = ScaleIn.duration + ScaleIn.delay;

			return Mathf.Max (moveInDuration, rotateInDuration, scaleInDuration);
		}

		public float DoInTween ()
		{
		    _enterSequence = DOTween.Sequence();

			if ( MoveIn.isOn )
			{
				_enterSequence.Append ( _rectTransform.DOLocalMove ( MoveIn.from , MoveIn.duration , false ).
					SetEase ( MoveIn.ease ).
						SetLoops ( MoveIn.loops ).
						SetDelay ( MoveIn.delay ).From () );

                _enterSequence.OnStart(() => PlaySound(MoveIn.beginSound));
                _enterSequence.OnComplete(() => { PlaySound(MoveIn.endSound); EnterCompleted(); });

			}

			if ( RotateIn.isOn )
			{
				_enterSequence.Insert (0f, _rectTransform.DOLocalRotate ( RotateIn.from , RotateIn.duration , RotateIn.rotationMode ).
					SetEase ( RotateIn.ease ).
						SetLoops ( RotateIn.loops ).
						SetDelay ( RotateIn.delay ).From() );

				_enterSequence.OnStart (()=> PlaySound ( RotateIn.beginSound ) );
                _enterSequence.OnComplete(() => { PlaySound(RotateIn.endSound); EnterCompleted(); });
			}

			if ( ScaleIn.isOn )
			{
				_enterSequence.Insert (0f , _rectTransform.DOScale ( ScaleIn.from , ScaleIn.duration ).
				                 SetEase ( ScaleIn.ease ).
				                 SetLoops ( ScaleIn.loops ).
				                 SetDelay ( ScaleIn.delay ).From() );

				_enterSequence.OnStart (()=> PlaySound ( ScaleIn.beginSound ) );
                _enterSequence.OnComplete(() => { PlaySound(ScaleIn.endSound); EnterCompleted(); });
			}

			_enterSequence.Play();

			return _enterSequence.Duration();

		}

		public float DoOutTween ()
		{
			_exitSequence = DOTween.Sequence();
			
			if ( MoveOut.isOn )
			{
				_exitSequence.Append ( _rectTransform.DOLocalMove ( MoveOut.from , MoveOut.duration , false ).
				                 SetEase ( MoveOut.ease ).
				                 SetLoops ( MoveOut.loops ).
				                 SetDelay ( MoveOut.delay ));
				
				_exitSequence.OnStart (()=> PlaySound ( MoveOut.beginSound ) );
                _exitSequence.OnComplete(() => { PlaySound(MoveOut.endSound); ExitCompleted(); });
			}
			
			if ( RotateOut.isOn )
			{
				_exitSequence.Insert (0f, _rectTransform.DOLocalRotate ( RotateOut.from , RotateOut.duration , RotateOut.rotationMode ).
				                 SetEase ( RotateOut.ease ).
				                 SetLoops ( RotateOut.loops ).
				                 SetDelay ( RotateOut.delay ).From() );
				
				_exitSequence.OnStart (()=> PlaySound ( RotateOut.beginSound ) );
                _exitSequence.OnComplete(() => { PlaySound(RotateOut.endSound); ExitCompleted(); });
			}
			
			if ( ScaleOut.isOn )
			{
				_exitSequence.Insert (0f , _rectTransform.DOScale ( ScaleOut.from , ScaleOut.duration ).
				                 SetEase ( ScaleOut.ease ).
				                 SetLoops ( ScaleOut.loops ).
				                 SetDelay ( ScaleOut.delay ).From() );
				
				_exitSequence.OnStart (()=> PlaySound ( ScaleOut.beginSound ) );
                _exitSequence.OnComplete(() => { PlaySound(ScaleOut.endSound); ExitCompleted(); });
			}
			
			_exitSequence.Play();

			return _exitSequence.Duration();
		}

	}

}
