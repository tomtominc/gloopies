﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using DG.Tweening;

namespace PotionSoup.MenuManagement
{
	public enum CullingMode { Current, Front, Back }
	public enum TimingAction { OnStart, OnEnd }
	
[System.Serializable]
	public class TweenData
	{
		[System.Serializable]
		public class TweenEvent : UnityEvent < TweenCallback > {}

		public LoopType loopType;

		public CullingMode cullingMode;
		public TimingAction cullingEffect;

		public bool isFrom = true;
		public bool isOn = false;

		public Vector3 from;

		public Ease ease = Ease.Linear;

		public float duration = 1f;
		public float delay = 0f;
		public int loops = 1;

		public AudioClip beginSound;
		public AudioClip endSound;

		public TweenEvent onStart = new TweenEvent();
		public TweenEvent onPlay = new TweenEvent();
		public TweenEvent onUpdate= new TweenEvent(); 
		public TweenEvent onStep = new TweenEvent();
		public TweenEvent onComplete = new TweenEvent();

		public RotateMode rotationMode;

	}
}
