﻿using UnityEngine;
using System.Collections;

public class CallbackSplash : SplashScreen 
{
    public bool isDone = false;

    protected override IEnumerator StartEffect()
    {
        while ( isDone == false )
        {
            yield return null;
        }

        isDone = false;
        gameObject.SetActive(false);


    }
}
