﻿using UnityEngine;
using System.Collections;
using PotionSoup.MenuManagement;

public class SplashScreen : MonoBehaviour 
{
    protected RectTransform _rectTransform;
    public string sound = "SplashFX";

    public virtual float DoStartSplashEffect ()
    {
        _rectTransform = GetComponent < RectTransform > ();

		gameObject.SetActive ( true );
        transform.SetSiblingIndex(MenuManager.Instance.transform.childCount);

		StartCoroutine ( StartEffect() );

        return 0f;
    }

    public virtual float DoEndSplashEffect ()
    {
        StartCoroutine( EndEffect() );

        return 0f;
    }

    protected virtual IEnumerator EndEffect ()
    {
        yield break;
    }

    protected virtual IEnumerator StartEffect ()
	{
        yield break;
	}


}
