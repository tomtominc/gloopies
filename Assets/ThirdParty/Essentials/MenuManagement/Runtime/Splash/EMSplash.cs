﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

public class EMSplash : SplashScreen 
{

    public Image image;
    public float loadingDelay = 1f;
    public string splashAudio = "PositiveJingle0";

    public List < GameObject > effects = new List < GameObject > ();

    public float frameRate = 12f;
    public List < Sprite > AnimSprites = new List<Sprite> ();

    public float FrameTime
    {
        get { return  frameRate * 0.01f;  }
    }

    public float duration 
    {
        get { return (AnimSprites.Count * ( FrameTime * Time.deltaTime )) + loadingDelay; }
    }

    public override float DoStartSplashEffect()
    {
        base.DoStartSplashEffect();

        return duration;
    }

    public override float DoEndSplashEffect()
    {
        base.DoEndSplashEffect();

        return duration ;
    }


    protected override IEnumerator StartEffect()
    {
        SoundManager.Instance.PlaySoundFX ( splashAudio );

        int i = 0;

        while ( i < AnimSprites.Count )
        {
            image.sprite = AnimSprites [i];

            i++;

            yield return new WaitForSeconds ( FrameTime  * Time.deltaTime );

        }

        foreach ( var e in effects ) e.SetActive ( true );
    }

    protected override IEnumerator EndEffect()
    {
        yield return new WaitForSeconds ( loadingDelay );


        foreach ( var e in effects ) e.SetActive ( false );

        int i = AnimSprites.Count - 1;

        while ( i >= 0 )
        {
            yield return new WaitForSeconds ( FrameTime * Time.deltaTime );

            image.sprite = AnimSprites [i];

            i--;



        }

        gameObject.SetActive ( false );
    }
}
