﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class EaseSplash : SplashScreen 
{
    public float easeDuration = 0.5f;
    public AnimationCurve easeCurve;

    private Tweener startEffect;
    private Tweener endEffect;
    public override float DoStartSplashEffect()
    {
        base.DoStartSplashEffect();

        return easeDuration;
    }

    public override float DoEndSplashEffect()
    {
        base.DoEndSplashEffect();

        return easeDuration;
    }

    protected override IEnumerator StartEffect()
    {
        Vector2 target = new Vector2 ( Camera.main.BoundsMax ().x + _rectTransform.rect.width,0f);
        _rectTransform.anchoredPosition = target;
        startEffect = _rectTransform.DOAnchorPos ( Vector2.zero , easeDuration, false ).SetEase(Ease.Linear );

        startEffect.Play ();

        //SoundManager.Instance.PlaySoundFX ( sound );

        yield break;
    }

    protected override IEnumerator EndEffect ()
    {
        startEffect.Complete();
        Vector2 target = new Vector2 ( Camera.main.BoundsMax ().x + _rectTransform.rect.width,0f);
        endEffect = _rectTransform.DOAnchorPos ( -target , easeDuration, false ).SetEase( Ease.InQuad ).SetDelay(1.0f);

        endEffect.Play ();

        //SoundManager.Instance.PlaySoundFX ( sound );

        yield break;
    }
}
