﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof (Image))]
public class DynamicIcon : MonoBehaviour, IDynamicGraphic
{
    protected Image _image;
    protected RectTransform _rectTransform;
    protected Vector2 _originalPosition;

    public bool useAttachedSprite = false;

    public Sprite onNormal;

    public Sprite onHover;
    public int hoverPixelOffset = 0;

    public Sprite onDown;
    public int downPixelOffset = 0;

    public Sprite onDisable;

    public int disabledPixelOffset = 0;

    private void Awake ()
    {
        _image = GetComponent < Image > ();
        _rectTransform = GetComponent < RectTransform > ();
        _originalPosition = _rectTransform.anchoredPosition;

        if ( useAttachedSprite )
        {
            onNormal = _image.sprite;
            onHover = _image.sprite;
            onDown = _image.sprite;
        }

        OnAwake ();
        OnNormal ();
    }

    protected virtual void OnAwake ()
    {
        
    }

    public virtual void SetAllSprites ( Sprite normal, Sprite hover, Sprite down )
    {
        onNormal = normal;
        onDown = down;
        onHover = hover;

        OnNormal ();
    }

    public virtual void OnNormal ()
    {
        if ( _image == null ) return;

        _image.sprite = onNormal;

        SetToOriginalPosition ();
    }

    public virtual void OnHover ()
    {
        if ( _image == null ) return;

        _image.sprite = onHover;

        SetToOriginalPosition ();

        _rectTransform.anchoredPosition = GetOffsetPosition ( hoverPixelOffset );
    }

    public virtual void OnDown ()
    {
        if ( _image == null ) return;

        _image.sprite = onDown;

        SetToOriginalPosition ();

        _rectTransform.anchoredPosition = GetOffsetPosition ( downPixelOffset );
    }

    public virtual void Disable ()
    {
        if ( _image == null ) return;

        _image.raycastTarget = false;
        _image.sprite = onDisable;

        SetToOriginalPosition ();

        _rectTransform.anchoredPosition = GetOffsetPosition ( disabledPixelOffset );
    }

    protected void SetToOriginalPosition ()
    {
        _rectTransform.anchoredPosition = _originalPosition;
    }

    protected Vector2 GetOffsetPosition ( int offset )
    {
        return new Vector2 ( _rectTransform.anchoredPosition.x, _rectTransform.anchoredPosition.y + (float)offset );
    }
}
