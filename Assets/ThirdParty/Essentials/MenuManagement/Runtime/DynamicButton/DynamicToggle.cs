﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DynamicToggle : DynamicIcon, IDynamicGraphic 
{
    public Sprite onNormalOff;
    public Sprite onHoverOff;
    public Sprite onDownOff;

    private bool _isOn = false;

    public bool IsOn 
    {
        get 
        { 
            return _isOn; 
        }

        set 
        {
            _isOn = value;

            OnNormal ();
        }
    }

    public override void OnNormal()
    {
        if ( _image == null ) return;

        _image.sprite = _isOn ? onNormal : onNormalOff;

        SetToOriginalPosition ();
    }

    public override void OnDown()
    {
        if ( _image == null ) return;

        _image.sprite = _isOn ? onDown : onDownOff;

        SetToOriginalPosition ();

        _rectTransform.anchoredPosition = GetOffsetPosition ( downPixelOffset );
    }

    public override void OnHover()
    {
        if ( _image == null ) return;

        _image.sprite = _isOn ? onHover : onHoverOff;

        SetToOriginalPosition ();

        _rectTransform.anchoredPosition = GetOffsetPosition ( hoverPixelOffset );
    }
}
