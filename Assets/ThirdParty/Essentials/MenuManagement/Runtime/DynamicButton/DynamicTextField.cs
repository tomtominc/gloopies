﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DynamicTextField : TextField, IDynamicGraphic 
{
    private RectTransform _rectTransform;
    private Vector2 _originalPosition;

    public Font onNormalFont;

    public Font onHoverFont;
    public int hoverPixelOffset = 4;

    public Font onDownFont;
    public int downPixelOffset = -4;

    private void Awake ()
    {
        _rectTransform = GetComponent < RectTransform > ();
        _originalPosition = _rectTransform.anchoredPosition;

        OnNormal ();
    }

    public void OnNormal ()
    {
        SetFont ( onNormalFont );
        SetToOriginalPosition ();
    }

    public void OnHover ()
    {
        SetFont ( onHoverFont );
        SetToOriginalPosition ();
        _rectTransform.anchoredPosition = GetOffsetPosition ( hoverPixelOffset );
    }

    public void OnDown ()
    {
        SetFont ( onDownFont );
        SetToOriginalPosition ();
        _rectTransform.anchoredPosition = GetOffsetPosition ( downPixelOffset );
    }

    public void SetFont ( Font font )
    {
        if ( _fields == null )
        {
            _fields = GetComponentsInChildren < Text > (true);
        }

        foreach ( Text field in _fields )
        {
            field.font = font;
        }
    }

    public void Disable ()
    {
        
    }

    private void SetToOriginalPosition ()
    {
        _rectTransform.anchoredPosition = _originalPosition;
    }

    private Vector2 GetOffsetPosition ( int offset )
    {
        return new Vector2 ( _rectTransform.anchoredPosition.x, _rectTransform.anchoredPosition.y + (float)offset );
    }
}
