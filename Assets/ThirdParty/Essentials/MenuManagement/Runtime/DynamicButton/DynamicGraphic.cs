﻿using UnityEngine;
using System.Collections;

public interface IDynamicGraphic 
{
    void OnHover  ();
    void OnDown   ();
    void OnNormal ();
    void Disable  ();
}
