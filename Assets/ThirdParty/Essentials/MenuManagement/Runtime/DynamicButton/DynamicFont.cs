﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DynamicFont : MonoBehaviour, IDynamicGraphic 
{
    private SpriteFontDisplay _fontDisplay;
    private RectTransform _rectTransform;
    private Vector2 _originalPosition;

    public SpriteFont onNormal;

    public SpriteFont onHover;
    public int hoverPixelOffset = 4;

    public SpriteFont onDown;
    public int downPixelOffset = -4;

    private void Awake ()
    {
        _fontDisplay = GetComponent < SpriteFontDisplay > ();
        _rectTransform = GetComponent < RectTransform > ();
        _originalPosition = _rectTransform.anchoredPosition;

        OnNormal ();
    }

    public void OnNormal ()
    {
        if ( _fontDisplay == null ) return;

        _fontDisplay.SetFont ( onNormal );

        SetToOriginalPosition ();
    }

    public void OnHover ()
    {
        if ( _fontDisplay == null ) return;

        _fontDisplay.SetFont ( onHover );

        SetToOriginalPosition ();

        _rectTransform.anchoredPosition = GetOffsetPosition ( hoverPixelOffset );
    }

    public void OnDown ()
    {
        if ( _fontDisplay == null ) return;

        _fontDisplay.SetFont ( onDown );

        SetToOriginalPosition ();

        _rectTransform.anchoredPosition = GetOffsetPosition ( downPixelOffset );
    }

    public void Disable ()
    {}


    private void SetToOriginalPosition ()
    {
        _rectTransform.anchoredPosition = _originalPosition;
    }

    private Vector2 GetOffsetPosition ( int offset )
    {
        return new Vector2 ( _rectTransform.anchoredPosition.x, _rectTransform.anchoredPosition.y + (float)offset );
    }
}
