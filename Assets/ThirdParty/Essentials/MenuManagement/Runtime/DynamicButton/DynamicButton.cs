﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.Events;

public class DynamicButton : Button 
{
    protected RectTransform rect { get { return GetComponent < RectTransform > (); } }
    public IDynamicGraphic[] targetGraphics;

    public UnityEvent onEnter = new UnityEvent ();
    public UnityEvent onExit = new UnityEvent ();

    protected override void Start()
    {
        base.Start();
        targetGraphics = GetComponentsInChildren < IDynamicGraphic > ();
        AddDefaultListeners ();
    }

    public virtual void Initialize () {}

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);

        foreach ( var graphic in targetGraphics ) graphic.OnDown ();
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);

        foreach ( var graphic in targetGraphics ) graphic.OnHover ();
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);

        foreach ( var graphic in targetGraphics ) graphic.OnNormal ();
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);

        foreach ( var graphic in targetGraphics ) graphic.OnNormal ();
    }

    public virtual float DoEnter ( float delay = 0f )
    {
        return rect.DOScale( 1f, 1f ).SetEase(Ease.OutElastic).SetDelay (delay).Duration (true);
    }

    public virtual float DoExit ( float delay = 0f )
    {
        return rect.DOScale( 0f, 1f ).SetEase(Ease.InElastic).SetDelay (delay).Duration (true);
    }

    public virtual void DoDisable ()
    {
        if ( targetGraphics == null ) targetGraphics = GetComponentsInChildren < IDynamicGraphic > ();

        foreach ( var graphic in targetGraphics ) graphic.Disable ();
    }

    public void RemoveAllListeners ()
    {
        onClick.RemoveAllListeners ();
        AddDefaultListeners ();
    }
    public void AddDefaultListeners ()
    {
        onClick.AddListener ( DoSoundFX );
    }

    protected virtual void DoSoundFX ()
    {
        SoundManager.Instance.PlaySoundFX ( "ButtonClick" );
    }
}
