﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Rotorz.ReorderableList;

[CustomEditor (typeof(SpriteFont))]
public class SpriteFontEditor : Editor 
{
    private SerializedProperty fontMap;

    private void OnEnable ()
    {
        fontMap = serializedObject.FindProperty ("fontMap");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update ();
        ReorderableListGUI.Title ("Font Map");
        ReorderableListGUI.ListField ( fontMap );
        serializedObject.ApplyModifiedProperties ();
    }
}

[CustomPropertyDrawer(typeof (FontItem))]
public class FontItemEditor : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 16f * 2f;
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var key = property.FindPropertyRelative ("key");
        var value = property.FindPropertyRelative ("value");

        Rect rect = new Rect (position.x,position.y, position.width , 16f );

        EditorGUI.PropertyField ( rect, key );

        rect = new Rect (position.x,position.y + EditorGUI.GetPropertyHeight (value)  , position.width , 16f );

        EditorGUI.PropertyField ( rect, value );
    }
}
