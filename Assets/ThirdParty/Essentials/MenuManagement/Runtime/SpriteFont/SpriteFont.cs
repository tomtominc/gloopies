﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpriteFont : ScriptableObject 
{
    [SerializeField]
    public List < FontItem > fontMap = new List<FontItem> ();

    private Dictionary < string , Sprite > _fontMap;

    public virtual Dictionary < string , Sprite > Map 
    {
        get 
        {
            if ( _fontMap == null )
            {
                _fontMap =new Dictionary<string, Sprite> ();
                foreach ( var item in fontMap ) _fontMap.Add ( item.key, item.value );
            }

            return _fontMap;
        }
    }

}
[System.Serializable]
public class FontItem
{
    public string key;
    public Sprite value;
}
