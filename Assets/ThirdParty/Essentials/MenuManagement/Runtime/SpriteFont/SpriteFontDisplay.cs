﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[RequireComponent (typeof(GridLayoutGroup))]
public class SpriteFontDisplay : MonoBehaviour 
{
    public bool setOnAwake = false;
    public string text;
    public GameObject characterPrefab;

    public SpriteFont font;

    private Image[] _images;
    private string _text;

    private void Awake ()
    {
        _images = GetComponentsInChildren < Image > ();

        if ( setOnAwake ) SetAndCreate (text);
    }

    public void SetFont ( SpriteFont font )
    {
        this.font = font;
        Set ( _text );
    }

    public void SetAndCreate ( string text )
    {
        if ( font == null || string.IsNullOrEmpty ( text ) ) return;

        transform.DestroyChildren ();

        text = text.ToUpper ();

        for ( int i = 0; i < text.Length; i++ )
        {
            string key = text[i].ToString ();

            if ( key == " " ) 
            {
                GameObject space = new GameObject (key, typeof(Image));

                space.transform.SetParent ( transform, false );

                Image spaceImage = space.GetComponent < Image > ();

                spaceImage.color = Color.clear;

                continue;
            }

            if (! font.Map.ContainsKey ( key ) ) continue;

            Sprite character = font.Map [ key ];

            GameObject go = new GameObject (key, typeof(Image));

            go.transform.SetParent ( transform, false );

            Image image = go.GetComponent < Image > ();

            image.sprite = character;

        }
    }

    public void Set ( string text )
    {
        if ( font == null || string.IsNullOrEmpty ( text ) ) return;

        for ( int i = 0; i < text.Length; i++ )
        {
            if ( i >= _images.Length ) continue;

            Sprite character;

            if ( font.Map.TryGetValue ( text [i].ToString () , out character ) )
            {
                _images [i].sprite = character;    
            }
        }

        _text = text;
    }
    
}
