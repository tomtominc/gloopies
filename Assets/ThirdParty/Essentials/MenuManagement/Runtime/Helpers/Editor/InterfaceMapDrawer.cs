﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

namespace PotionSoup.MenuManagement
{
    [CustomPropertyDrawer(typeof(InterfaceMap))]
    public class InterfaceMapDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 4f;
        }

        public void DoPopup < T > (SerializedProperty property, Rect position, SerializedProperty memberProperty )
        {
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

            if ( property.objectReferenceValue == null )
            {
                //EditorGUI.HelpBox(new Rect (position.x,position.y,position.width,16f), "Insert Component to get Members",MessageType.Info);
                return;
            }

            List< string > fields =  property.objectReferenceValue.GetType().GetProperties(flags).ToList().ConvertAll(x => string.Format("{0} ({1})",x.Name ,x.PropertyType ));

            fields.Sort();

            int currentIndex = Array.FindIndex < string > (fields.ToArray(), x => x.Contains( memberProperty.stringValue ));

            currentIndex = EditorGUI.Popup(new Rect (position.x,position.y,position.width,16f), "Member Field", currentIndex, fields.ToArray() );

            if ( currentIndex < fields.Count && currentIndex > -1 )
            {
                memberProperty.stringValue = fields[currentIndex].Split(' ')[0];
            }

        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            var idProperty = property.FindPropertyRelative("propertyId");
            var typeProperty = property.FindPropertyRelative("type");
            var imageProperty = property.FindPropertyRelative("image");
            var textProperty = property.FindPropertyRelative("text");
            var buttonProperty = property.FindPropertyRelative("button");
            var memberProperty = property.FindPropertyRelative("member");

            EditorGUI.PropertyField(position,idProperty,new GUIContent("Property ID"), true);

            position.y += 16f;

            EditorGUI.PropertyField(position,typeProperty,new GUIContent("Component Type"), true);

			position.y += 16f;

			int typeValue = typeProperty.enumValueIndex;

			if (typeValue == 0)
            {
                DoPopup < Text > ( textProperty, position, memberProperty );

                position.y += 16f;

                EditorGUI.PropertyField(position,textProperty, true);

            }
			else if (typeValue == 1)
            {
                DoPopup < Image > ( imageProperty, position, memberProperty );

                position.y += 16f;

                EditorGUI.PropertyField(position,imageProperty,new GUIContent(imageProperty.displayName), true); 
            }
            else if ( typeValue == 2 )
            {
                DoPopup < Button > ( buttonProperty, position, memberProperty );

                position.y += 16f;


                EditorGUI.PropertyField( position,buttonProperty,true);
            }

            EditorGUI.EndProperty();
        }
    }
}
