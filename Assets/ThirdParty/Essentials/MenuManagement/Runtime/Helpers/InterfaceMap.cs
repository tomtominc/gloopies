﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

namespace PotionSoup.MenuManagement
{
    [System.Serializable]
    public class InterfaceMap
    {
        public enum Type
        {
            Text,
            Image,
            Button
        }

        public Type type;
        public string propertyId;
        public string member;

        public Image image;
        public Text text;
        public Button button;

        public void SetValue(object value)
        {
            switch (type)
            {
             
                case Type.Image:
                    image.GetType().GetProperty(member).SetValue(image,value,null);
                    break;
                case Type.Text:
                    text.GetType().GetProperty(member).SetValue(text,value,null);
                    break;
                case Type.Button:
                    button.GetType().GetProperty(member).SetValue(button,value,null);
                    break;
            }
        }

    }
}