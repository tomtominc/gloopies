﻿using UnityEngine;
using System.Collections;

public enum NavigationEvent 
{
	OnFocus, OnLoseFocus, OnPositive, OnNegative, OnSelectablePressed
}

public enum CharacterSelectEvent
{
	OnSelect , OnDeselect , OnHorizontalLeft, OnHorizontalRight
}