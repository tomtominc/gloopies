﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System;

public class TweenFactory
{

    public static Tweener BuildMovementTween < T > ( TweenMotion motion, T component ) where T : Component
    {
        if ( typeof(T) == typeof(RectTransform) )
        {
            return BuildMovementTweenRectTransform(component as RectTransform,motion);
        }
        else if (typeof(T) == typeof (Transform))
        {
            return BuildMovementTweenTransform(component as Transform,motion);
        }

        Debug.LogError("Unsupported type");

        return null;
    }
    public static Tweener BuildScaleTween < T > ( TweenMotion motion, T component ) where T : Component
    {
        if ( typeof(T) == typeof(RectTransform) )
        {
            return BuildScaleTweenRectTransform(component as RectTransform,motion);
        }
        else if (typeof(T) == typeof (Transform))
        {
            return BuildScaleTweenTransform(component as Transform,motion);
        }

        Debug.LogError("Unsupported type: " + typeof(T));
        return null;
    }

    public static Tweener BuildRotateTween < T > ( TweenMotion motion, T component ) where T : Component
    {
        if ( typeof(T) == typeof(RectTransform) )
        {
            return BuildRotationTweenRectTransform(component as RectTransform,motion);
        }
        else if (typeof(T) == typeof (Transform))
        {
            return BuildRotationTweenTransform(component as Transform,motion);
        }

        Debug.LogError("Unsupported type");
        return null;
    }

    public static Tweener BuildColorTween < T >  ( TweenMotion motion, T component ) where T : Component
    {
        if ( typeof(T) == typeof(Image) )
        {
            
            return BuildColorTweenImage((component as Image),motion);
        }
        else if (typeof(T) == typeof (SpriteRenderer))
        {
            return BuildColorTweenSprite((component as SpriteRenderer),motion);
        }

        Debug.LogError("Unsupported type");
        return null;
    }

    private static Tweener BuildGenericTweenParameters ( Tweener tweener, TweenMotion motion )
    {
        tweener.SetId(motion.Id);
        tweener.SetDelay(motion.delay);

        if (motion.ease == Ease.INTERNAL_Custom) tweener.SetEase( motion.animationCurve );
        else tweener.SetEase(motion.ease);

        tweener.SetLoops( motion.loops, motion.loopType );
        tweener.SetRelative(motion.relative);

        if (motion.isFrom)
        {
            tweener.From();
        }

        if (motion.onStartEnabled)
        {
            tweener.OnStart( () => motion.OnStart.Invoke(motion));
        }

        if (motion.onCompleteEnabled)
        {
            tweener.OnComplete( () => motion.OnComplete.Invoke(motion));
        }

        if (motion.onStepCompleteEnabled )
        {
            tweener.OnStepComplete( () => motion.OnStepComplete.Invoke(motion));
        }

        if (motion.onUpdateEnabled )
        {
            tweener.OnUpdate( () => motion.OnUpdate.Invoke(motion));
        }

        return tweener;
    }


    private static Tweener BuildMovementTweenRectTransform ( RectTransform transform, TweenMotion motion )
    {
        Tweener tweener = null;

        if ( motion.tweenType == TweenMotion.TweenType.Punch )
        {
            tweener = transform.DOPunchPosition(motion.punch,motion.duration,motion.vibrato, motion.elasticity,motion.snapping);
        }
        else if (motion.tweenType == TweenMotion.TweenType.Shake )
        {
            tweener = transform.DOShakePosition(motion.duration,motion.strength, motion.vibrato, motion.randomness, motion.snapping);
        }
        else
        {
            tweener = transform.DOLocalMove(motion.motionTo, motion.duration );
        }

        return BuildGenericTweenParameters(tweener,motion);

    }

    private static Tweener BuildMovementTweenTransform ( Transform transform, TweenMotion motion )
    {
        Tweener tweener = null;

        if ( motion.tweenType == TweenMotion.TweenType.Punch )
        {
            tweener = transform.DOPunchPosition(motion.punch,motion.duration,motion.vibrato, motion.elasticity,false);
        }
        else if (motion.tweenType == TweenMotion.TweenType.Shake )
        {
            tweener = transform.DOShakePosition(motion.duration,motion.strength, motion.vibrato, motion.randomness, false);
        }
        else
        {
            tweener = transform.DOLocalMove(motion.motionTo, motion.duration );
        }

        return BuildGenericTweenParameters(tweener,motion);
    }

    private static Tweener BuildScaleTweenRectTransform ( RectTransform transform, TweenMotion motion )
    {
        Tweener tweener = null;

        if ( motion.tweenType == TweenMotion.TweenType.Punch )
        {
            tweener = transform.DOPunchScale(motion.punch,motion.duration,motion.vibrato, motion.elasticity);
        }
        else if (motion.tweenType == TweenMotion.TweenType.Shake )
        {
            tweener = transform.DOShakeScale(motion.duration,motion.strength, motion.vibrato, motion.randomness);
        }
        else
        {
            tweener = transform.DOScale(motion.motionTo, motion.duration );
        }

        return BuildGenericTweenParameters(tweener,motion);
    }

    private static Tweener BuildScaleTweenTransform ( Transform transform, TweenMotion motion )
    {
        Tweener tweener = null;

        if ( motion.tweenType == TweenMotion.TweenType.Punch )
        {
            tweener = transform.DOPunchScale(motion.punch,motion.duration,motion.vibrato, motion.elasticity);
        }
        else if (motion.tweenType == TweenMotion.TweenType.Shake )
        {
            tweener = transform.DOShakeScale(motion.duration,motion.strength, motion.vibrato, motion.randomness);
        }
        else
        {
            tweener = transform.DOScale(motion.motionTo, motion.duration );
        }

        return BuildGenericTweenParameters(tweener,motion);
    }

    private static Tweener BuildRotationTweenRectTransform ( RectTransform transform, TweenMotion motion )
    {
        Tweener tweener = null;

        if ( motion.tweenType == TweenMotion.TweenType.Punch )
        {
            tweener = transform.DOPunchRotation(motion.punch,motion.duration,motion.vibrato, motion.elasticity);
        }
        else if (motion.tweenType == TweenMotion.TweenType.Shake )
        {
            tweener = transform.DOShakeRotation(motion.duration,motion.strength, motion.vibrato, motion.randomness);
        }
        else
        {
            tweener = transform.DOLocalRotate(motion.motionTo, motion.duration, motion.rotateMode );
        }

        return BuildGenericTweenParameters(tweener,motion);
    }

    private static Tweener BuildRotationTweenTransform ( Transform transform, TweenMotion motion )
    {
        Tweener tweener = null;

        if ( motion.tweenType == TweenMotion.TweenType.Punch )
        {
            tweener = transform.DOPunchRotation(motion.punch,motion.duration,motion.vibrato, motion.elasticity);
        }
        else if (motion.tweenType == TweenMotion.TweenType.Shake )
        {
            tweener = transform.DOShakeRotation(motion.duration,motion.strength, motion.vibrato, motion.randomness);
        }
        else
        {
            tweener = transform.DOLocalRotate(motion.motionTo, motion.duration, motion.rotateMode );
        }

        return BuildGenericTweenParameters(tweener,motion);
    }

    private static Tweener BuildColorTweenSprite ( SpriteRenderer sprite, TweenMotion motion )
    {
        Tweener tweener = null;

        if ( motion.tweenType == TweenMotion.TweenType.Punch )
        {
            //tweener = transform.DOPunchRotation(motion.punch,motion.duration,motion.vibrato, motion.elasticity);
            tweener = sprite.DOColor(motion.colorTo,motion.duration);
        }
        else if (motion.tweenType == TweenMotion.TweenType.Shake )
        {
            //tweener = transform.DOShakeRotation(motion.duration,motion.strength, motion.vibrato, motion.randomness);
            tweener = sprite.DOColor(motion.colorTo,motion.duration);
        }
        else
        {
            tweener = sprite.DOColor(motion.colorTo,motion.duration);
        }

        return BuildGenericTweenParameters(tweener,motion);
    }

    private static Tweener BuildColorTweenImage ( Image image,  TweenMotion motion )
    {
        Tweener tweener = null;

        if ( motion.tweenType == TweenMotion.TweenType.Punch )
        {
            //tweener = transform.DOPunchRotation(motion.punch,motion.duration,motion.vibrato, motion.elasticity);
            tweener = image.DOColor( motion.colorTo, motion.duration );
        }
        else if (motion.tweenType == TweenMotion.TweenType.Shake )
        {
            //tweener = transform.DOShakeRotation(motion.duration,motion.strength, motion.vibrato, motion.randomness);
            tweener = image.DOColor( motion.colorTo, motion.duration );
        }
        else
        {   
            tweener = image.DOColor( motion.colorTo, motion.duration );
        }

        return BuildGenericTweenParameters(tweener,motion);
    }
}
