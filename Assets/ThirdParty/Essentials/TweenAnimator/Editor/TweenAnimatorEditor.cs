﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using DG.Tweening;
using System;
using NUnit.Framework;
using System.Collections.Generic;

[CustomEditor(typeof(TweenAnimator), true)]
public class TweenAnimatorEditor : Editor
{
    public TweenAnimator _tweenAnimator;

    private bool _showHelp = true;

    private string _newSequenceKey = string.Empty;

    private int _currentSequenceIndex = 0;

    private SequenceMotion _currentSequence;

    private SerializedProperty _currentSequenceProp;

    public void OnEnable()
    {
        _tweenAnimator = target as TweenAnimator;
        _showHelp = EditorPrefs.GetBool("TA_ShowHelp_", true);
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        serializedObject.Update();

        DrawTweenAnimatorSettings();

        DrawSequenceSettings();

        DrawTweenMotionSettings();

        DrawSequenceEventSettings();

        serializedObject.ApplyModifiedProperties();
        
    }

    public void DrawTweenAnimatorSettings()
    {
        bool tweenAnimatorToggle = EditorGUIExtensions.DrawHeader("Tween Animator Settings", EditorColor.pink);

        if (tweenAnimatorToggle)
        {
            EditorGUIExtensions.BeginContent(Color.white);

            EditorGUIExtensions.DrawTitle("General Settings");

            _tweenAnimator.playSequenceOnStart = EditorGUILayout.Toggle("Play Sequence On Start",  _tweenAnimator.playSequenceOnStart);

            if (_tweenAnimator.playSequenceOnStart )
            {
                EditorGUI.indentLevel++;
                _tweenAnimator.playOnAwakeSequence = EditorGUILayout.Popup("Sequence To Play",_tweenAnimator.playOnAwakeSequence, _tweenAnimator.Keys);
                EditorGUI.indentLevel--;
            }

            EditorGUIExtensions.DrawTitle("Creation Settings");

            EditorGUIExtensions.SetBackgroundColor(EditorColor.bluelight);

            _newSequenceKey = EditorGUILayout.TextField("New Sequence State", _newSequenceKey);

            EditorGUIExtensions.RestoreBackgroundColor();

            if (EditorGUIExtensions.Button("Create Sequence", "button", Color.white))
            {
                if (_newSequenceKey == string.Empty)
                {
                    //Debug.LogError("Sequence State is empty!");
                    return;
                }

                if (_tweenAnimator.Add(_newSequenceKey) == false)
                {
                    Debug.LogErrorFormat("Invalid entry, Sequence with key {0} already added, change the key name and try again.", _newSequenceKey);
                    return;
                }

                _newSequenceKey = string.Empty;

                return;
            }

            if (EditorGUIExtensions.Button("Clear All Sequences", "button", EditorColor.pink))
            {
                if (EditorUtility.DisplayDialog("Clear all sequences", "Are you sure you want to clear all sequences?", "Yes! Destory them all!", "No, please, no!"))
                {
                    _tweenAnimator.sequences.Clear();
                }
            }

            EditorGUIExtensions.EndContent();
        }
    }

    public void DrawSequenceSettings()
    {
        if (_tweenAnimator == null || _tweenAnimator.Count <= 0)
        {
            EditorGUILayout.HelpBox("Create a new sequence to get started!", MessageType.Info);
            return;
        }

        EditorGUILayout.Space();

        bool sequenceSettingsToggle = EditorGUIExtensions.DrawHeader("Sequence Settings", EditorColor.yellow);

        string[] sequences = _tweenAnimator.Keys;

        if (sequenceSettingsToggle)
        {
            EditorGUIExtensions.BeginContent(Color.white);

            _currentSequenceIndex = EditorGUILayout.Popup("Current Sequence: ", _currentSequenceIndex, sequences);

            if (_currentSequenceIndex > -1 && _currentSequenceIndex < sequences.Length)
            {
                _currentSequence = _tweenAnimator.GetValue(sequences[_currentSequenceIndex]);

                if (_currentSequence != null)
                {
                    float totalDuration = 0f;

                    foreach (var tween in _currentSequence.tweens)
                    {
                        if (totalDuration < tween.duration + tween.delay)
                        {
                            totalDuration = tween.duration + tween.delay;
                        }
                    }

                    EditorGUILayout.HelpBox(string.Format("### Sequence Info ### \n\nKey: {0} \nTween Count: {1} \nDuration: {2}", 
                            _currentSequence.key, _currentSequence.tweens.Count, totalDuration), MessageType.Info);

                    _currentSequence.cullingType = (SequenceMotion.CullingType)EditorGUILayout.EnumPopup("Culling Type", _currentSequence.cullingType);
                }
            }

            if (EditorGUIExtensions.Button("Play Sequence", "button", EditorColor.yellow))
            {
                if (_currentSequence != null && _currentSequence.tweens != null)
                {
                    _currentSequence.Play(_tweenAnimator.gameObject);
                }
            }

            EditorGUIExtensions.EndContent();
        }

        EditorGUILayout.Space();

       
       

    }

    public void DrawTweenMotionSettings()
    {
        string[] sequences = _tweenAnimator.Keys;

        SequenceMotion sequence = null;
        _currentSequenceProp = null;

        if (_currentSequenceIndex > -1 && _currentSequenceIndex < sequences.Length)
        {
            sequence = _tweenAnimator.GetValue(sequences[_currentSequenceIndex]);

            if (_currentSequenceIndex < serializedObject.FindProperty("sequences").arraySize)
            {
                _currentSequenceProp = serializedObject.FindProperty("sequences").GetArrayElementAtIndex(_currentSequenceIndex);
            }
        }

        if (sequence == null || _currentSequenceProp == null)
            return;

        

        bool motionSettingsToggle = EditorGUIExtensions.DrawHeader(string.Format("Tween Motion Settings [{0}]", sequence.key), EditorColor.greenyellow);

        if (motionSettingsToggle)
        {

            EditorGUIExtensions.BeginContent(Color.white);

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Collapse", "ButtonLeft"))
            {
                
            }
            if (GUILayout.Button("Expand", "ButtonMid"))
            {

            }
            if (GUILayout.Button("Clear", "ButtonMid"))
            {
                _currentSequence.tweens.Clear();
                return;
            }
            if (GUILayout.Button("Add", "ButtonRight"))
            {
                _tweenAnimator.Add(sequence.key, new TweenMotion()); 
                return;
            }

            EditorGUILayout.EndHorizontal();

            if (sequence.tweens != null)
            {
                List<int> indexesToDestroy = new List< int >();

                for (int i = 0; i < sequence.tweens.Count; i++)
                {
                    TweenMotion motion = sequence.tweens[i];

                    SerializedProperty motionProp = _currentSequenceProp.FindPropertyRelative("tweens").GetArrayElementAtIndex(i);

                    if (motion == null)
                        continue;

                    EditorGUIExtensions.BeginContent(Color.white);

                    EditorGUILayout.Separator();

                    EditorGUILayout.BeginHorizontal();

                    string header =  string.Format("{0}: [{1} Tween]", (i + 1) < 9 ? "0" + (i + 1).ToString() : (i + 1).ToString(), 
                        motion.motionType);

                    bool foldout = EditorGUIExtensions.DrawFoldoutAuto(header,string.Format("SequenceTweenFoldout_{0}", i),GUIStyle.none,Color.white,11);

                    EditorGUILayout.Space();

                    string id = motion.Id;

                    motion.enabled = EditorGUIExtensions.ToggleButton("Enabled", "EnabledKey_" + id, "button", EditorColor.green);

                    motionProp.FindPropertyRelative("enabled").boolValue = motion.enabled;

                    if (EditorGUIExtensions.Button("X", "button", EditorColor.red, GUILayout.Width(32f)))
                    {
                        indexesToDestroy.Add(i); 
                    }

                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.Separator();

                    if (foldout)
                    {
                        EditorGUI.indentLevel++;
                        EditorGUIExtensions.BeginContent(Color.white);

                        motion.motionType = (TweenMotion.MotionType)EditorGUILayout.EnumPopup("Motion Type", motion.motionType);
                        motionProp.FindPropertyRelative("motionType").enumValueIndex = (int)motion.motionType;

                        motion.duration = EditorGUILayout.FloatField("Duration", motion.duration);
                        motionProp.FindPropertyRelative("duration").floatValue = motion.duration;

                        motion.delay = EditorGUILayout.FloatField("Delay", motion.delay);
                        motionProp.FindPropertyRelative("delay").floatValue = motion.delay;

                        motion.ease = (Ease)EditorGUILayout.EnumPopup("Ease", motion.ease);
                        motionProp.FindPropertyRelative("ease").enumValueIndex = (int)motion.ease;

                        if (motion.ease == Ease.INTERNAL_Custom)
                        {   
                            EditorGUI.indentLevel++;
                            motion.animationCurve = EditorGUILayout.CurveField("Custom Curve", motion.animationCurve);
                            motionProp.FindPropertyRelative("animationCurve").animationCurveValue = motion.animationCurve;
                            EditorGUI.indentLevel--;
                        }

                        motion.loops = EditorGUILayout.IntField("Loops", motion.loops);
                        motionProp.FindPropertyRelative("loops").intValue = motion.loops;

                        if (motion.loops < 0)
                        {
                            motion.loops = -1;

                            EditorGUI.indentLevel++;

                            motion.loopType = (LoopType)EditorGUILayout.EnumPopup("Loop Type ", motion.loopType);
                            motionProp.FindPropertyRelative("loopType").enumValueIndex = (int)motion.loopType;

                            EditorGUI.indentLevel--;
                        }

                        motion.tweenType = (TweenMotion.TweenType)EditorGUILayout.EnumPopup("Tween Type", motion.tweenType);
                        motionProp.FindPropertyRelative("tweenType").enumValueIndex = (int)motion.tweenType;

                        EditorGUILayout.BeginHorizontal();

                        GUILayout.Space(16f);

                        string text = motion.isFrom ? "From" : "To";
						motion.isFrom = EditorGUIExtensions.ToggleButton(text, "MotionIsFromKey_" + id, "button", Color.green, GUILayout.Width(94f));
                        motionProp.FindPropertyRelative("isFrom").boolValue = motion.isFrom;

                        EditorGUILayout.Separator();

                        //GUILayout.Space(48f);
                        if (motion.motionType != TweenMotion.MotionType.Color)
                        {
                            motion.motionTo = EditorGUILayout.Vector3Field("", motion.motionTo);
                            motionProp.FindPropertyRelative("motionTo").vector3Value = motion.motionTo;
                        }
                        else
                        {
                            motion.colorTo = EditorGUILayout.ColorField("", motion.colorTo, GUILayout.ExpandWidth(true));
                            motionProp.FindPropertyRelative("colorTo").colorValue = motion.colorTo;
                        }

                        EditorGUILayout.EndHorizontal();

                        EditorGUI.indentLevel++;

                        if ( motion.tweenType == TweenMotion.TweenType.Default )
                        {
                            if (motion.motionType == TweenMotion.MotionType.Movement)
                            {
                                motion.relative = EditorGUILayout.Toggle("Relative",motion.relative);
                                motionProp.FindPropertyRelative("relative").boolValue = motion.relative;

                                motion.snapping = EditorGUILayout.Toggle("Snapping",motion.snapping);
                                motionProp.FindPropertyRelative("snapping").boolValue = motion.snapping;
                            }

                            if (motion.motionType == TweenMotion.MotionType.Rotation)
                            {
                                motion.rotateMode = (RotateMode)EditorGUILayout.EnumPopup("Rotate Mode", motion.rotateMode);
                                motionProp.FindPropertyRelative("rotateMode").enumValueIndex = (int)motion.rotateMode;

                                motion.relative = EditorGUILayout.Toggle("Relative",motion.relative);
                                motionProp.FindPropertyRelative("relative").boolValue = motion.relative;
                            }

                            if (motion.motionType == TweenMotion.MotionType.Scale)
                            {
                                motion.relative = EditorGUILayout.Toggle("Relative",motion.relative);
                                motionProp.FindPropertyRelative("relative").boolValue = motion.relative;
                            }
                        }



                        if (motion.tweenType == TweenMotion.TweenType.Punch)
                        {
                            motion.punch = EditorGUILayout.Vector3Field("Punch", motion.punch);
                            motionProp.FindPropertyRelative("punch").vector3Value = motion.punch;

                            motion.elasticity = EditorGUILayout.FloatField("Elasticity", motion.elasticity);
                            motionProp.FindPropertyRelative("elasticity").floatValue = motion.elasticity;

                            motion.vibrato = EditorGUILayout.IntField("Vibrato", motion.vibrato);
                            motionProp.FindPropertyRelative("vibrato").intValue = motion.vibrato;
						}

                        if (motion.tweenType == TweenMotion.TweenType.Shake)
                        {
                            motion.vibrato = EditorGUILayout.IntField("Vibrato", motion.vibrato);
                            motionProp.FindPropertyRelative("vibrato").intValue = motion.vibrato;

                            motion.strength = EditorGUILayout.Vector3Field("Strength", motion.strength);
                            motionProp.FindPropertyRelative("strength").vector3Value = motion.strength;

                            motion.randomness = EditorGUILayout.FloatField("Randomness", motion.randomness);
                            motionProp.FindPropertyRelative("randomness").floatValue = motion.randomness;
                        }


                        // unindent twice! 
                        EditorGUI.indentLevel--;
                        EditorGUI.indentLevel--;

                        EditorGUILayout.Separator();

                        EditorGUILayout.BeginHorizontal();



                        motion.onStartEnabled = EditorGUIExtensions.ToggleButton("OnStart", "OnStartKey_" + id,"ButtonLeft", EditorColor.green);
                        motion.onCompleteEnabled = EditorGUIExtensions.ToggleButton( "OnComplete", "OnCompleteKey_" + id, "ButtonMid", EditorColor.green);
                        motion.onStepCompleteEnabled = EditorGUIExtensions.ToggleButton("OnStepComplete","OnStepCompleteKey_" + id,"ButtonMid",EditorColor.green);
                        motion.onUpdateEnabled = EditorGUIExtensions.ToggleButton("OnUpdate","OnUpdateKey_" + id,"ButtonRight", EditorColor.green);

                        EditorGUILayout.EndHorizontal();

                        if (motion.onStartEnabled)
                        {
                            SerializedProperty OnStart = serializedObject.FindProperty("sequences")
                        .GetArrayElementAtIndex(_currentSequenceIndex)
                        .FindPropertyRelative("tweens").GetArrayElementAtIndex(i)
                            .FindPropertyRelative("OnStart");

                            EditorGUILayout.PropertyField(OnStart, true);
                          
                        }
                        if (motion.onCompleteEnabled)
                        {
                            SerializedProperty OnComplete = serializedObject.FindProperty("sequences")
                        .GetArrayElementAtIndex(_currentSequenceIndex)
                        .FindPropertyRelative("tweens").GetArrayElementAtIndex(i)
                            .FindPropertyRelative("OnComplete");

                            EditorGUILayout.PropertyField(OnComplete, true);
                        }
                        if (motion.onStepCompleteEnabled)
                        {
                            SerializedProperty OnStepComplete = serializedObject.FindProperty("sequences")
                        .GetArrayElementAtIndex(_currentSequenceIndex)
                        .FindPropertyRelative("tweens").GetArrayElementAtIndex(i)
                            .FindPropertyRelative("OnStepComplete");

                            EditorGUILayout.PropertyField(OnStepComplete, true);
                        }
                        if (motion.onUpdateEnabled)
                        {
                            SerializedProperty OnUpdate = serializedObject.FindProperty("sequences")
                        .GetArrayElementAtIndex(_currentSequenceIndex)
                        .FindPropertyRelative("tweens").GetArrayElementAtIndex(i)
                            .FindPropertyRelative("OnUpdate");

                            EditorGUILayout.PropertyField(OnUpdate, true);
                        }

                        EditorGUIExtensions.EndContent();
                   
                    }

                    EditorGUIExtensions.DrawBox(string.Empty, 1f, "box", Color.white);
                    EditorGUIExtensions.EndContent();

                }

                   
                foreach (int i in indexesToDestroy)
                {
                    if (i >= 0 && i < sequence.tweens.Count)
                    {
                        sequence.tweens.RemoveAt(i);
                    }

                }

            }

            EditorGUIExtensions.EndContent();
        }

        EditorGUILayout.Separator();
    }

    public void DrawSequenceEventSettings()
    {
        string[] sequences = _tweenAnimator.Keys;

        SequenceMotion sequence = null;

        if (_currentSequenceIndex > -1 && _currentSequenceIndex < sequences.Length)
        {
            sequence = _tweenAnimator.GetValue(sequences[_currentSequenceIndex]);
        }

        if (sequence == null)
            return;

        
        bool sequenceEventHeader = EditorGUIExtensions.DrawHeader("Sequence Events", EditorColor.bluelight);

        if (sequenceEventHeader)
        {
            EditorGUIExtensions.BeginContent(Color.white);

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Collapse", "ButtonLeft"))
            {
                
            }
            if (GUILayout.Button("Expand", "ButtonMid"))
            {   
                
            }
            if (GUILayout.Button("Clear", "ButtonMid"))
            {
                sequence.CustomEvents.Clear();
                return;
            }
            if (GUILayout.Button("Add", "ButtonRight"))
            {
                sequence.CustomEvents.Add(new TweenCustomEvent());
                return;
            }

            EditorGUILayout.EndHorizontal();

            List <int> indexesToDestroy = new List<int>();

            for (int i = 0; i < sequence.CustomEvents.Count; i++)
            {
                var e = sequence.CustomEvents[i];

                if (e == null)
                    continue;

                EditorGUILayout.Separator();

                EditorGUILayout.BeginHorizontal();
                string foldoutString = string.Format("{0}: {1}", (i + 1) < 9 ? "0" + (i + 1).ToString() : (i + 1).ToString(), e.name);
                bool foldout = EditorGUIExtensions.DrawFoldoutAuto (foldoutString, string.Format("SequenceCustomActionsFoldout_{0}", e.GetHashCode()), GUIStyle.none, Color.white, 11);

                EditorGUILayout.Space();


                if (EditorGUIExtensions.Button("X", "button", EditorColor.red, GUILayout.Width(32f)))
                {
                    indexesToDestroy.Add(i); 
                }

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Separator();

                if (foldout)
                {
                    EditorGUI.indentLevel++;

                    EditorGUIExtensions.BeginContent(EditorColor.greyblue);

                    SerializedProperty OnAction = serializedObject.FindProperty("sequences")
                            .GetArrayElementAtIndex(_currentSequenceIndex)
                            .FindPropertyRelative("CustomEvents").GetArrayElementAtIndex(i)
                                .FindPropertyRelative("OnAction");

                    
                    e.name = EditorGUIExtensions.LabelField("Name", e.name, EditorStyles.textField, EditorColor.bluelight);
                    e.atTime = EditorGUILayout.FloatField("Activate at Time", e.atTime);

                    EditorGUILayout.PropertyField(OnAction, true);

                    EditorGUI.indentLevel--;


                    EditorGUIExtensions.EndContent();
                }

                EditorGUIExtensions.DrawBox(string.Empty, 1f, "box", Color.white);
                 

            }

            EditorGUIExtensions.EndContent();

            foreach (int i in indexesToDestroy)
            {
                if (i >= 0 && i < sequence.CustomEvents.Count)
                {
                    sequence.CustomEvents.RemoveAt(i);
                }

            }

           
        }

        EditorGUILayout.Separator();
    }

    public void DrawRotateModeHelp(RotateMode rotateMode)
    {
        switch (rotateMode)
        {
            case RotateMode.Fast:
                DrawHelpBox("Fastest way to rotate, it will never rotate beyond 360 degrees.");
                break;
            case RotateMode.FastBeyond360:
                DrawHelpBox("Fastest way to rotate but will rotate beyond 360 degrees.");
                break;
            case RotateMode.LocalAxisAdd:
                DrawHelpBox("Adds the given rotgation to the transform's local axis " +
                    "(like when rotating an object with the 'local' switch enabled in Unity's editor or using transform.Rotate (Space.Self))." +
                    "In this mode the end value is always considered relative.");
                break;
            case RotateMode.WorldAxisAdd:
                DrawHelpBox("Adds the given rotation to the transform using world axis and an advanced precision mode" +
                    " (like when using transform.Rotate(Space.World)). In this mode the end value is always considered relative.");
                break;
        }
    }

    public void DrawLoopTypeHelp(LoopType loopType)
    {
        switch (loopType)
        {
            case LoopType.Incremental:
                DrawHelpBox("Continuously increments the tween at the end of each " +
                    "loop cycle (A to B, B to B+(A-B), and so on) thus always moving" +
                    "'onward'");
                break;
            case LoopType.Restart:
                DrawHelpBox("Each Loop cycle starts from the beginning.");
                break;
            case LoopType.Yoyo:
                DrawHelpBox("The tween moves forward and backwards at alternate cycles.");
                break;
        }
    }

    public void DrawHelpBox(string message)
    {
        if (_showHelp)
            EditorGUILayout.HelpBox(message, MessageType.Info);
    }
}
