﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using System;

[System.Serializable]
public class TweenMotion
{
    [System.Serializable]
    public enum MotionType 
    {
        Movement, Rotation, Scale, Color
    }
    [System.Serializable]
    public enum TweenType
    {
        Default,
        Punch,
        Shake
    }

    [SerializeField]
    public MotionType motionType = MotionType.Movement;

    [SerializeField]
    public string id = string.Empty;

    public string Id 
    { get 
        {
            if (id ==string.Empty)
            {
                id = Guid.NewGuid().ToString();
            }

            return id;
        }
    }

    [SerializeField]
    public bool enabled = true;
    [SerializeField]
    public bool isFrom = false;
    [SerializeField]
    public TweenType tweenType = TweenType.Default;
    [SerializeField]
    public float duration = 1.0f;
    [SerializeField]
    public float delay = 0f;
    [SerializeField]
    public Ease ease = Ease.OutQuad;
    [SerializeField]
    public AnimationCurve animationCurve = new AnimationCurve();
    [SerializeField]
    public int loops = 1;
    [SerializeField]
    public LoopType loopType = LoopType.Yoyo;
    [SerializeField]
    public Vector3 punch = Vector3.one;
    [SerializeField]
    public float elasticity = 1f;
    [SerializeField]
    public int vibrato = 10;
    [SerializeField]
    public Vector3 strength = Vector3.one;
    [SerializeField]
    public float randomness = 90f;
    [SerializeField]
    public bool relative = false;
    [SerializeField]
    public bool snapping = false;

    [SerializeField]
    public RotateMode rotateMode;
    [SerializeField]
    public Vector3 motionTo = Vector3.zero;
    [SerializeField]
    public Color colorTo = Color.white;

    public bool onStartEnabled = false;
    [SerializeField]
    public TweenEvent OnStart = new TweenEvent();

    public bool onCompleteEnabled = false;
    [SerializeField]
    public TweenEvent OnComplete = new TweenEvent();

    public bool onUpdateEnabled = false;
    [SerializeField]
    public TweenEvent OnUpdate = new TweenEvent();

    public bool onStepCompleteEnabled = false;
    [SerializeField]
    public TweenEvent OnStepComplete = new TweenEvent();

    public Tweener Build ( GameObject user )
    {
        if ( motionType != MotionType.Color )
        {
            RectTransform rectTransform = user.GetComponent < RectTransform > ();

            if (rectTransform == null )
            {
                return Build < Transform > (user.transform);
            }
            else 
            {
                return Build < RectTransform > ( rectTransform );
            }
        }
        else 
        {
            Image image = user.GetComponent < Image > ();

            if (image == null)
            {
                SpriteRenderer sprite = user.GetComponent < SpriteRenderer > ();

                if (sprite != null)
                {
                    return Build < SpriteRenderer > (sprite);
                }
            }
            else 
            {
                return Build < Image > ( image ); 
            }
        }

        Debug.LogError("No valid component on object!");

        return null;
    }

    public Tweener Build < T >  ( T component ) where T : Component
    {
        if (enabled == false )
        {
            return null;
        }

        Tweener tweener = null;

        switch ( motionType )
        {
            case MotionType.Movement: 
            tweener = TweenFactory.BuildMovementTween < T > ( this, component);
            break;

            case MotionType.Scale: 
            tweener = TweenFactory.BuildScaleTween < T > ( this, component );
            break;

            case MotionType.Rotation: 
            tweener = TweenFactory.BuildRotateTween < T > ( this, component );
            break;

            case MotionType.Color: 
            tweener = TweenFactory.BuildColorTween < T > ( this, component);
            break;
        }

        return tweener;
    }
}


