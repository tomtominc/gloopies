﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PotionSoup.Persistent
{
    public class PersistentSettings
    {
		public static void Clear ()
		{
            PlayerPrefs.DeleteAll ();
		}

        public static bool Contains ( string key )
        {
            return PlayerPrefs.HasKey ( key );
        }

        public static T GetValue<T> (string key, T defaultValue)
        {
            if (!PlayerPrefs.HasKey(key))
			{
				SetValue(key,defaultValue);
			}

            string value = PlayerPrefs.GetString (key,string.Empty);

            if (value == string.Empty) 
				return defaultValue;
            else 
				return DeserializeObject<T>(value);
        }

        public static void SetValue(string key, object value)
        {
            string serialized = SerializeObject(value);
            PlayerPrefs.SetString(key,serialized);

            PlayerPrefs.Save ();
        }

        public static T DeserializeObject<T> (string serialized)
        {
            return JSONSerializer.Deserialize<T>(serialized);
        }

        public static string SerializeObject(object value)
        {
            return JSONSerializer.Serialize(value, false);
        }
    }
}
